<?php

// composer autoload и dotenv подключаются в файлах конфигурации ядра
// bitrix/.settings.php и bitrix/php_interface/dbconn.php
// которые в свою очередь можно обновить, отредактировав данные в директории /environments/
// и "перезагрузить" командой `./vendor/bin/jedi env:init default`



// так как  автолоад (в нашем случае) регистрируется до ядра,
// Твиг не успевает зарегистрироваться
// необходимо это действие повтроить еще раз:

use Local\ServiceProvider\BaseFacade\Facade;
use Local\ServiceProvider\ServiceProvider;
use Symfony\Component\Security\Csrf\CsrfTokenManager;

maximasterRegisterTwigTemplateEngine();

Arrilot\BitrixModels\ServiceProvider::register();
Arrilot\BitrixModels\ServiceProvider::registerEloquent();

Bex\Monolog\MonologAdapter::loadConfiguration();

CModule::IncludeModule('iblock');
CModule::IncludeModule('form');

// Arrilot\BitrixMigrations\Autocreate\Manager::init($_SERVER["DOCUMENT_ROOT"].'/migrations');

// Csrf токен приложения. До всякого кэширования.
$csrf = new CsrfTokenManager();
$appCsrfToken = $csrf->getToken('app')->getValue();
$_SESSION['csrf_token'] = $appCsrfToken;

// Symfony сервис-провайдер
$symfonyServiceProvider = new ServiceProvider(
    'local/configs/services.yaml'
);

// Инициализация фасадов сервис-провайдера.
Facade::boot();

include_once 'events.php';
