<?php
/**
 * @var array $arParams
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (empty($arResult['documents'])) {
    return;
}
?>

<div class="container">
    <h1 class="title title_regular">
        <?php echo $arResult['documents']['H1']?>
    </h1>
</div>

<section class="section container objectList">
    <div id="vue-lotAccordionBlock" class="vue-component" data-component="LotAccordionBlock">
        <?php echo $arResult['documents']['DESCRIPTION']?>
    </div>
</section>


