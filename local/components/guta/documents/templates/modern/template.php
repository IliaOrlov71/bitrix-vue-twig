<?php
/**
 * @var array $arParams
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (empty($arResult['documents']['tree'])) {
    return;
}
?>
    <div class="container mb-4">
        <h1 class="title title_regular">
            <?php echo $arResult['documents']['H1']?>
        </h1>
    </div>

    <div class="container">
        <?php
            $count = 0;
            foreach ($arResult['documents']['tree'] as $sectionName => $data) :?>
            <?php if (!empty($data)) : ?>
                <?php if ($count === 0) :?>
                    <h2 class="mb-4 h2 pl-3"><?php echo $sectionName?></h2>
                <?php else : ?>
                    <h3 class="mb-3 h4 pl-3"><?php echo $sectionName?></h3>
                <?php endif;?>

                <table class="table mb-5">
                    <tbody>
                        <?php foreach ($data as $item) :?>
                            <tr>
                                <td>
                                    <a target="_blank" href="<?php echo $item['PDF_FILE']?>" class="btn-default">
                                        <?php echo $item['NAME']?>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            <?php endif;?>
            <?php $count++;?>
        <?php endforeach;?>
    </div>
