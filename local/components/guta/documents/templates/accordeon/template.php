<?php
/**
 * @var array $arParams
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (empty($arResult['documents']['tree'])) {
    return;
}?>

<accordion>
<?php
    $count = 0;
    foreach ($arResult['documents']['tree'] as $sectionName => $data) :?>
        <?php if (!empty($data)) : ?>
                <template v-slot:title>
                    <?php echo $sectionName?>
                </template>
                <template v-slot:content>
                    <?php foreach ($data as $item) :?>
                        <a target="_blank" href="<?php echo $item['PDF_FILE']?>" class="btn-default">
                            <?php echo $item['NAME']?>
                        </a>
                        <br>
                    <?php endforeach;?>
            <?php if ($count === 0) :?>
                <accordion>
            <?php else : ?>
                </template>
                </accordion>
                <accordion>
            <?php endif;?>
         <?php endif;?>
    <?php $count++; ?>
<?php endforeach;?>
        </template>
</accordion>
