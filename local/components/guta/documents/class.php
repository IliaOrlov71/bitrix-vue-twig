<?php

namespace local\components;

use Bex\Bbc\Basis;
use Bitrix\Main\Loader;
use CFile;
use Local\Models\ObjectFiles;
use Local\Models\ObjectFilesSection;
use Local\Models\RentalObjects;
use Local\SEO\SectionMetaTagsDirector;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!Loader::includeModule('bex.bbc')) {
    return false;
}

/**
 * Class Documents
 * @package local\components
 */
class Documents extends Basis
{
    /**
     * Основная логика - получение
     * и обработка данных
     * Кешируется
     * @return void
     */
    protected function executeMain()
    {
        if ($this->arParams['CODE']) {
            $this->arParams['ID'] = RentalObjects::idByCode($this->arParams['CODE']);
        }

        $this->arParams['ID_DOCS_SUBSECTION'] = RentalObjects::idDocs($this->arParams['ID']);

        if (!$this->arParams['ID']) {
            return;
        }

        $this->arResult['documents'] = $this->getDescriptionObject($this->arParams['ID']);

        $this->arResult['documents']['tree'] = [];
        if ($this->arParams['ID_DOCS_SUBSECTION']) {
            $this->arResult['documents']['tree'] = $this->getDocumentTree(
                $this->arParams['ID_DOCS_SUBSECTION']
            );
        }

        $h1 = $this->arResult['documents']['NAME'];
        $this->arResult['documents']['H1'] = $this->arParams['H1'] ?? $h1;
    }

    /**
     * @param integer $sectionId ID подраздела (объекта).
     *
     * @return array
     */
    private function getDescriptionObject(int $sectionId) : array
    {
        return RentalObjects::getById($sectionId)->toArray();
    }

    /**
     * Собрать данные на раздел документов.
     *
     * @param integer $subsectionId ID подраздела.
     *
     * @return array
     */
    private function getDocumentTree(int $subsectionId) : array
    {
        $docs = ObjectFiles::query()
            ->sort(['SORT' => 'ASC', 'SECTION' => 'ASC'])
            ->filter(['ACTIVE' => 'Y', 'SECTION_ID' => $subsectionId, 'INCLUDE_SUBSECTIONS' => 'Y'])
            ->select('ID', 'NAME', 'PROPERTY_DOCUMENTS', 'IBLOCK_SECTION_ID', 'SORT')
            ->getList()
            ->groupBy(['IBLOCK_SECTION_ID'])
        ;

        $result = [];
        $docs->each(
            function ($items, $index) use (&$result) {
                $section = ObjectFilesSection::getById($index);
                $result[$section['NAME']] =  $items->toArray();
            }
        );

        foreach ($result as $section => $items) {
            foreach ($items as $key => $item) {
                $result[$section][$key]['PDF_FILE'] = (string)CFile::GetPath($item['PROPERTY_DOCUMENTS_VALUE']);
            }

        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    protected function executeEpilog()
    {
        global $APPLICATION;
        $APPLICATION->AddChainItem('Документы ' . $this->arResult['documents']['H1']);

        $metaTagDirector = new SectionMetaTagsDirector();
        $metaTagDirector->setPrefix('Документы  | ');
        $metaTagDirector->setDefaultTitle($this->arResult['documents']['H1']);
        $metaTagDirector->setDefaultDescription($this->arResult['documents']['H1']);

        $metaTagDirector->set(RentalObjects::IBLOCK_ID, $this->arResult['documents']['ID']);
    }
}
