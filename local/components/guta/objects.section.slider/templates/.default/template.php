<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (empty($arResult['objects'])) {
    return;
}
?>

<object-slider :initial='<?php echo htmlspecialchars(json_encode($arResult), ENT_QUOTES, 'UTF-8');?>' v-if="isShowSlider">

</object-slider>
