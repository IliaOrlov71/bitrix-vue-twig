<?php

namespace local\components;

use Bex\Bbc\Basis;
use Illuminate\Database\Eloquent\Collection;
use Local\Models\RentalObjects;
use Local\Models\Rentals;
use Local\Util\Slider\CommonTrait;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) {
    return false;
}

/**
 * Class ObjectsSectionSlider
 * @package local\components
 */
class ObjectsSectionSlider extends Basis
{
    use CommonTrait;

    /** @const integer SLIDER_WIDTH_PICTURE Ширина картинок для ресайза. */
    private const SLIDER_WIDTH_PICTURE = 800;
    /** @const integer SLIDER_HEIGHT_PICTURE Высота картинок для ресайза. */
    private const SLIDER_HEIGHT_PICTURE = 800;
    /** @const integer MIN_COUNT_ITEMS_IN_SLIDER Минимальное количество картинок в слайдере. */
    private const MIN_COUNT_ITEMS_IN_SLIDER = 3;

    /**
     * Получение данных, согласно посещению подраздела.
     *
     * @param integer $idSubSection ID подраздела, взятое из кук.
     *
     * @return array
     */
    private function getDataFromSubsection(int $idSubSection) : array
    {
        $obLotsModel = Rentals::query()
            ->filter(['ACTIVE' => 'Y', 'SECTION_ID' => $idSubSection])
            ->sort(['SORT' => 'DESC'])
            ->select(self::$arFilter)
            ->fetchUsing('GetNext')
            ->getList();

        return $this->processModel($obLotsModel);
    }

    /**
     * Данные из битриксового arResult.
     *
     * @param array $arResultData Битриксовый arResult.
     *
     * @return array
     */
    private function getDataFromArResult(array $arResultData = []) : array
    {
        if (empty($arResultData)) {
            return [];
        }

        $obLotsModel = collect($arResultData);

        $arResult = [];

        $obLotsModel->each(
            function ($item, $key) use (&$arResult) {
                $indexObject = $item['ID'];
                $arData = $item;
                $arData['PREVIEW_PICTURE'] = $item['PREVIEW_PICTURE']['ID'];
                $arData['DETAIL_PICTURE'] = $item['DETAIL_PICTURE']['ID'];

                // Ресайз картинки.
                $this->treatmentPicture($arData);
                // Обработка параметров. Только в случае наличия картинки.
                if (!empty($arData['PICTURE_RESIZED'])) {
                    $arResult[$indexObject] = $this->treatmentProperties($arData);
                }
            }
        );

        return $arResult;
    }

    /**
     * Обработка свойств из битриксовского arResult.
     *
     * @param array $arData Битриксовый $arResult['ITEMS'].
     *
     * @return array
     */
    private function treatmentProperties(array $arData) : array
    {
        $arResult = [
            'title' => $arData['NAME'],
            'url' => $arData['DETAIL_PAGE_URL'],
            'image' => $arData['PICTURE_RESIZED'],
            'tags' => !empty($arData['PROPERTIES']['TYPE']['VALUE']) ? $arData['PROPERTIES']['TYPE']['VALUE'] : [],
            'area' => $arData['PROPERTIES']['SQUARE']['VALUE'],
            'city' => $arData['PROPERTIES']['CITY']['VALUE'],
            'address' => !empty($arData['~PROPERTIES']['ADDRESS']['VALUE']) ? $arData['~PROPERTIES']['ADDRESS']['VALUE'] :
                $arData['UF_ADDRESS'],
            'PRICE_SQUARE' => self::normalizePrice($arData['PROPERTIES']['PRICE_SQUARE']['VALUE']),
            'PRICE_TOTAL' => self::normalizePrice($arData['PROPERTIES']['TOTAL_PRICE']['VALUE']),
            'PRICE_TOTAL_MONTH' => self::normalizePrice($arData['PROPERTIES']['TOTAL_PRICE_MONTH']['VALUE']),
            'pricePerYear' => self::normalizePrice($arData['PROPERTIES']['PRICE_SQUARE']['VALUE']),
            'pricePerMonth' => self::normalizePrice($arData['PROPERTIES']['TOTAL_PRICE_MONTH']['VALUE']),
        ];

        return $arResult;
    }

    /**
     * Основная логика - получение
     * и обработка данных
     * Кешируется
     * @return void
     * @throws \Bitrix\Main\LoaderException   Error.
     */
    protected function executeMain()
    {
        if (empty($this->arParams['FILTERED_PARAMS'])) {
            $this->arResult['objects'] =  $this->getDataFromSubsection((int)$this->arParams['IBLOCK_SECTION_ID']);
        } else {
            $this->arResult['objects'] =  $this->getDataFromArResult($this->arParams['FILTERED_PARAMS']);
        }

        $this->arResult['objects'] = array_values($this->arResult['objects']);
    }
}
