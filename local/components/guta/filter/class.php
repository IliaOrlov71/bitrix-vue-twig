<?php

namespace local\components;

use Bex\Bbc\Basis;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\LoaderException;
use Local\Constants;
use Local\Guta\IBlockManager;
use Local\Office\FilterData;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) {
    return false;
}

/**
 * Class ObjectLists
 * @package local\components
 */
class Filter extends Basis
{
    /**
     * Основная логика - получение
     * и обработка данных
     * Кешируется
     * @return void
     * @throws \Bitrix\Main\LoaderException   Error.
     */
    protected function executeMain()
    {
        $arObjects = FilterData::objects();

        // Формирование данных под JSON фильтра.
        $arFilter['filters'] = [
            'city' => [
                'title' => 'Город',
                'items' => $arObjects['city'],
            ],

            'objects' => [
                'title' => 'Объекты',
                'items' => $arObjects['objects']
            ],
            'type' => [
                'title' => 'Категория',
                'items' => FilterData::getTypes()
            ],
        ];

        // URL инфоблока. Используется в качестве целевой страницы для фильтра.
        try {
            $arFilter['url'] = IBlockManager::getIblockUrlByCode(
                [
                    'TYPE' => Constants::IBLOCK_TYPE_CONTENT,
                    'CODE' => Constants::IBLOCK_CODE_RENTA,
                ]
            );
        } catch (ArgumentException $e) {
            $arFilter['url'] = '';
        } catch (LoaderException $e) {
            $arFilter['url'] = '';
        }

        // Если надо, то выставить флаг открывать фильтр сразу после загрузки страницы.
        if (!empty($this->arParams['FILTER']) && $this->arParams['FILTER'] === 'Y') {
            $arFilter['static'] = true;
        }

        /** @var array $arMaxMinParameters Минимальные и максимальные значения цены и площади лотов.*/
        $arMaxMinParameters = FilterData::getMinMax();

        $arFilter = array_merge($arMaxMinParameters, $arFilter);
        $this->arResult = $arFilter;
    }
}
