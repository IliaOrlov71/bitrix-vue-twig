<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (empty($arResult)) {
    return;
}
?>
<div class="vue-component" id="vue-search" data-component="Search" data-initial='<?php echo json_encode($arResult)?>'></div>



