<?php

namespace local\components;

use Bex\Bbc\Basis;
use Local\Constants;
use Local\Guta\IBlockManager;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) {
    return false;
}

/**
 * Class Footer
 * @package local\components
 */
class Footer extends Basis
{
    /**
     * Метод получает список значений свойства TYPE инфоблока,
     * @return void
     * @throws
     */
    protected function executeMain()
    {
        $this->arResult['BLOCK_URL'] = IBlockManager::getIblockUrlByCode(array(
                'type' => Constants::IBLOCK_TYPE_CONTENT,
                'code' => Constants::IBLOCK_CODE_RENTA
        ));

        $this->arResult['AR_TYPES'] =\Local\Guta\IBlockElementManager::getIBlockElements(
            ['AR_ORDER' => ['SORT' => 'DESC'],
                'AR_FILTER' => ['ACTIVE' => 'Y', 'IBLOCK_CODE' => Constants::IBLOCK_CODE_RENTA],
                'AR_GROUP' => ['PROPERTY_TYPE']
            ]
        );

        $this->arResult['PHONE'] = Constants::PHONE_TO_CALL;
    }
}
