<?php

namespace local\components;

use CBitrixComponent;
use Local\Custom\FormGenerator;
use Local\Custom\PDFFacade;
use Local\Custom\PDFGenerator;

/**
* Класс для генерации PDF-файла из любой формы.
* Class PDFComponent
*/
class PDFComponent extends CBitrixComponent
{

    /**
    * Метод на основе $arParams формирует на сервере файл PDF.
     * Возвращает путь до этого файла.
     *
    * @return array
    */
    public function setFile(): array
    {
        $sRegeneratedForm = PDFFacade::getHTML(
            $this->arParams['BLOCK_ID'],
            $this->arParams['ELEMENT_ID'],
            $this->arParams['BLOCK_SECTION_ID']
        );
        $arResult['PDF_FILE_PATH'] = PDFFacade::setPDF($sRegeneratedForm);

        return $arResult;
    }

    /**
    * Метод заполняет значениями $arResult и вызывает шаблон компонента
    *
    * @return void
    */
    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $this->arResult = array_merge($this->arResult, $this->setFile());
            $this->includeComponentTemplate();
        }
    }
}

