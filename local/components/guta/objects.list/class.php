<?php

namespace local\components;

use Bex\Bbc\Basis;
use Illuminate\Database\Eloquent\Collection;
use Local\Models\RentalObjects;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) {
    return false;
}

/**
 * Class ObjectLists
 * @package local\components
 */
class ObjectLists extends Basis
{
    /**
     * Основная логика - получение
     * и обработка данных
     * Кешируется
     * @return void
     * @throws \Bitrix\Main\LoaderException   Error.
     */
    protected function executeMain()
    {
        $obRentalModel = RentalObjects::query()
            ->filter(['ACTIVE' => 'Y'])
            ->sort(['SORT' => 'DESC'])
            ->select('ID', 'NAME', 'UF_ADDRESS', 'UF_CITY', 'SECTION_PAGE_URL')
            ->fetchUsing('GetNext')
            ->getList()
            ->groupBy('UF_CITY');

        /**
         * Collections
         */

        $arCollections = [];
        $obRentalModel->each(
            static function ($item, $key) use (&$arCollections) {
                /** @var Collection $item */
                $arCollections[] = ['CITY' => $key, 'DATA' => $item->toArray()];
            }
        );

        $this->arResult['ITEMS'] = $arCollections;
    }
}
