<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}


if (empty($arResult['ITEMS'])) {
    return;
}
?>

<!-- officesList "Список адресов по городам" -->
<div class="vue-component" data-component="OfficesList" data-initial='<?=json_encode($arResult['ITEMS'])?>'></div>



