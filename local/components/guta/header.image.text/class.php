<?php

namespace local\components;

use Bex\Bbc\Basis;
use Local\Constants;
use Local\Guta\IBlockManager;
use Local\Util\GutaResize;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) {
    return false;
}

/**
 * Class HeaderImage
 * @package local\components
 */
class HeaderImage extends Basis
{
    /** @const string DEFAULT_PICTURE_PATH Картинка по умолчанию. */
    private const DEFAULT_PICTURE_PATH = '/local/assets/images/index_img.png';
    /**
     * Метод получает список значений свойства TYPE инфоблока,
     * @return void
     * @throws
     */
    protected function executeMain()
    {
        $this->arResult['H1'] = $this->arParams['H1'];

        /** @var integer $iblockId ID инфоблока. */
        $iblockId = IBlockManager::getIBlockIdByCode(
            ['TYPE' => Constants::IBLOCK_TYPE_CONTENT,
                'CODE' => Constants::IBLOCK_CODE_RENTA
            ]
        );
        // Описание.
        $this->arResult['DESCRIPTION'] = IBlockManager::getIBlockDescriptionByCode(
            ['TYPE' => Constants::IBLOCK_TYPE_CONTENT,
             'CODE' => Constants::IBLOCK_CODE_RENTA
            ]
        );
        // Ресайз картинки. Если картинки нет, то использовать статическую.
        // По умолчанию.
        /** @var string $sImageUrl URL картинки, подлежащей ресайзу. */
        $sImageUrl = IBlockManager::getImageIB($iblockId);
        // По умолчанию: статическая картинка.
        $this->arResult['IMAGE'] = self::DEFAULT_PICTURE_PATH;
        if ($sImageUrl) {
            $this->arResult['IMAGE'] = GutaResize::url($sImageUrl, 1920, 693);
        }
    }
}
