<?php

namespace local\components;

use Bex\Bbc\Basis;
use Local\Models\Rentals;
use Local\Util\Slider\CommonTrait;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) {
    return false;
}

/**
 * Class SimilarLots
 * @package local\components
 */
class SimilarLots extends Basis
{
    use CommonTrait;

    /** @const integer SLIDER_WIDTH_PICTURE Ширина картинок для ресайза. */
    private const SLIDER_WIDTH_PICTURE = 800;
    /** @const integer SLIDER_HEIGHT_PICTURE Высота картинок для ресайза. */
    private const SLIDER_HEIGHT_PICTURE = 800;
    /** @const integer MIN_COUNT_ITEMS_IN_SLIDER Минимальное количество картинок в слайдере. */
    private const MIN_COUNT_ITEMS_IN_SLIDER = 3;
    /** @const integer MAX_COUNT_PICTURES_IN_SLIDER Максимальное количество картинок в слайдере. */
    private const MAX_COUNT_PICTURES_IN_SLIDER = 12;

    /**
     * Получение данных, фильтр по категории помещения.
     *
     * @param integer $iExcludeId ID лота, исключаемого из выборки.
     * @param array   $arTypes    ID свойств категорий лота, подлежащих выборке.
     *
     * @return array
     */
    private function getDataFromCategories(int $iExcludeId, array $arTypes = []) : array
    {
        if (empty($arTypes)) {
            return [];
        }

        $obLotsModel = Rentals::query()
            ->filter(['ACTIVE' => 'Y', 'PROPERTY_TYPE' => $arTypes, '!ID' => $iExcludeId])
            ->sort(['SORT' => 'DESC'])
            ->activeCategory()
            ->select(self::$arFilter)
            ->limit(self::MAX_COUNT_PICTURES_IN_SLIDER)
            ->fetchUsing('GetNext')
            ->getList();

        return $this->processModel($obLotsModel);
    }

    /**
     * Получить все категории лота по его ID.
     *
     * @param integer $idElement ID лота.
     *
     * @return array
     */
    private function getCategoriesOfLot(int $idElement) : array
    {
        if (!$idElement) {
            return [];
        }

        $obLotsModel = Rentals::query()
            ->filter(['ACTIVE' => 'Y', 'ID' => $idElement])
            ->sort(['SORT' => 'DESC'])
            ->select('ID', 'NAME', 'PROPERTY_TYPE', 'IBLOCK_SECTION_ID')
            ->limit(1)
            ->getList();

        $arData = $obLotsModel->first()->toArray();

        return !empty($arData['PROPERTY_TYPE_VALUE']) ? array_keys($arData['PROPERTY_TYPE_VALUE']) : [];
    }

    /**
     * Основная логика - получение
     * и обработка данных
     * Кешируется
     * @return void
     * @throws \Bitrix\Main\LoaderException   Error.
     */
    protected function executeMain()
    {
        // Нужно ли нормализовывать лоты.
        $this->bNormalizeQuantityItems = $this->arParams['NORMALIZE_QUANTITY'] === true ? true : false;
        /** @var integer $iCurrentIdElement ID текущего элемента. */
        $iCurrentIdElement = (int)$this->arParams['CURRENT_ELEMENT_ID'];
        /** @var array $arTypes Все категории лота. */
        $arTypes = $this->getCategoriesOfLot($iCurrentIdElement);

        $this->arResult['objects'] = $this->getDataFromCategories($iCurrentIdElement, $arTypes);

        // Если лотов меньше минимального количества и выставлен параметр "Нормализация",
        // то нужно догнать количество до минимального.
        if ($this->bNormalizeQuantityItems
            && count($this->arResult['objects']) < self::MIN_COUNT_ITEMS_IN_SLIDER) {
            $this->arResult['objects'] = $this->normalizeCountItems($this->arResult['objects']);
        }

        $this->arResult['objects'] = array_values($this->arResult['objects']);
    }
}
