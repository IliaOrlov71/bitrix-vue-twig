<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (empty($arResult['objects'])) {
    return;
}
?>

<!--Похожие предложения-->
<section class="section recommendObject">
    <?php if (!empty($arParams['HEADER'])) :?>
        <div class="container">
            <h2 class="title recommendObject__title recommendObject__title--small">
                <?php echo $arParams['HEADER'];?>
            </h2>
        </div>
    <?php endif?>
    <div class="vue-component" data-component="ObjectSlider" data-initial='<?php echo json_encode($arResult)?>'></div>

</section>
<!--/Похожие предложения-->


