<?php

namespace local\components;

use Bex\Bbc\Basis;
use Illuminate\Database\Eloquent\Collection;
use Local\Models\RentalObjects;
use Local\Models\Rentals;
use Local\Util\Slider\CommonTrait;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) {
    return false;
}

/**
 * Class RecommendedObjects
 * @package local\components
 */
class RecommendedObjects extends Basis
{
    use CommonTrait;

    /** @const integer SLIDER_WIDTH_PICTURE Ширина картинок для ресайза. */
    private const SLIDER_WIDTH_PICTURE = 800;
    /** @const integer SLIDER_HEIGHT_PICTURE Высота картинок для ресайза. */
    private const SLIDER_HEIGHT_PICTURE = 800;
    /** @const integer MIN_COUNT_ITEMS_IN_SLIDER Минимальное количество картинок в слайдере. */
    private const MIN_COUNT_ITEMS_IN_SLIDER = 3;

    /**
     * Получение данных "обычным" (без использования кук) способом.
     * Один лот из каждого объекта.
     *
     * @return array
     */
    private function getOrdinaryData() : array
    {
        $obRentalModel = RentalObjects::query()
            ->filter(['ACTIVE' => 'Y'])
            ->sort(['SORT' => 'DESC'])
            ->select('ID', 'UF_ADDRESS')
            ->getList();

        $arResult = [];

        $obRentalModel->each(
            function ($item, $key) use (&$arData, &$arResult) {
                $obLotsModel = Rentals::query()
                    ->filter(['ACTIVE' => 'Y', 'SECTION_ID' => $item['ID']])
                    ->sort(['SORT' => 'DESC'])
                    ->select(self::$arFilter)
                    ->limit(1) // ТЗ: выводить по одному лоту каждого объекта.
                    ->fetchUsing('GetNext')
                    ->getList();

                if ($obLotsModel->isNotEmpty()) {
                    $arResult = array_merge($arResult, $this->processModel($obLotsModel));
                }
            }
        );

        return $arResult;
    }

    /**
     * Получение данных, согласно посещению подраздела.
     *
     * @param integer $idSubSection ID подраздела, взятое из кук.
     *
     * @return array
     */
    private function getDataFromCookies(int $idSubSection) : array
    {
        $obLotsModel = Rentals::query()
            ->filter(['ACTIVE' => 'Y', 'SECTION_ID' => $idSubSection])
            ->sort(['SORT' => 'DESC'])
            ->select(self::$arFilter)
            ->fetchUsing('GetNext')
            ->getList();

        return $this->processModel($obLotsModel);
    }

    /**
     * Основная логика - получение
     * и обработка данных
     * Кешируется
     * @return void
     * @throws \Bitrix\Main\LoaderException   Error.
     */
    protected function executeMain()
    {
        $this->bNormalizeQuantityItems = $this->arParams['NORMALIZE_QUANTITY'] === true;

        // Если не задан параметр COOKIES, то получать данные обычным образом.
        if (empty($this->arParams['COOKIES'])) {
            $this->arResult['objects'] = $this->getOrdinaryData();
        // Иначе выбираем лоты из категории, заданной параметровм  COOKIES.
        } else {
            $this->arResult['objects'] =  $this->getDataFromCookies((int)$this->arParams['COOKIES']);
            // Если лотов меньше минимального количества и выставлен параметр "Нормализация",
            // то нужно догнать количество до минимального.
            if ($this->bNormalizeQuantityItems
                && count($this->arResult['objects']) < self::MIN_COUNT_ITEMS_IN_SLIDER) {
                    $this->arResult['objects'] = $this->normalizeCountItems($this->arResult['objects']);
            }
        }
        $this->arResult['objects'] = array_values($this->arResult['objects']);
    }
}
