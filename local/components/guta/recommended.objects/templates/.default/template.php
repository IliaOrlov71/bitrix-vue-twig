<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (empty($arResult['objects'])) {
    return;
}
?>

<!--Рекомендованные объекты-->
<section class="section recommendObject">
    <?php if (!empty($arParams['HEADER'])) :?>
        <h2 class="title recommendObject__title">
            <?php echo $arParams['HEADER'];?>
        </h2>
    <?php endif?>
    <div class="vue-component" data-component="ObjectSlider" data-initial='<?php echo json_encode($arResult)?>'></div>

</section>
<!--/Рекомендованные объекты-->

