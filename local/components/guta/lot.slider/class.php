<?php

namespace local\components;

use Bex\Bbc\Basis;
use Local\Models\Rentals;
use Local\Util\GutaResize;
use Local\Util\GutaResizeNoUpscale;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!\Bitrix\Main\Loader::includeModule('bex.bbc')) {
    return false;
}

/**
 * Class LotSlider
 * @package local\components
 */
class LotSlider extends Basis
{
    /** @const integer SLIDER_WIDTH_PICTURE Ширина картинок для ресайза. */
    private const SLIDER_WIDTH_PICTURE = 800;
    /** @const integer SLIDER_HEIGHT_PICTURE Высота картинок для ресайза. */
    private const SLIDER_HEIGHT_PICTURE = 800;
    /** @const integer MAX_COUNT_PICTURES Максимальное количество картинок в слайдере. */
    private const MAX_COUNT_PICTURES = 12;

    /**
     * Основная логика - получение
     * и обработка данных
     * Кешируется
     * @return void
     * @throws \Bitrix\Main\LoaderException   Error.
     */
    protected function executeMain()
    {
        /** @var array $arPics ID картинок для ресайза. */
        $arPics = [];

        // Ресайз картинок, переданных в параметре 'IMAGES'.
        if (!empty($this->arParams['IMAGES'])) {
            // Ограничить максимальное количество картинок.
            $arOriginalPicturesId = array_slice($this->arParams['IMAGES'], 0, self::MAX_COUNT_PICTURES);

            array_walk($arOriginalPicturesId, function ($picsId) use (&$arPics) {
                $arPics[] = [
                    'image' => GutaResizeNoUpscale::resize(
                        $picsId,
                        self::SLIDER_WIDTH_PICTURE,
                        self::SLIDER_HEIGHT_PICTURE
                    ),
                    'original_image' => GutaResizeNoUpscale::resize(
                        $picsId,
                        1920,
                        1080
                    ),
                ];
            });
        } else {
            // Если картинок не передали, то берем в дело картинку PREVIEW_PICTURE или DETAIL_PICTURE
            $arPics = $this->getDataFromPreviewPicture($this->arParams['ID']);
        }

        $this->arResult['objects'] = $arPics;
    }

    /**
     * Пустить в дело картинку PREVIEW_PICTURE или DETAIL_PICTURE.
     *
     * @param integer $idElement ID элемента.
     *
     * @return array
     */
    protected function getDataFromPreviewPicture(int $idElement): array
    {
        /** @var array $arPics ID картинок для ресайза. */
        $arPics = [];

        $obLotsModel = Rentals::query()
            ->filter(['ACTIVE' => 'Y', 'ID' => $idElement])
            ->select('ID', 'PREVIEW_PICTURE', 'DETAIL_PICTURE')
            ->getList()
            ->first()
        ;
        /** @var array $arData Выборка данных о лотах. */
        $arData = $obLotsModel->toArray();

        // PREVIEW_PICTURE или DETAIL_PICTURE.
        /** @var integer $idPicture Битриксовый ID картинки, которая пойдет под ресайз.*/
        $idPicture = 0;
        if ($arData['DETAIL_PICTURE']) {
            $idPicture = $arData['DETAIL_PICTURE'];
        } elseif ($arData['PREVIEW_PICTURE']) {
            $idPicture = $arData['PREVIEW_PICTURE'];
        }
        // Ресайз картинки.
        if ($idPicture) {
            $arPics[] = [
                'image' => GutaResize::resize(
                    $idPicture,
                    self::SLIDER_WIDTH_PICTURE,
                    self::SLIDER_HEIGHT_PICTURE
                )
            ];
        }

        return $arPics;
    }
}
