<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

if (empty($arResult['objects'])) {
    return;
}
?>

<!--Слайдер детальной страницы лота-->
<div class="vue-component" data-component="LotSlider" data-initial='<?php echo json_encode($arResult)?>'></div>
<!--/Слайдер детальной страницы лота-->


