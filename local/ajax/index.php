<?php
use Local\Util\Ajax\AjaxFactory;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$host = filter_input(INPUT_GET, 'host', FILTER_VALIDATE_URL);
$host = $host ?? '*';

// Обезопасить массив $_POST.
$data = AjaxFactory::sanitizeArray((array)$_POST);

$component = mb_strtolower(filter_var($_REQUEST['component'], FILTER_SANITIZE_STRING));

$factory = new AjaxFactory($host, $data, ['Response']);
$factory->getInstance($component)->getResponse();
$factory->getAnswer();
