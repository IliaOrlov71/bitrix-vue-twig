<?php
return [
    Local\Bundles\CustomArgumentResolverBundle\CustomArgumentResolverBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Local\Bundles\BitrixCustomPropertiesBundle\BitrixCustomPropertiesBundle::class => ['all' => true],
];

