/**
 * Здесь определяется коллекция Vue компонентов для модуля vueInvoker
 * Помимо "статичных" компонентов возможна также загрузка динамических "по требованию"
 *
 * import StaticComponent from './components/StaticComponent'
 * export default {
 *  StaticComponent,
 *  DynamicComponent: () => import('./components/DynamicComponent.vue')
 * };
 *
 */

import Header from './components/Header.vue';
import Footer from './components/Footer.vue';
import Search from './components/Search.vue';
import PopupForm from './components/PopupForm.vue';
import PopupMessage from './components/PopupMessage.vue';
import SubscribeForm from './components/SubscribeForm.vue';
import InfoPanel from './components/InfoPanel.vue';
import ObjectList from './components/ObjectList.vue';
import ObjectListFilter from './components/ObjectListFilter.vue';
import ObjectCard from './components/ObjectCard.vue';
import ObjectSlider from './components/ObjectSlider.vue';
import OfficesList from './components/OfficesList.vue';
import LotSlider from './components/LotSlider.vue';
import LotAccordionBlock from './components/LotAccordionBlock.vue';
import DocAccordionBlock from './components/DocAccordionBlock.vue';

export default {
  Header,
  Footer,
  Search,
  PopupForm,
  PopupMessage,
  SubscribeForm,
  InfoPanel,
  ObjectList,
  ObjectListFilter,
  ObjectCard,
  ObjectSlider,
  OfficesList,
  LotSlider,
  LotAccordionBlock,
  DocAccordionBlock,
};
