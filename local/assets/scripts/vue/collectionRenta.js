/**
 * Здесь определяется коллекция Vue компонентов для модуля vueInvoker
 * Помимо "статичных" компонентов возможна также загрузка динамических "по требованию"
 *
 * import StaticComponent from './components/StaticComponent'
 * export default {
 *  StaticComponent,
 *  DynamicComponent: () => import('./components/DynamicComponent.vue')
 * };
 *
 */

import OfficesItems from './components/OfficesItems.vue';
import OfficesSlider from './components/OfficesSlider.vue';

export default {
    OfficesItems,
    OfficesSlider,
};