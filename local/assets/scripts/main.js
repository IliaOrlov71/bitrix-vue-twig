/** @global jQuery */

// Polyfills
import elementClosest from 'element-closest';

// import local dependencies
import Router from './util/Router';

import common from './routes/common';

elementClosest(window);

// import pageMain module on-demand only when body has `page_main` class
const pageMain = () => import('./routes/pageMain');
const pageRenta = () => import('./routes/pageRenta');

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Main page
  pageMain,
  pageRenta,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
