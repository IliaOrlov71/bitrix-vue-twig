<?php

namespace Local\Bundles\ApiExceptionBundle\Exception;

/**
 * Class BadRequestException
 *
 * @since 26.10.2020
 */
class BadRequestException extends Exception
{
}
