<?php

namespace Local\Bundles\ApiExceptionBundle\Exception;

use Local\Bundles\ApiExceptionBundle\Exception\Interfaces\HttpExceptionInterface;

/**
 * class HttpException
 *
 * @since 26.10.2020
 */
class HttpException extends Exception implements HttpExceptionInterface
{
    /**
     * @var integer
     */
    protected $statusCode;

    /**
     * @var array
     */
    protected $headers;

    /**
     * Constructor
     *
     * @param integer $statusCode Код ответа.
     * @param integer $code       Код ошибки.
     * @param string  $message    Сообщение.
     * @param array   $headers    Заголовки.
     */
    public function __construct(
        int $statusCode = 500,
        int $code = 0,
        string $message = '',
        array $headers = []
    ) {
        $this->statusCode = $statusCode;
        $this->headers    = $headers;
        parent::__construct($code, $message);
    }

    /**
     * Set status code.
     *
     * @param mixed $statusCode Код ответа.
     *
     * @return self
     */
    public function setStatusCode($statusCode) : self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get status code
     *
     * @return string
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set headers.
     *
     * @param array $headers Заголовки.
     *
     * @return self
     */
    public function setHeaders(array $headers) : self
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Get headers.
     *
     * @return array
     */
    public function getHeaders() : array
    {
        return $this->headers;
    }
}
