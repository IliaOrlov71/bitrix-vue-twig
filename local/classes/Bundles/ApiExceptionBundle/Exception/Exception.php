<?php

namespace Local\Bundles\ApiExceptionBundle\Exception;

use Local\Bundles\ApiExceptionBundle\Exception\Interfaces\ExceptionInterface;

/**
 * class Exception
 *
 * @since 26.10.2020
 */
class Exception extends \Exception implements ExceptionInterface
{
    /** @const string VARIABLE_REGEX Паттерн для вычленения переменной. */
    private const VARIABLE_REGEX = "/(\{[a-zA-Z0-9\_]+\})/";

    /**
     * Constructor
     *
     * @param integer $code    Код.
     * @param string  $message Сообщение об ошибке.
     */
    public function __construct(
        int $code = 0,
        string $message = ''
    ) {
        parent::__construct($message, $code);
    }

    /**
     * Set code
     *
     * @param integer $code Код.
     *
     * @return self
     */
    public function setCode($code) : self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Set message
     *
     * @param string $message Сообщение об ошибке.
     *
     * @return self
     */
    public function setMessage($message) : self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message with variables
     *
     * @throws \Exception
     *
     * @return string
     */
    public function getMessageWithVariables()
    {
        $message = $this->message;

        preg_match(self::VARIABLE_REGEX, $message, $variables);

        foreach ($variables as $variable) {
            $variableName = substr($variable, 1, -1);

            if (!isset($this->$variableName)) {
                throw new \Exception(sprintf(
                    'Variable "%s" for exception "%s" not found',
                    $variableName,
                    get_class($this)
                ), 500);
            }

            if (!is_string($this->$variableName)) {
                $this->$variableName = (string)$this->$variableName;
            }

            $message = str_replace($variable, $this->$variableName, $message);
        }

        return $message;
    }
}
