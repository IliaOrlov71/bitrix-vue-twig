<?php

namespace Local\Bundles\ApiExceptionBundle\Exception;

/**
 * Class BadRequestHttpException
 *
 * @since 26.10.2020
 */
class BadRequestHttpException extends HttpException
{
}
