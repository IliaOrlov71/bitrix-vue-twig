<?php

namespace Local\Bundles\CustomRequestResponserBundle\Event\Listeners;

use Local\Bundles\CustomRequestResponserBundle\Event\Interfaces\OnKernelResponseHandlerInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Class XRobots
 *
 * @package Local\Bundles\CustomRequestResponserBundle\Event\Listeners
 *
 * @since 21.05.2021
 */
class XRobots implements OnKernelResponseHandlerInterface
{
    /**
     * Событие kernel.response.
     *
     * Установка заголовков XRobots.
     *
     * @param ResponseEvent $event Объект события.
     *
     * @return void
     *
     */
    public function handle(ResponseEvent $event): void
    {
        // Фильтрация обычных маршрутов.
        if (!$event->isMasterRequest()
            ||
            $event->getResponse()->getStatusCode() === 404
        ) {
            return;
        }

        $response = $event->getResponse();

        $response->headers->set('X-Robots-Tag', 'noindex');
    }
}
