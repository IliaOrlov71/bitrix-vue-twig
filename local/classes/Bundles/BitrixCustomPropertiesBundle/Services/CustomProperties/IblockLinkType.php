<?php

namespace Local\Bundles\BitrixCustomPropertiesBundle\Services\CustomProperties;

use Bitrix\Main\LoaderException;
use CIBlock;
use Local\Bundles\BitrixCustomPropertiesBundle\Services\IblockPropertyType\Abstraction\IblockPropertyTypeBase;

/**
 * Class IblockLinkType
 * Привязка к инфоблоку
 *
 * @package Local\Bundles\BitrixCustomPropertiesBundle\Services\CustomProperties
 *
 * @since 10.03.2021
 */
class IblockLinkType extends IblockPropertyTypeBase
{
    /**
     * @var array
     */
    private static $iblockList;

    /**
     * @inheritdoc
     */
    public function getPropertyType()
    {
        return static::PROPERTY_TYPE_STRING;
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Привязка к инфоблоку';
    }

    /**
     * @inheritdoc
     */
    public function getCallbacksMapping()
    {
        return [
            'GetAdminListViewHTML' => [$this, 'getAdminListViewHTML'],
            'GetPropertyFieldHtml' => [$this, 'getPropertyFieldHtml'],
            'GetAdminFilterHTML'   => [$this, 'getAdminFilterHTML'],
            'GetUIFilterProperty'  => [$this, 'getUIFilterProperty'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAdminListViewHTML(array $property, array $value, array $control)
    {
        return static::getIblockName($value['VALUE']);
    }

    /**
     * @inheritdoc
     */
    public function getPropertyFieldHtml(array $property, array $value, array $control)
    {
        return static::getFormFieldHtml($control['VALUE'], $value['VALUE']);
    }

    /**
     * @inheritdoc
     */
    public function getUIFilterProperty(array $property, $controlName, array &$filter)
    {
        $filter["type"] = "list";
        $filter["items"] = array_column(static::getIblockList(), 'NAME', 'NAME');
    }

    /**
     * @inheritdoc
     */
    public function getAdminFilterHTML(array $property, array $control)
    {
        $curValue = '';
        if (isset($_REQUEST[$control['VALUE']])) {
            $curValue = $_REQUEST[$control['VALUE']];
        } elseif (isset($GLOBALS[$control['VALUE']])) {
            $curValue = $GLOBALS[$control['VALUE']];
        }

        return static::getFormFieldHtml($control['VALUE'], $curValue);
    }

    /**
     * @param string  $inputName
     * @param string  $selectedValue
     * @param boolean $addEmpty
     *
     * @return string
     */
    private function getFormFieldHtml(string $inputName, string $selectedValue = '', $addEmpty = true)
    {
        $items = static::getIblockList();

        $input = '<select style="max-width:250px;" name="' . $inputName . '">';

        $input .= ($addEmpty) ? '<option value="">нет</option>' : '';

        foreach ($items as $item) {
            $selected = ($item['ID'] == $selectedValue) ? 'selected="selected"' : '';
            $input .= '<option ' . $selected . ' value="' . $item['ID'] . '">' . $item['NAME'] . '</option>';
        }
        $input .= '</select>';

        return $input;
    }

    /**
     * @param string $iblockId
     *
     * @return string
     */
    private function getIblockName(string $iblockId)
    {
        $iblockId = trim($iblockId);
        if ($iblockId == '') {
            return '';
        }

        $forms = static::getIblockList();
        if (array_key_exists($iblockId, $forms) && array_key_exists('NAME', $forms[$iblockId])) {
            return trim($forms[$iblockId]['NAME']);
        }

        return trim($iblockId);
    }

    /**
     * @return array
     */
    private static function getIblockList() : array
    {
        if (is_array(static::$iblockList)) {
            return static::$iblockList;
        }

        static::$iblockList = [];

        $dbres = CIBlock::GetList(['SORT' => 'SORT'], ['ACTIVE' => 'Y']);
        while ($item = $dbres->Fetch()) {
            static::$iblockList[$item['NAME']] = $item;
        }

        return static::$iblockList;
    }
}
