<?php

namespace Local\SymfonyTools\Router\Exceptions;

use Local\SymfonyTools\Framework\Exceptions\BaseException;

/**
 * Class ArgumentsLoaderRoutesException
 * Arguments error исключения LoaderRoutes.
 * @package Local\Router\Exceptions
 *
 * @since 07.09.2020
 */
class ArgumentsLoaderRoutesException extends BaseException
{

}
