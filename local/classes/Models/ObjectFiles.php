<?php

namespace Local\Models;

use Arrilot\BitrixModels\Models\ElementModel;

/**
 * Class ObjectFiles
 * Модель инфоблока "Файлы объектов"
 * @package Local\Models
 */
class ObjectFiles extends ElementModel
{
    /**
     * Corresponding iblock id.
     *
     * @return int
     */
    const IBLOCK_ID = 2;

}
