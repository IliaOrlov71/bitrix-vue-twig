<?php

namespace Local\Models;

use Arrilot\BitrixModels\Models\SectionModel;

/**
 * Class RentalObjects
 * Модель инфоблока "Аренда". Подразделы.
 * @package Local\Models
 */
class RentalObjects extends SectionModel
{
    /**
     * Corresponding iblock id.
     *
     * @return int
     */
    const IBLOCK_ID = 1;

    /**
     * ID раздела по символьному коду.
     *
     * @param string $code Код раздела.
     *
     * @return integer
     */
    public static function idByCode(string $code) : int
    {
        if (!$code) {
            return 0;
        }

        $id = 0;

        $model = RentalObjects::query()
            ->filter(['ACTIVE' => 'Y', 'CODE' => $code])
            ->sort(['SORT' => 'DESC'])
            ->select('ID')
            ->getList();

        if ($model->isNotEmpty()) {
            $array = current($model->toArray());
            $id = $array['ID'];
        }

        return $id;
    }

    /**
     * ID раздела привязки к подразделу документов по ID объекта.
     *
     * @param integer $id ID объекта.
     *
     * @return integer
     */
    public static function idDocs(int $id) : int
    {
        if (!$id) {
            return 0;
        }

        $idDocs = 0;

        $model = RentalObjects::query()
            ->filter(['ACTIVE' => 'Y', 'ID' => $id])
            ->sort(['SORT' => 'DESC'])
            ->select('ID', 'UF_DOCS')
            ->getList();

        if ($model->isNotEmpty()) {
            $array = current($model->toArray());
            $idDocs = (int)$array['UF_DOCS'];
        }

        return $idDocs;
    }
}
