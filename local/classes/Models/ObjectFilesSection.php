<?php

namespace Local\Models;

use Arrilot\BitrixModels\Models\SectionModel;

/**
 * Class ObjectFilesSection
 * Модель подразделов инфоблока "Файлы объектов".
 * @package Local\Models
 */
class ObjectFilesSection extends SectionModel
{
    /**
     * Corresponding iblock id.
     *
     * @return int
     */
    const IBLOCK_ID = 2;

}
