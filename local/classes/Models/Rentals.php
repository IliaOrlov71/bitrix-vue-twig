<?php

namespace Local\Models;

use Arrilot\BitrixModels\Models\ElementModel;
use Arrilot\BitrixModels\Queries\BaseQuery;

/**
 * Class Rentals
 * Модель инфоблока "Аренда"
 * @package Local\Models
 */
class Rentals extends ElementModel
{
    /**
     * Corresponding iblock id.
     *
     * @return int
     */
    const IBLOCK_ID = 1;

    /** @var array Поля для подмеса в модель. */
    protected $appends = ['UF_ADDRESS'];

    /**
     * Искать элементы только в активных подразделах.
     *
     * @param BaseQuery $query Запрос.
     *
     * @return BaseQuery
     */
    public function scopeActiveCategory($query): BaseQuery
    {
        $obRentalModel = RentalObjects::query()
            ->filter(['ACTIVE' => 'Y'])
            ->select('ID')
            ->getList();

        if ($obRentalModel) {
            $query->filter['IBLOCK_SECTION_ID'] = $obRentalModel->pluck('ID')->toArray();
        }

        return $query;
    }

    /**
     * Подмес в модель значения UF_ADDRESS из родительского подраздела.
     *
     * @return mixed
     */
    public function getUfAddressAttribute()
    {
        $obRentalModel = RentalObjects::query()
            ->filter(['ACTIVE' => 'Y', 'ID' => $this->fields['IBLOCK_SECTION_ID']])
            ->sort(['SORT' => 'DESC'])
            ->select('ID', 'UF_ADDRESS')
            ->getList()
            ->first();

        if ($obRentalModel === null) {
            return '';
        }

        $arData = $obRentalModel->toArray();

        return !empty($arData['UF_ADDRESS']) ? $arData['UF_ADDRESS'] : '';
    }
}
