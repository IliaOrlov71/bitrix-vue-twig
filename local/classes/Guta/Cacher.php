<?php

namespace Local\Guta;

use CPHPCache;
use Exception;
use Local\Constants;
use Local\Facades\KernelFacade;

/**
 * Class Cacher
 * Кэширование.
 * @package Local\Guta
 *
 * @since 22.10.2020 Доработка.
 */
class Cacher
{
    /**
     * @var CPHPCache $cacheHandler Обработчик кэша.
     */
    protected $cacheHandler;

    /**
     * @var string ID кэша.
     */
    protected $cacheId;

    /**
     * @var callable $callback Обработчик, он же получатель данных.
     */
    protected $callback;

    /**
     * @var array $timeSeconds Параметры обработчика.
     */
    protected $arCallbackParams;

    /**
     * @var integer $timeSeconds Время жизни кэша.
     */
    protected $timeSeconds;

    /**
     * @var string $currentUrl Текущий URL.
     */
    protected $currentUrl;

    /**
     * @var string $baseDir Базовая директория кэша (подпапка в bitrix/cache).
     */
    protected $baseDir = '';

    /**
     * Cacher constructor.
     *
     * @param CPHPCache $cacheHandler     Обработчик кэша.
     * @param string    $cacheId          Ключ кэша.
     * @param mixed     $callback         Callback функция.
     * @param array     $arCallbackParams Параметры callback функции.
     * @param integer   $timeSeconds      Время кэширования.
     * @param string    $currentUrl       Текущий URL.
     */
    public function __construct(
        CPHPCache $cacheHandler,
        string $cacheId = '',
        $callback = null,
        array $arCallbackParams = [],
        int $timeSeconds = Constants::SECONDS_IN_WEEK,
        string $currentUrl = ''
    ) {
        $this->cacheHandler = $cacheHandler;
        $this->currentUrl = $currentUrl;

        // ID кэша формируется из переданного и соли от callback и параметров.
        $this->cacheId = $cacheId . md5($callback) . $this->hashCache($arCallbackParams);

        $this->callback = $callback;
        $this->arCallbackParams = $arCallbackParams;

        // Отрубить кэш для окружения dev.
        /** @noinspection PhpUndefinedMethodInspection */
        $this->timeSeconds = KernelFacade::isProduction() ? $timeSeconds : 0 ;
    }

    /**
     * Фасад.
     *
     * @param string  $cacheId          Ключ кэша.
     * @param mixed   $callback         Callback функция.
     * @param array   $arCallbackParams Параметры callback функции.
     * @param integer $timeSeconds      Время кэширования.
     * @param string  $currentUrl       Текущий URL.
     *
     * @return mixed
     */
    public static function cacheFacade(
        string $cacheId,
        $callback,
        array $arCallbackParams = [],
        int $timeSeconds = 86400,
        string $currentUrl = ''
    ) {
        $instance = new static(
            new CPHPCache(),
            $cacheId,
            $callback,
            $arCallbackParams,
            $timeSeconds,
            $currentUrl
        );

        return $instance->returnResultCache();
    }

    /**
     * @return array|mixed
     * @throws Exception
     */
    public function returnResultCache()
    {
        /** Результат. */
        $arResult = [];

        $cachePath = '/' . SITE_ID . '/' . $this->baseDir . $this->cacheId;

        if ($this->cacheHandler->InitCache($this->timeSeconds, $this->cacheId, $cachePath)) {
            $vars = $this->cacheHandler->GetVars();
            $arResult = $vars['result'];
        } elseif ($this->cacheHandler->StartDataCache()) {
            $callback = $this->callback;
            try {
                $arResult = $callback(...$this->arCallbackParams);
            } catch (Exception $e) {
                $this->cacheHandler->AbortDataCache();
                $exceptionClass = get_class($e);
                throw new $exceptionClass($e->getMessage());
            }

            $this->cacheHandler->EndDataCache(['result' => $arResult]);
        }

        return $arResult;
    }

    /**
     * ID кэша.
     *
     * @param string $cacheId ID кэша.
     *
     * @return Cacher
     */
    public function setCacheId(string $cacheId): Cacher
    {
        $this->cacheId = $cacheId;

        return $this;
    }

    /**
     * Callback.
     *
     * @param callable $callback Callback.
     *
     * @return Cacher
     */
    public function setCallback(callable $callback): Cacher
    {
        $this->callback = $callback;

        return $this;
    }

    /**
     * Параметры callback.
     *
     * @return Cacher
     */
    public function setCallbackParams(): Cacher
    {
        $this->arCallbackParams = func_get_args();

        return $this;
    }

    /**
     * Базовая директория кэша.
     *
     * @param string $baseDir Базовая директория кэша.
     *
     * @return Cacher
     *
     * @since 22.10.2020
     */
    public function setBaseDir(string $baseDir): Cacher
    {
        $this->baseDir = $baseDir;

        return $this;
    }

    /**
     * Время кэширования.
     *
     * @param integer $timeSeconds Время в секундах.
     *
     * @return Cacher
     */
    public function setTTL(int $timeSeconds): Cacher
    {
        $this->timeSeconds = $timeSeconds;

        return $this;
    }


    /**
     * Задать текущий URL.
     *
     * @param string $currentUrl Текущий URL.
     *
     * @return Cacher
     */
    public function setCurrentUrl(string $currentUrl): Cacher
    {
        $this->currentUrl = $currentUrl;

        return $this;
    }

    /**
     * Salt кэша.
     *
     * @param array $arParams Параметры callback.
     *
     * @return string
     */
    protected function hashCache(array $arParams = []) : string
    {
        return md5(serialize($arParams));
    }
}
