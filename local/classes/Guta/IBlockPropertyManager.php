<?php
/**
 * Created by PhpStorm.
 * User: zakusilo.dv
 * Date: 18.10.2018
 * Time: 12:58
 */

namespace Local\Guta;

/**
 * Class IBlockPropertyManager - менеджер свойств инфоблоков
 * @package Guta\Manager
 */

class IBlockPropertyManager
{

    /** Метод возвращает свойство типа - список с XML_ID
     * @param array $arParams - параметры с типом и кодом инфоблока
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */

    public static function getPropertyEnumListByCode($arParams = array('IBLOCK_ID', 'PROPERTY_CODE'))
    {

        $arPropEnumList = array();

        $arO = array('ID' => 'ASC');
        $arF = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'CODE' => $arParams['PROPERTY_CODE']);

        $rs = \CIBlockPropertyEnum::GetList($arO, $arF);

        while ($ob = $rs->Fetch()) {
            $arPropEnumList[$ob['ID']] = $ob;
        }

        return $arPropEnumList;
    }


    /**
     * Метод возвращает свойство типа - список с XML_ID из кэша
     * @param string $iblockID      ID инфоблока.
     * @param string $sPropertyCode Код свойства инфоблока.
     * @return integer
     */
    public static function getPropertyEnumListByCodeCached($iblockID, $sPropertyCode)
    {
        return $res = CacheManager::returnResultCache(
            $iblockID . $sPropertyCode,
            array('\Local\Guta\IBlockPropertyManager', 'getPropertyEnumListByCode'),
            array('IBLOCK_ID' => $iblockID, 'PROPERTY_CODE' => $sPropertyCode)
        );
    }

    /** Метод возвращает все свойства ИБ.
     * @param array $arParams - параметры ID инфоблока
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getPropertiesIblockID($arParams = array('IBLOCK_ID'))
    {

        $arRes = array();
        $properties = \CIBlockProperty::GetList(
            array('sort' => 'asc'),
            array('IBLOCK_ID' => $arParams['IBLOCK_ID'])
        );

        while ($prop_fields = $properties->GetNext()) {
            $arRes[$prop_fields['CODE']] = $prop_fields;
        }
        return $arRes;
    }

    /**
     * Метод возвращает все свойства ИБ из кэша
     * @param string $iblockID - ID инфоблока
     * @return integer
     */
    public static function getPropertiesIblockIDCached($iblockID)
    {
        return $res = CacheManager::returnResultCache(
            $iblockID . '_getPropertiesIblockID',
            array('\Local\Guta\IBlockPropertyManager', 'getPropertiesIblockID'),
            array('IBLOCK_ID' => $iblockID)
        );
    }
}
