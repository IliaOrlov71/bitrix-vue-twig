<?php
/**
 * Created by PhpStorm.
 * User: zakusilo.dv
 * Date: 18.10.2018
 * Time: 12:58
 */

namespace Local\Guta;

use CFile;
use CIBlock;

/**
 * Class IBlockManager - менеджер инфоблоков
 * @package Guta\Manager
 */

class IBlockManager
{

    /** Метод возвращает ID инфоблока
     * @param array $arParams Параметры с типом и кодом инфоблока.
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getIBlockIdByCode($arParams = array('TYPE', 'CODE'))
    {

        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $res = \CIBlock::GetList(
                array(),
                array('ACTIVE' => 'Y', 'TYPE' => $arParams['TYPE'], 'CODE' => $arParams['CODE'])
            );
            $arResult = $res->Fetch();
            if ($arResult['ID'] > 0) {
                return $arResult['ID'];
            } else {
                throw new \Bitrix\Main\ArgumentException('Инфоблок ' . $arParams['CODE'] . ' не найден', $arParams['CODE']);
            }
        }
    }

    /**
     * Метод возвращает ID инфоблока по его коду из кэша
     * @param string $sIBlockType - тип инфоблока
     * @param string $sIBlockCode - код инфоблока
     *
     * @return integer
     */
    public static function getIBlockIdByCodeCached($sIBlockType, $sIBlockCode)
    {
        return $res = CacheManager::returnResultCache(
            $sIBlockType . $sIBlockCode,
            array('\Local\Guta\IBlockManager', 'getIBlockIdByCode'),
            array('TYPE' => $sIBlockType, 'CODE' => $sIBlockCode)
        );
    }

    /**
     * Метод возвращает описание инфоблока
     * @param array $arParams
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getIBlockDescriptionByCode($arParams = array('TYPE', 'CODE'))
    {

        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $res = \CIBlock::GetList(
                array(),
                array('ACTIVE' => 'Y', 'TYPE' => $arParams['TYPE'], 'CODE' => $arParams['CODE'])
            );
            $arResult = $res->Fetch();
            if ($arResult['ID'] > 0) {
                return $arResult['DESCRIPTION'];
            } else {
                throw new \Bitrix\Main\ArgumentException('Инфоблок ' . $arParams['CODE'] . ' не найден', $arParams['CODE']);
            }
        }
    }

    /**
     * Описание инфоблока по ID.
     *
     * @param array $arParams [ID] - ID инфоблока.
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getIBlockDescriptionById($arParams = ['ID'])
    {
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $res = \CIBlock::GetList(
                [],
                ['ACTIVE' => 'Y', 'ID' => $arParams['ID']]
            );
            $arResult = $res->Fetch();
            if ($arResult['ID'] > 0) {
                return $arResult['DESCRIPTION'];
            } else {
                throw new \Bitrix\Main\ArgumentException('Инфоблок ' . $arParams['ID'] . ' не найден', $arParams['CODE']);
            }
        }
    }


    /**
     * Кэшированный ответ - URL инфоблока по коду.
     *
     * @param string $sIBlockType Тип инфоблока.
     * @param string $sIBlockCode Код инфоблока.
     *
     * @return mixed
     */
    public static function getIblockUrlByCodeCached($sIBlockType, $sIBlockCode)
    {
        return $res = CacheManager::returnResultCache(
            'iblockurl'.$sIBlockType . $sIBlockCode,
            array('\Local\Guta\IBlockManager', 'getIblockUrlByCode'),
            array('TYPE' => $sIBlockType, 'CODE' => $sIBlockCode)
        );
    }

    /**
     * Получить URL инфоблока.
     *
     * @param array $arParams
     *
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getIblockUrlByCode($arParams = array('TYPE', 'CODE'))
    {
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $res = \CIBlock::GetList(
                array(),
                array('ACTIVE' => 'Y', 'TYPE' => $arParams['TYPE'], 'CODE' => $arParams['CODE'])
            );
            $arResult = $res->Fetch();

            if ($arResult['ID'] > 0) {
                return str_replace('#SITE_DIR#', '', $arResult['LIST_PAGE_URL']);
            } else {
                throw new \Bitrix\Main\ArgumentException('Инфоблок ' . $arParams['CODE'] . ' не найден', $arParams['CODE']);
            }
        }
    }

    /**
     * Метод возвращает описание инфоблока по его коду из кэша
     * @param string $sIBlockType - тип инфоблока
     * @param string $sIBlockCode - код инфоблока
     * @return integer
     */
    public static function getIBlockDescriptionByCodeCached($sIBlockType, $sIBlockCode)
    {
        return $res = CacheManager::returnResultCache(
            'iblockDescription'.$sIBlockType . $sIBlockCode,
            array('\Local\Guta\IBlockManager', 'getIBlockDescriptionByCode'),
            array('TYPE' => $sIBlockType, 'CODE' => $sIBlockCode)
        );
    }

    /**
     * Название инфоблока по коду.
     *
     * @param int $idIblock
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getIBlockNameById(int $idIblock)
    {

        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $res = \CIBlock::GetList(
                [], ['ACTIVE' => 'Y', 'ID' => $idIblock]
            );
            $arResult = $res->Fetch();
            if ($arResult['ID'] > 0) {
                return $arResult['NAME'];
            } else {
                throw new \Bitrix\Main\ArgumentException('Инфоблок не найден', $idIblock);
            }
        }
    }

    /**
 * Получить картинку инфоблока.
 *
 * @param integer $iblockId ID инфоблока.
 *
 * @return string
 */
    public static function getImageIB(int $iblockId) : string
    {
        $iPictureId = CIBlock::GetArrayByID($iblockId, 'PICTURE');
        $sUrlPicture = CFile::GetPath($iPictureId);

        return $sUrlPicture ?: '';
    }

    /**
     * Получить ID картинки инфоблока.
     *
     * @param integer $iblockId ID инфоблока.
     *
     * @return integer
     */
    public static function getImageIbId(int $iblockId) : int
    {
        $iPictureId = CIBlock::GetArrayByID($iblockId, 'PICTURE');

        return $iPictureId ?: 0;
    }
}
