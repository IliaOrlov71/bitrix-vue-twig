<?php
/**
 * Created by PhpStorm.
 * User: Grudtsina.EG
 * Date: 28.02.2019
 * Time: 10:22
 */

namespace Local\Guta;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use CForm;

/**
 * Class IBlockManager - менеджер форм
 * @package Guta\Manager
 */
class FormManager
{
    /** @const string URL проверки Google Captcha re2 на валидность. */
    public const GOOGLE_CAPTCHA_VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';
    /** @const string Google Secret Site Key. */
    public const GOOGLE_CAPTCHA_SECRET_KEY = '6LcS3K0UAAAAAKTn4Odrs17pLEAMiaVuLJOToPbl';
    /** @const string Google Site Key. */
    public const GOOGLE_CAPTCHA_SITE_KEY = '6LcS3K0UAAAAAODPNv_DHGHABdhBg2C_Ubqbwdzg';

    /** Метод возвращает ID формы
     *
     * @param array $arParams Параметры с символьным кодом формы.
     *
     * @return integer
     * @throws ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getFormIdBySID(array $arParams = ['FORM_SID']) : int
    {
        if (Loader::includeModule('form')) {
            $FORM_SID = $arParams['FORM_SID'];
            $rsForm = \CForm::GetBySID($FORM_SID);
            $arResult = $rsForm->Fetch();

            if ($arResult['ID'] > 0) {
                return (int)$arResult['ID'];
            }
        }

        return 0;
    }

    /**
     * Метод возвращает ID формы по его коду из кэша.
     *
     * @param string $sFormSid Символьный код формы.
     *
     * @return integer
     */
    public static function getFormIdBySIDCached(string $sFormSid = '')
    {
        return (int)$res = CacheManager::returnResultCache(
            $sFormSid,
            ['\Local\Guta\FormManager', 'getFormIdBySID'],
            ['FORM_SID' => $sFormSid]
        );
    }

    /** Метод возвращает ответы формы (не учитывает множественные вопросы!).
     *
     * @param array $arParams Параметры с символьным кодом формы.
     *
     * @return array
     * @throws ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getFormAnswers(array $arParams = ['FORM_SID']) : array
    {

        $arAnswerForm = [];
        if (Loader::includeModule('form')) {
            $formID = FormManager::getFormIdBySIDCached($arParams['FORM_SID']);
            $is_filtered = false;

            $rsQuestions = \CFormField::GetList($formID, 'N', $by = 's_id', $order = 'asc', array(), $is_filtered);

            while ($arQuestion = $rsQuestions->Fetch()) {
                $QUESTION_ID = $arQuestion['ID']; // ID вопроса

                // получим ответ
                $rsAnswers = \CFormAnswer::GetList(
                    $QUESTION_ID,
                    $by = 's_id',
                    $order = 'desc',
                    [],
                    $is_filtered
                );

                if ($arAnswer = $rsAnswers->Fetch()) {
                    $arAnswerForm[$arQuestion['SID']] = [
                        'QUESTION' => $arQuestion,
                        'ANSWER' => $arAnswer
                    ];
                }
            }
        }


        return $arAnswerForm;
    }

    /**
     * Получить все ответы по коду вопроса.
     *
     * @param string $sFormCode     Символьный код формы.
     * @param string $sCodeQuestion Символьный код вопроса.
     *
     * @return array|mixed
     */
    public static function getAllAnswersByIdQuestion(string $sFormCode, string $sCodeQuestion)
    {
        try {
            $idForm = self::getFormIdBySID(['FORM_SID' => $sFormCode]);
        } catch (ArgumentException | LoaderException $e) {
            return [];
        }

        $arForm = CForm::GetDataByID(
            $idForm,
            $form,
            $questions,
            $answers,
            $dropdown,
            $multiselect
        );

        if ($arForm) {
               return $answers[$sCodeQuestion];
        }

        return [];
    }

    /**
     * Метод возвращает ответы формы.
     *
     * @param string $sFormSid Символьный код формы.
     *
     * @return array
     */
    public static function getFormAnswersCached(string $sFormSid = '')
    {
        return $res = CacheManager::returnResultCache(
            $sFormSid . '_getFormAnswers',
            ['\Local\Guta\FormManager', 'getFormAnswers'],
            ['FORM_SID' => $sFormSid]
        );
    }
}
