<?php

namespace Local\Guta;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;

/**
 * Class HLIBlockElementManager - менеджер элементов Highload инфоблоков
 * @package Guta\Manager
 */
class HLIBlockElementManager
{
    /** Метод возвращает результат выборки из HL ИБ
     *
     * @param string $sHLIblockCode Код HL блока.
     * @param array  $arParams      Параметры выборки.
     *
     * @return mixed
     */
    public static function getIBlockElements(
        string $sHLIblockCode,
        array $arParams = [
            'AR_ORDER',
            'AR_FILTER',
            'AR_GROUP',
            'AR_SELECT'
        ]
    ) : array {

        // Если не передали код HL блока, то прекращаем работу.
        if (!$sHLIblockCode) {
            return [];
        }

        /** @var $arDefValues $arDefValues  Значения по-умолчанию. */
        $arDefValues = [
            'AR_ORDER' => ['ID' => 'ASC'],
            'AR_FILTER' => [],
            'AR_SELECT' => ['*'],
        ];
        $arParams = array_merge($arDefValues, $arParams);

        try {
            $sHlClassName = self::getHLBlockClassByCode($sHLIblockCode);
        } catch (ObjectPropertyException | ArgumentException | SystemException $e) {
            return [];
        }

        try {
            if (Loader::includeModule('highloadblock')) {
                $obData = $sHlClassName::getList(
                    [
                        'select' => $arParams['AR_SELECT'],
                        'order' => $arParams['AR_ORDER'],
                        'filter' => $arParams['AR_FILTER'],
                    ]
                );

                /** @var array $arResult Результат. */
                $arResult = [];

                while ($arData = $obData->fetch()) {
                    $idElement = $arData['ID'];
                    $arResult[$idElement] = $arData;
                }

                return $arResult;
            }
        } catch (LoaderException $e) {
            return [];
        }

        return [];
    }

    /**
     * Получить название класса HL блока по его коду
     *
     * @param string $sCodeHLblock Код HL блока.
     *
     * @return string | boolean
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    private static function getHLBlockClassByCode(string $sCodeHLblock) : ?string
    {
        $obHlblock = HighloadBlockTable::getList(
            [
                'filter' => ['=NAME' => $sCodeHLblock],
            ]
        )->fetch();

        if (!$obHlblock) {
            $arResult['TERMS_DESCRIPTION'] = [];
        }

        try {
            $sHlClassName = (HighloadBlockTable::compileEntity($obHlblock))->getDataClass();
        } catch (SystemException $e) {
            return false;
        }

        return $sHlClassName;
    }

    /**
     * Метод возвращает кэшированный результат выборки
     * из HL блока.
     *
     * @param string $sCodeHLblock Код HL блока.
     * @param array  $arParams     Параметры с типом и кодом инфоблока.
     *
     * @return mixed
     */
    public static function getIBlockElementsCached(
        string $sCodeHLblock,
        array $arParams = ['AR_ORDER', 'AR_FILTER', 'AR_SELECT']
    ) : array {

        // Ключ кэша. Учитывается код HL блока.
        $sKeyCache = $sCodeHLblock.serialize(array_values($arParams));

        return $res = CacheManager::returnResultCache(
            $sKeyCache,
            ['\Local\Guta\HLIBlockElementManager', 'getIBlockElements'],
            $sCodeHLblock,
            $arParams
        );
    }
}
