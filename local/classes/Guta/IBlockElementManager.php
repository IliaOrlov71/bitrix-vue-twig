<?php
/**
 * Created by PhpStorm.
 * User: zakusilo.dv
 * Date: 18.10.2018
 * Time: 12:58
 */

namespace Local\Guta;

use Bitrix\Main\DB\Exception;
use CIBlock;
use CIBlockElement;
use CIBlockPropertyEnum;

/**
 * Class IBlockElementManager
 * @package Local\Guta
 */
class IBlockElementManager
{

    /**
     * Метод возвращает массив возможных значений свойства $sPropertyCode.
     * @param string $sBlockCode    Символьный код инфоблока.
     * @param string $sBlockType    Символьный тип инфоблока.
     * @param string $sPropertyCode Символьный код свойства инфоблока.
     * @return array.
     * @throws
     */
    public static function getAllPropertyValues(string $sBlockCode, string $sBlockType, string $sPropertyCode): array
    {
        $arResult = [];
        $iBlockId = IBlockManager::getIBlockIdByCode(array('TYPE' => $sBlockType, 'CODE' => $sBlockCode));

        $properties = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => $iBlockId, 'CODE' => $sPropertyCode));

        while ($property = $properties->GetNext()) {
            $arResult[$property['ID']] = $property['VALUE'];
        }

        return $arResult;
    }

    /** Метод возвращает результат выборки из ИБ
     * @param array   $arParams           Параметры с типом и кодом инфоблока.
     * @param boolean $bDisableProperties Не добавлять в массив свойства элемента.
     * @return mixed
     * @throws \Bitrix\Main\LoaderException Error.
     */
    public static function getIBlockElements(
        array $arParams = array(
            'AR_ORDER',
            'AR_FILTER',
            'NAV_PARAMS',
            'AR_GROUP',
            'AR_SELECT'
        ),
        bool $bDisableProperties = false
    ) : array {
        /** @var $arDefValues $arDefValues  Значения по-умолчанию. */
        $arDefValues = array(
            'AR_ORDER' => array('SORT' => 'ASC'),
            'AR_FILTER' => array(),
            'AR_GROUP' => false,
            'NAV_PARAMS' => false,
            'AR_SELECT' => array('*'),
        );
        $arParams = array_merge($arDefValues, $arParams);
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $res = \CIBlockElement::GetList(
                $arParams['AR_ORDER'],
                $arParams['AR_FILTER'],
                $arParams['AR_GROUP'],
                $arParams['NAV_PARAMS'],
                $arParams['AR_SELECT']
            );

            $i = 0;
            $arResult = array();
            while ($ob = $res->GetNextElement()) {
                $id = $ob->fields['ID'] ?? $i++;
                $arResult[$id] = $ob->fields;
                $arResult[$id]['PROPERTIES'] = $bDisableProperties ?? $ob->GetProperties();
            }
            return $arResult;
        }
    }

    /**
     * Метод возвращает кэшированный результат выборки
     * @param array   $arParams           Параметры с типом и кодом инфоблока.
     * @param boolean $bDisableProperties Не добавлять в массив свойства элемента.
     * @return mixed
     */
    public static function getIBlockElementsCached(
        array $arParams = ['AR_ORDER', 'AR_FILTER', 'NAV_PARAMS', 'AR_GROUP', 'AR_SELECT'],
        bool $bDisableProperties = false
    ) : array {
        return $res = CacheManager::returnResultCache(
            md5(serialize(array_values($arParams))),
            array('\Local\Guta\IBlockElementManager', 'getIBlockElements'),
            $arParams
        );
    }

    /**
     * Узнать версмю инфоблока
     * @param integer $iblockID Ид инфоблока.
     * @return integer|null
     */
    public static function getVersionInfoblock(int $iblockID)
    {
        $res = CIBlock::GetList(
            [],
            [
                'TYPE' => 'content',
                'SITE_ID' => SITE_ID,
                'ACTIVE' => 'Y',
                'CNT_ACTIVE' => 'Y',
                'ID' => $iblockID,
            ],
            true
        );
        if ($arRes = $res->Fetch()) {
            return (int)$arRes['VERSION'];
        }

        return null;
    }

    /**
     * Первый элемент инфоблока
     * @param integer $iblockID ID инфоблока.
     * @return integer
     */
    public static function getFirstElementOfIblock(int $iblockID) : int
    {

        $link = CIBlockElement::GetList(
            [],
            ['ACTIVE' => 'Y', 'IBLOCK_ID' => $iblockID],
            false,
            false,
            ['ID']
        );
        if ($arElement = $link->Fetch()) {
            return $arElement['ID'];
        }

        return 0;
    }
}
