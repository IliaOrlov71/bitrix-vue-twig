<?php
/**
 * Created by PhpStorm.
 * User: zakusilo.dv
 * Date: 18.10.2018
 * Time: 12:58
 */

namespace Local\Guta;

/**
 * Class IBlockSectionManager
 * @package Local\Guta
 */
class IBlockSectionManager
{

    /**
     * Метод возвращает имя раздела инфоблока или пустую строку.
     * @param integer $iSectionID ИД раздела инфоблока.
     *
     * @return string
     */
    public static function getSBlockSectionNameByID(int $iSectionID): string
    {
        $obBlockResult = \CIBlockSection::GetByID($iSectionID);

        if ($arSection = $obBlockResult->GetNext()) {
            return $arSection['NAME'];
        }

        return '';
    }

    /** Метод возвращает ID инфоблока
     * @param array $arParams Параметры с типом и кодом инфоблока.
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getIBlockSectionIdByCode($arParams = array('IBLOCK_CODE', 'CODE', 'AR_SELECT'))
    {

        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $res = \CIBlockSection::GetList(
                array('SORT' => 'ASC'),
                array(
                    'IBLOCK_CODE'   => $arParams['IBLOCK_CODE'],
                    'CODE'          => $arParams['CODE'],
                ),
                false,
                $arParams['AR_SELECT'],
                false
            );

            $arResult = $res->GetNext();
            if ($arResult['ID'] > 0) {
                //если есть картинка, взять путь к ней
                if ($arResult['PICTURE'] > 0) {
                    $arResult['PICTURE'] = \CFile::GetFileArray($arResult['PICTURE']);
                }

                return $arResult;
            } else {
                throw new \Bitrix\Main\ArgumentException(
                    'Раздел в инфоблоке ' . $arParams['IBLOCK_CODE'] . ' не найден',
                    $arParams['CODE']
                );
            }
        }
    }

    /**
     * Метод возвращает ID секции по его коду из кэша.
     * @param string $sIBlockCode        Код инфоблока.
     * @param string $sIBlockSectionCode Код раздела инфоблока.
     * @param array  $arSelect           Массив возвращаемых полей.
     * @return integer
     */
    public static function getIBlockSectionIdByCodeCodeCached(
        string $sIBlockCode,
        string $sIBlockSectionCode,
        array $arSelect = array('ID')
    ) {

        return CacheManager::returnResultCache(
            md5($sIBlockCode . $sIBlockSectionCode . serialize(array_keys($arSelect))),
            array('\Local\Guta\IBlockSectionManager', 'getIBlockSectionIdByCode'),
            array('IBLOCK_CODE' => $sIBlockCode, 'CODE' => $sIBlockSectionCode, 'AR_SELECT' => $arSelect)
        );
    }

    /**
     * Элементы подраздела по ID.
     *
     * @param array $arParams
     * @return array
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getSectionsItemsByCode($arParams = ['IBLOCK_CODE', 'ID', 'AR_SORT', 'AR_SELECT'])
    {
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $res = \CIBlockSection::GetList(
                [],
                [
                    'IBLOCK_CODE'   => $arParams['IBLOCK_CODE'],
                    'ID'          => $arParams['ID']
                ],
                false,
                [],
                false
            );

            if ($obRes = $res->GetNext()) {
                $iblockId = $obRes['IBLOCK_ID'];
                $idSection = $obRes['ID'];
            } else {
                return [];
            }

            $arSort = (!empty($arParams['AR_SORT'])) ? $arParams['AR_SORT'] : ['SORT' => 'ASC'];
            $arSelect = (!empty($arParams['AR_SELECT'])) ? $arParams['AR_SELECT'] : [];

            $res = \CIBlockElement::GetList(
                $arSort,
                [
                    'IBLOCK_ID' => $iblockId,
                    'SECTION_ID' => $idSection,
                    'ACTIVE' => 'Y'
                ],
                false,
                false,
                $arSelect
            );

            $arResult = [];

            while ($ob = $res->GetNext()) {
                $arResult[$ob['ID']] = $ob;
            }

            return $arResult;
        }

        return [];
    }
}
