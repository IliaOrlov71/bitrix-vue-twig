<?php

namespace Local\Bitrix\AdminPanel\Tabs;

use CIBlockElement;
use Exception;

/**
 * Class DoctorMisInfoAdminTab
 * @package Local\Bitrix\AdminPanel\Tabs
 *
 * @since 04.02.2021
 */
class DoctorMisInfoAdminTab extends AbstractAdminPanelTabExtender
{
    /**
     * @inheritDoc
     * @throws Exception Ошибки Твига.
     */
    public function showTabContent(string $div, array $elementInfo, $formData) : void
    {
        $data = CIBlockElement::GetByID($elementInfo['ID'])->GetNext();

        echo $this->twig->render(
            $this->template,
            [
                'id' => $elementInfo['ID'],
                'fio' => $data ? $data['NAME'] : '',

            ]);
    }
}
