<?php

namespace Local\Bitrix\AdminPanel\Tabs;

/**
 * Interface AdminPanelExtenderInterface
 * @package Local\Bitrix\AdminPanel\Tabs
 *
 * @since 04.02.2021
 */
interface AdminPanelExtenderInterface
{
    /**
     * Подходящий элемент для вывода таба?
     *
     * @param array $elementInfo Сведения о элементе.
     *
     * @return boolean
     */
    public function valid(array $elementInfo) : bool;

    /**
     * Параметры будущей вкладки.
     *
     * @param array $elementInfo Сведения о элементе.
     *
     * @return array|null
     */
    public function getTabList(array $elementInfo): ?array;

    /**
     * Вывод контента вкладки.
     *
     * @param string $div         DIV для верстки.
     * @param array  $elementInfo Сведения о элементе.
     * @param mixed  $formData    Не знаю, что это такое.
     *
     * @return void
     */
    public function showTabContent(string $div, array $elementInfo, $formData) : void;

    /**
     *
     * @return boolean
     */
    public function check() : bool;

    /**
     * @return boolean
     */
    public function action() : bool;

    /**
     * @return string
     */
    public function getCodeTab(): string;
}
