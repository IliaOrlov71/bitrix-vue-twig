<?php

namespace Local\Bitrix\AdminPanel\Tabs;

use Bitrix\Main\Context;
use CIBlockElement;
use CJSCore;
use Twig\Environment;

/**
 * Class AbstractAdminPanelTabExtender
 * @package Local\Bitrix\AdminPanel\Tabs
 *
 * @since 05.02.2021
 */
abstract class AbstractAdminPanelTabExtender implements AdminPanelExtenderInterface
{
    /**
     * @var Environment $twig Твиг.
     */
    protected $twig;

    /**
     * @var integer $idIblock ID инфоблока.
     */
    protected $idIblock;

    /**
     * @var string $codeTab Код таба.
     */
    protected $codeTab;

    /**
     * @var string $tabName Название таба.
     */
    protected $tabName;

    /**
     * @var string $tabTitle Заголовок таба.
     */
    protected $tabTitle;

    /**
     * @var string $template Твиговский шаблон.
     */
    protected $template;

    /**
     * DoctorMisInfo constructor.
     *
     * @param Environment $twig     Твиг.
     * @param integer     $idIblock ID инфоблока.
     * @param string      $codeTab  Код таба.
     * @param string      $tabName  Название таба.
     * @param string      $tabTitle Заголовок таба.
     * @param string      $template Твиговский шаблон.
     */
    public function __construct(
        Environment $twig,
        int $idIblock,
        string $codeTab,
        string $tabName,
        string $tabTitle,
        string $template
    ) {
        $this->idIblock = $idIblock;
        $this->codeTab = $codeTab;
        $this->tabName = $tabName;
        $this->tabTitle = $tabTitle;
        $this->twig = $twig;
        $this->template = $template;

        CJSCore::Init(['jquery']);
    }

    /**
     * @inheritDoc
     */
    public function valid(array $elementInfo) : bool
    {
        $request = Context::getCurrent()->getRequest();

        $addTabs = $elementInfo['ID'] > 0
            && (int)$elementInfo['IBLOCK']['ID'] === $this->idIblock
            && (!isset($request['action']) || $request['action'] !== 'copy');

        // Только для активных элементов.
        if ($addTabs) {
            $data = CIBlockElement::GetByID($elementInfo['ID'])->GetNext();
            if ($data && $data['ACTIVE'] === 'N') {
                return false;
            }
        }

        return $addTabs;
    }

    /**
     * @inheritDoc
     */
    public function getTabList(array $elementInfo): ?array
    {
        if (!$this->valid($elementInfo)) {
            return null;
        }

        return [
            [
                'DIV' => $this->codeTab,
                'SORT' => PHP_INT_MAX,
                'TAB' => $this->tabName,
                'TITLE' => $this->tabTitle,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function check() : bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function action() : bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getCodeTab(): string
    {
        return $this->codeTab;
    }
}
