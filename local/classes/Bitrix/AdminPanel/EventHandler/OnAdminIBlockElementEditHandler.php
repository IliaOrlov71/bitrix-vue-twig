<?php

namespace Local\Bitrix\AdminPanel\EventHandler;

use Local\Bitrix\AdminPanel\Tabs\AdminPanelExtenderInterface;

/**
 * Class OnAdminIBlockElementEditHandler
 * @package Local\Bitrix\AdminPanel\EventHandler
 *
 * @since 04.02.2021
 */
class OnAdminIBlockElementEditHandler
{
    /**
     * @var AdminPanelExtenderInterface[] $eventsHandlerBag Сервисы, помеченные тэгом bitrix.admin.element.tab.handler.
     */
    private $eventsHandlerBag = [];

    /**
     * OnAdminIBlockElementEditHandler constructor.
     *
     * @param mixed ...$eventsHandlerBag Сервисы, помеченные тэгом bitrix.admin.element.tab.handler.
     */
    public function __construct(...$eventsHandlerBag)
    {
        $handlers = [];

        foreach ($eventsHandlerBag as $eventHandler) {
            $iterator = $eventHandler->getIterator();
            $array = iterator_to_array($iterator);
            $handlers[] = $array;
        }

        $this->eventsHandlerBag = array_merge($this->eventsHandlerBag, ...$handlers);
    }

    /**
     * Инициализация событий.
     *
     * @return void
     */
    public function initEvent(): void
    {
        foreach ($this->eventsHandlerBag as $tabset) {
            $interfaces = class_implements($tabset);
            if (in_array(AdminPanelExtenderInterface::class, $interfaces, true)) {
                AddEventHandler(
                    'main',
                    'OnAdminIBlockElementEdit',
                    static function () use ($tabset) {
                        return [
                            'TABSET' => $tabset->getCodeTab(),
                            'Check' => [$tabset, 'check'],
                            'Action' => [$tabset, 'action'],
                            'GetTabs' => [$tabset, 'getTabList'],
                            'ShowTab' => [$tabset, 'showTabContent'],
                        ];
                    });
            }
        }
    }
}
