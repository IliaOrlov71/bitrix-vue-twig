<?php

namespace Local\Controllers;

use CHTTP;
use Local\Constants;
use Local\Models\RentalObjects;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DocumentsController
 * @package Local\Controllers
 *
 * @since 01.07.2021
 */
class DocumentsController extends AbstractController
{
    /**
     * @param Request $request Request.
     *
     * @return Response
     */
    public function action(Request $request) : Response
    {
        global $SiteExpireDate; // Убираем надпись о просрочке сайта.
        global $APPLICATION;

        $codeObject = $request->attributes->get('code', '');

        require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog.php';

        $id = RentalObjects::idByCode($codeObject);

        if (!$id) {
            $this->show404();
        }

        ob_start();

        $APPLICATION->IncludeComponent(
            'bitrix:breadcrumb',
            'bk',
            [
                'START_FROM' => '0',
            ]
        );

        $APPLICATION->IncludeComponent(
            'guta:documents',
            'modern',
            [
                'ID' => $id,
                'CACHE_TYPE' => 'A',
                'CACHE_TIME' => Constants::SECONDS_IN_MONTH,
            ],
            false
        );

        /** @psalm-suppress UnresolvableInclude */
        require($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH .'/footer.php');

        $content = ob_get_clean();

        return new Response(
            $content,
            Response::HTTP_OK,
            ['Content-Type' => 'text/html; charset=utf-8']
        );
    }

    /**
     * Показать 404 страницу.
     *
     * @return void
     */
    public function show404() : void
    {
        global $APPLICATION;

        $APPLICATION->RestartBuffer();

        CHTTP::SetStatus('404 Not Found');
        include($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/header.php');
        include($_SERVER['DOCUMENT_ROOT'].'/404.php');
        include($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/footer.php');
    }
}
