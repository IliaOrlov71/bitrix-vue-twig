<?php
/**
 * @author Zakusilo.
 * @time: 03.07.2018 16:48
 */

namespace Local\Util\Ajax;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use CForm;
use CFormAnswer;
use CFormResult;
use Local\Constants;
use Local\Guta\FormManager;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Базовый конструктор, который подключает обработчики запросов через ajax.
 * Class AjaxFactory
 * @package Local\Util\Ajax
 */
class AjaxFactory
{
    /** @var $key */
    protected $key;
    /** @var string $host */
    protected $host;
    /** @var array $data */
    protected $data = array();
    /** @var object $return */
    static $return = null;
    /** @var array $arPath */
    public $arPath = array();
    /** @var array $arClasses */
    public $arClasses = array();
    /** @var array $arDirectory */
    public $arDirectory = array();
    /** @var array $instance */
    private $instance = array();
    /** @var string $sPrefix */
    private $sPrefix = '.response.php';

    /**
     * ResponseFactory constructor.
     *
     * @param string $host        Домен, с которого пришел запрос.
     * @param array  $data        Массив с данными из запроса.
     * @param array  $arDirectory Массив с директориями, в которых будут искаться компоненты ответов на запросы.
     *
     * @return void
     */
    public function __construct(string $host, array $data = array(), array $arDirectory = array('Response'))
    {
        $this->data = $data;
        $this->host = $host;
        $this->arDirectory = $arDirectory;

        // Если требуется сохранение сессии, то необходимо указывать url домена, с которого пришел запрос
        header("Access-Control-Allow-Origin: {$host}");
        header('Access-Control-Allow-Credentials: true');
        header('Content-Type: application/json');

        $this->setInstances($arDirectory);
        spl_autoload_register(array($this, 'register_autoload'));

        self::$return = new AjaxData();
    }

    /**
     * Автоподключение классов
     * @param array $arDirectory Директории, где ищутся классы.
     * @return void
     */
    public function setInstances($arDirectory = []): void
    {
        foreach ($arDirectory as $dir) {
            $rdir = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator(__DIR__.DIRECTORY_SEPARATOR.$dir),
                true
            );

            foreach ($rdir as $file) {
                // Наполняем массив путей для автолоадера и массив доступных классов для  пулла объектов
                if (!$file->isDir()) {
                    $this->arPath[mb_strtolower($file->getbaseName($this->sPrefix))] = $file->getPath();
                    if (!isset($this->arClasses[mb_strtolower($file->getbaseName($this->sPrefix))])) {
                        $this->arClasses[mb_strtolower($file->getbaseName($this->sPrefix))] = $file->getbaseName(
                            $this->sPrefix
                        );
                    }
                }
            }
        }
    }

    /** Автозагрузчик
     *
     * @param string $sClassName Название класса.
     *
     * @return boolean
     */
    public function register_autoload(string $sClassName = ''): bool
    {
        if ($sClassName == __CLASS__) {
            return true;
        }
        foreach ($this->arPath as $item) {
            $sClassParh = $item.DIRECTORY_SEPARATOR.str_replace(__NAMESPACE__.'\\', '', $sClassName).$this->sPrefix;

            if (file_exists($sClassParh)) {
                @require_once $sClassParh;

                return true;
            }
        }

        return false;
    }

    /**
     * Метод подключает синглтоны
     *
     * @param string $sKey Имя класса.
     *
     * @return AjaxFactory
     */
    public function getInstance(string $sKey = '')
    {
        if (!empty($sKey) && empty($this->arClasses[$sKey])) {
            self::errorEnd(
                'Класс '.
                __NAMESPACE__.DIRECTORY_SEPARATOR.ucfirst($sKey).
                ' не найден в файле '.ucfirst($sKey).$this->sPrefix
            );
        }

        if (empty($sKey)) {
            self::errorEnd('Не указан компонент!');
        }

        $sKey = mb_strtolower($sKey);
        $className = __NAMESPACE__.'\\'.$this->arClasses[$sKey];

        if (!isset($this->instance[$sKey])) {
            $this->instance[$sKey] = new $className($this->host, $this->data, $this->arDirectory);
        }

        if ($this->instance[$sKey] instanceof AjaxResponseTemplate) {
            return $this->instance[$sKey];
        }

        self::errorEnd('Класс '.$this->arClasses[$sKey].' не наследует шаблон AjaxResponseTemplate');
    }

    /**
     * Выход с сообщением об ошибке.
     *
     * @param string  $sMessage      Сообщение.
     * @param integer $iResponseCode Код ответа.
     *
     * @return  void
     */
    public static function errorEnd(string $sMessage, int $iResponseCode = 500) : void
    {
        // Avoid echo of empty string (which is invalid JSON), and
        // JSONify the error message instead:
        $json = json_encode(array('jsonError', json_last_error_msg()));
        if (empty(json_last_error())) {
            // This should not happen, but we go all the way now:
            $json = "{'jsonError': '$sMessage'}";
        }
        // Set HTTP response status code to: 500 - Internal Server Error
        http_response_code($iResponseCode);

        echo $json;
        die();
    }

    /**
     * @param array $instance
     *
     * @return void
     */
    public function setInstance(array $instance): void
    {
        $this->instance = $instance;
    }

    /**
     * Метод формирует итоговый ответ на запрос
     * @return void
     */
    public function getAnswer(): void
    {
        if (!self::$return->bSuccess) {
            $sErrorMessage = !empty(self::$return->sErrorMessage) ? self::$return->sErrorMessage : 'Неизвестная ошибка';
            self::errorEnd($sErrorMessage, 400);
        }

        echo json_encode(self::$return->getData());
    }

    /**
     * Отправка почты по результатам заполнения
     * формы.
     *
     * @param integer $idResult ID результата ответа формы.
     * @return mixed
     */
    protected function mailResult(int $idResult)
    {
        if (CFormResult::Mail($idResult)) {
            return true;
        }

        global $strError;

        return $strError;
    }

    /**
     * Обезопасить массив ($_POST)
     *
     * @param array $arPost Массив а-ля $_POST.
     *
     * @return array
     */
    public static function sanitizeArray(array $arPost): array
    {
        $result = [];

        foreach ($arPost as $key => $item) {
            $result[$key] = filter_var($item, FILTER_SANITIZE_STRING);
        }

        return $result;
    }

    /**
     * Насильно прописать поле "Согласен
     * на обработку персональных данных",
     * так как данные не придут, если согласие
     * не получено.
     *
     * @param integer $resultID ID формы.
     * @param string  $sFormSid Строковый код формы.
     *
     * @return boolean
     */
    public function forceUpdateFieldPrivateAgreement(int $resultID, string $sFormSid = ''): bool
    {
        // Получаем код ответа, означающего согласие на обработку данных.
        $arAnswers = FormManager::getFormAnswersCached($sFormSid);
        $idAnswer = $arAnswers['CONFIRMATION']['QUESTION']['ID'];

        if (!$idAnswer) {
            return false;
        }

        $FIELD_SID = 'CONFIRMATION';
        $arVALUE[$idAnswer] = 'Y';

        return CFormResult::SetField($resultID, $FIELD_SID, $arVALUE);
    }


    /**
     * Создать форму с данными из $_POST
     * @param array       $data     Массив $_POST.
     *
     * @param string|null $sFormSid ID формы.
     *
     * @return array
     * @throws LoaderException
     */
    public function formCreate(array $data, string $sFormSid = '')
    {
        Loader::includeModule('form');

        $arReturn = [
            'RESULT_ID' => -1,
            'ERROR' => '',
        ];

        if (!$sFormSid) {
            $arReturn['ERROR'] = 'Не передали код формы';

            return $arReturn;
        }

        /** @var string $sFormSid Код формы подписки, в зависимости от сайта. */

        $formID = FormManager::getFormIdBySIDCached($sFormSid);
        if (!$formID) {
            $arReturn['ERROR'] = 'Форма не найдена.';

            return $arReturn;
        }

        $arDataForm = [];

        // Получим ID вопросов и ответов для сохранения формы.
        CForm::GetResultAnswerArray(
            $formID,
            $arQuestions,
            $arAnswers,
            $arAnswersVarname,
            []
        );

        foreach ($arQuestions as $questionID => $questionItem) {
            $rsAnswers = CFormAnswer::GetList(
                $questionID,
                $by = 's_id',
                $order = 'asc',
                [],
                $is_filtered
            );

            while ($arAnswer = $rsAnswers->Fetch()) {
                if ($arAnswer['FIELD_TYPE'] === 'text') {
                    $arDataForm['form_text_'.$arAnswer['ID']] = $data[$questionItem['SID']];
                } elseif ($arAnswer['FIELD_TYPE'] === 'dropdown') {
                    if (mb_strtolower($data[$questionItem['SID']]) == mb_strtolower($arAnswer['MESSAGE'])) {
                        $arDataForm['form_dropdown_'.$questionItem['SID']] = $arAnswer['ID'];
                    }
                } elseif ($arAnswer['FIELD_TYPE'] === 'textarea') {
                    $arDataForm['form_textarea_'.$arAnswer['ID']] = $data[$questionItem['SID']];
                } else {
                    $arDataForm['form_'.$arAnswer['FIELD_TYPE'].'_'.$arAnswer['ID']] = $data[$questionItem['SID']];
                }
            }
        }

        $resultID = CFormResult::Add($formID, $arDataForm);

        if ($resultID) {
            // Поле Confirmation - всегда true, так как проверка идет на фронтенде.
            $this->forceUpdateFieldPrivateAgreement($resultID, $sFormSid);
            $this->mailResult($resultID); // Уведомить по почте о заполнении формы.
            $arReturn['RESULT_ID'] = $resultID;
        } else {
            global $strError;
            $arReturn['ERROR'] = $strError;
        }

        return $arReturn;
    }

    /**
     * Обработка ответа.
     *
     * @return array|void
     * @throws LoaderException
     */
    public function getResponse()
    {
        $arRes = ['SUCCESS' => 'N', 'ERROR' => ''];

        $arResAdd = $this->formCreate($_POST, $_POST['FORM_ID']);
        if (intval($arResAdd['RESULT_ID']) > 0) {
            $arRes['SUCCESS'] = 'Y';
        } else {
            $arRes['ERROR'] = $arResAdd['ERROR'];
        }

        // $arRes['CAPTCHA_CHECK_RESULT'] = $this->checkCaptcha($_POST['CAPTCHA']);

        static::$return->addData(['RESULT' => $arRes]);
        static::$return->setSuccess();
    }

    /**
     * Проверка капчи на валидность.
     *
     * @param mixed $sCaptchaRespones Ответ капчи.
     *
     * @return boolean
     */
    protected function checkCaptcha($sCaptchaRespones) : bool
    {
        if (!$sCaptchaRespones) {
            return false;
        }

        $data = [
            'secret' => FormManager::GOOGLE_CAPTCHA_SECRET_KEY,
            'response' => $sCaptchaRespones
        ];
        $options = [
            'http' => [
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];

        $context  = stream_context_create($options);
        $verify = file_get_contents(FormManager::GOOGLE_CAPTCHA_VERIFY_URL, false, $context);
        $captcha_success=json_decode($verify);

        return $captcha_success->success;
    }
}
