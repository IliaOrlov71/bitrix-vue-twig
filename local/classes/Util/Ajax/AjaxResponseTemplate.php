<?php
/**
 * Created by PhpStorm.
 * User: Zakusilo.DV
 * Date: 21.02.2019
 * Time: 15:40
 */

namespace Local\Util\Ajax;

/**
 * Интерфейс для ответов
 * Interface AjaxResponseTemplate
 * @package Local\Util\Ajax
 */
interface AjaxResponseTemplate
{
    /**
     * Метод, который возвращает ответ от сервера
     * @return array
     */
    public function getResponse();
}
