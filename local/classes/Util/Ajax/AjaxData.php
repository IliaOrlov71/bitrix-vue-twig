<?php
/**
 * Created by PhpStorm.
 * User: Zakusilo.DV
 * Date: 21.02.2019
 * Time: 16:57
 */

namespace Local\Util\Ajax;

/**
 * Class AjaxData
 * @package Local\Util\Ajax
 */
class AjaxData
{
    /** @var array $arData Массив с данными. */
    private $arData = array();
    /** @var bool $bSuccess */
    public $bSuccess = false;
    /** @var string $sErrorMessage Сообщение об ошибке. */
    public $sErrorMessage = '';
    /**
     * Метод добавляет в результирующий массив новую запись.
     *
     * @param array $arData Массив с передаваемыми значениями в ответ на запрос.
     *
     * @return void
     */
    public function addData(array $arData): void
    {
        $this->arData += $arData;
        array_multisort(array_column($this->arData[0]), $this->arData);
    }

    /**
     * Метод возвращает список данных для ответа на запрос.
     * @return array
     */
    public function getData(): array
    {
        return $this->arData;
    }

    /**
     * Метод устанавливает признак корректного ответа на запрос.
     * @return void
     */
    public function setSuccess(): void
    {
        $this->addData(array('STATUS' => 1));
        $this->bSuccess = true;
        http_response_code(200);
    }

    /**
     * Задать сообщение об ошибке.
     *
     * @param string $sErrorMessage Сообщение об ошибке.
     */
    public function setErrorMessage(string $sErrorMessage = '') : void
    {
        $this->sErrorMessage = $sErrorMessage;
    }
}
