<?php

namespace Local\Util\Ajax;

use Bitrix\Main\Loader;
use CForm;
use CFormAnswer;
use CFormResult;
use Local\Constants;
use Local\Guta\FormManager;

/**
 * Class Callback
 * Обработчик формы Callback
 * @package Local\Util\Ajax
 */
class Callback extends AjaxFactory implements AjaxResponseTemplate
{

}
