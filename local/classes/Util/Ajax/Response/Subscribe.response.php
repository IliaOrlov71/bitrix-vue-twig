<?php

namespace Local\Util\Ajax;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use CForm;
use CFormAnswer;
use CFormResult;
use Local\Constants;
use Local\Guta\FormManager;

/**
 * Class Subscribe
 * Обработчик формы подписки
 * @package Local\Util\Ajax
 */
class Subscribe extends AjaxFactory implements AjaxResponseTemplate
{
    /**
     * Обработка ответа (без капчи).
     *
     * @return array|void
     * @throws LoaderException
     */
    public function getResponse()
    {
        $arRes = [
            'SUCCESS' => 'N',
            'ERROR' => '',
        ];

        $arResAdd = $this->formCreate($_POST, $_POST['FORM_ID']);
        if ((int)$arResAdd['RESULT_ID'] > 0) {
            $arRes['SUCCESS'] = 'Y';
            parent::$return->setSuccess();

        } else {
            $arRes['ERROR'] = $arResAdd['ERROR'];
            $arRes['SUCCESS'] = 'N';
            parent::$return->setErrorMessage($arRes['ERROR']);
        }

        parent::$return->addData(['RESULT' => $arRes]);
    }
}
