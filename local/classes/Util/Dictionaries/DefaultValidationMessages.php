<?php

namespace Local\Util\Dictionaries;

/**
 * Class DefaultValidationMessages
 * @package Local\Util\Dictionaries
 *
 * @since 30.10.2020 Дополнительные сообщения.
 */
class DefaultValidationMessages extends AbstractDictionary
{
    /**
     * @return string[]
     */
    public static function getItems(): array
    {
        return  [
            'required' => 'Не указано поле :attribute',
            'numeric' => 'Поле :attribute должно содержать числовое значение',
            'digits' => 'Поле :attribute должно состоять только из цифр',
            'max' => 'Поле :attribute - нарушение по заданной длине параметра.',
            'min' => 'Поле :attribute - нарушение по минимальной длине параметра.',
            'url' => 'Поле :attribute должно быть валидным url.',
            'array' => 'Поле :attribute должно быть массивом.',
        ];
    }
}
