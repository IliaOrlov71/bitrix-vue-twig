<?php

namespace Local\Util;

/**
 * Class StaticPageH1
 * @package Local\Util
 */
class StaticPageH1
{
    /**
     * Получить H1 статической страницы
     *
     * @return string
     */
    public static function value(): string
    {
        global $APPLICATION;
        $sDirH1 = $APPLICATION->GetDirProperty('h1');

        if (!$sDirH1) {
            $sDirH1 =  $APPLICATION->GetTitle();
        }

        $APPLICATION->AddViewContent('h1', $sDirH1);

        return $sDirH1 ?: '';
    }
}
