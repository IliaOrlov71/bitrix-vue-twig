<?php

namespace Local\Util;

/**
 * Class Supply
 * @package Local\Util
 */
class Supply
{
    /**
     * Приведение цены к виду 12 200.
     *
     * @param mixed $price Цена.
     *
     * @return string
     */
    public static function normalizePrice($price) : string
    {
        if (empty($price)) {
            return '';
        }

        return number_format($price, 0, ',', ' ');
    }
}
