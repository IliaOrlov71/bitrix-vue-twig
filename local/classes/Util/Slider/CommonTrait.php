<?php

namespace Local\Util\Slider;

use Illuminate\Database\Eloquent\Collection;
use Local\Models\RentalObjects;
use Local\Models\Rentals;
use Local\Util\GutaResizeNoUpscale;

/**
 * Trait CommonTrait
 * Для слайдеров.
 * @package Local\Util\Slider
 */
trait CommonTrait
{
    /** @var boolean $bNormalizeQuantityItems Если количество лотов в объекте меньше трех,
     * то догонять ли их количество до минимально необходимого.
     * По умолчанию - нет (false).
     */
    private $bNormalizeQuantityItems = false;
    /** @var array $arFilter Набор свойств для выборки из ИБ по лотам. */
    private static $arFilter = [
        'ID',
        'IBLOCK_SECTION_ID',
        'NAME',
        'PREVIEW_PICTURE',
        'DETAIL_PICTURE',
        'PROPERTY_TYPE',
        'PROPERTY_SQUARE',
        'PROPERTY_CITY',
        'PROPERTY_ADDRESS',
        'PROPERTY_PRICE_SQUARE',
        'PROPERTY_TOTAL_PRICE',
        'PROPERTY_TOTAL_PRICE_MONTH',
        'DETAIL_PAGE_URL'
    ];


    /**
     * Ресайз картинки.
     *
     * @param integer $idImage ID картинки.
     *
     * @return string
     */
    private function resizeImage(int $idImage): string
    {
        $sUrlResizedPicture = GutaResizeNoUpscale::resize((int)$idImage, self::SLIDER_WIDTH_PICTURE, self::SLIDER_HEIGHT_PICTURE);

        return $sUrlResizedPicture?: '';
    }

    /**
     * Обработка данных, полученных моделью.
     *
     * @param \Illuminate\Support\Collection $obLotsModel Выборка из ИБ.
     *
     * @return array
     */
    private function processModel(\Illuminate\Support\Collection $obLotsModel): array
    {
        $arResult = [];

        $obLotsModel->each(
            function ($item, $key) use (&$arResult) {
                /** @var \Illuminate\Support\Collection $item */
                $indexObject = $item['ID'];
                $arData = $item->toArray();

                // Ресайз картинки.
                $this->treatmentPicture($arData);

                // Обработка параметров. Только в случае наличия картинки.
                if (!empty($arData['PICTURE_RESIZED'])) {
                    $arResult[$indexObject] = $this->treatmentProperties($arData);
                }
            }
        );

        return $arResult;
    }

    /**
     * Обработка картинок. Базовой берется PREVIEW. Если ее
     * не существует, то DETAIL. Иначе - облом.
     *
     * @param array $arData Данные из запроса.
     *
     * @return void
     */
    private function treatmentPicture(array &$arData) : void
    {
        $idImage = 0;

        if (!empty($arData['PREVIEW_PICTURE'])) {
            $idImage = $arData['PREVIEW_PICTURE'];
        } elseif (!empty($arData['DETAIL_PICTURE'])) {
            $idImage = $arData['DETAIL_PICTURE'];
        }

        $arData['PICTURE_RESIZED'] = $idImage ? $this->resizeImage($idImage) : '';
    }

    /**
     * Обработка свойств.
     *
     * @param array $arData Данные из запроса.
     * @return array
     */
    private function treatmentProperties(array $arData) : array
    {
        /** @var integer $iTotalPricesMonth Если не указана цена за месяц, то вычислить ее. */
        $iTotalPricesMonth = (int)$arData['PROPERTY_TOTAL_PRICE_MONTH_VALUE'];

        if (!$iTotalPricesMonth
            && $arData['PROPERTY_PRICE_SQUARE_VALUE']
            && $arData['PROPERTY_SQUARE_VALUE']) {
             $iTotalPricesMonth = round(($arData['PROPERTY_SQUARE_VALUE'] * $arData['PROPERTY_PRICE_SQUARE_VALUE']) / 12);
        }

        return [
            'title' => $arData['NAME'],
            'url' => $arData['DETAIL_PAGE_URL'],
            'image' => $arData['PICTURE_RESIZED'],
            'tags' => !empty($arData['PROPERTY_TYPE_VALUE']) ? $arData['PROPERTY_TYPE_VALUE'] : [],
            'area' => $arData['PROPERTY_SQUARE_VALUE'],
            'city' => $arData['PROPERTY_CITY_VALUE'],
            'address' => !empty($arData['~PROPERTY_ADDRESS_VALUE']) ? $arData['~PROPERTY_ADDRESS_VALUE'] :
                $arData['UF_ADDRESS'],
            'PRICE_SQUARE' => self::normalizePrice($arData['PROPERTY_PRICE_SQUARE_VALUE']),
            'PRICE_SQUARE_RAW' => $arData['PROPERTY_PRICE_SQUARE_VALUE'],
            'PRICE_TOTAL' => self::normalizePrice($arData['PROPERTY_TOTAL_PRICE_VALUE']),
            'PRICE_TOTAL_RAW' => $arData['PROPERTY_TOTAL_PRICE_VALUE'],
            'PRICE_TOTAL_MONTH' => self::normalizePrice($arData['PROPERTY_TOTAL_PRICE_MONTH_VALUE']),
            'PRICE_TOTAL_MONTH_RAW' => $arData['PROPERTY_TOTAL_PRICE_MONTH_VALUE'],
            'pricePerYear' => self::normalizePrice($arData['PROPERTY_PRICE_SQUARE_VALUE']),
            'pricePerMonth' => self::normalizePrice($iTotalPricesMonth),
        ];
    }

    /**
     * Приведение цены к виду 12 200.
     *
     * @param mixed $price Цена.
     *
     * @return string
     */
    private static function normalizePrice($price) : string
    {
        if (empty($price)) {
            return '';
        }

        return number_format($price, 0, ',', ' ');
    }

    /**
     * Запрос лотов для заполнения пустот. Выбираются элементы в случайном порядке.
     *
     *
     * @param array   $arIdsToExclude ID лотов, подлежащих исключению.
     * @param integer $iQty           Максимальное количество лотов для заполенния пустот.
     *
     * @return array
     */
    private function getDataForNormalize(array $arIdsToExclude, int $iQty = 10): array
    {
        $obLotsModel = Rentals::query()
            ->filter(['ACTIVE' => 'Y', '!ID' => $arIdsToExclude])
            ->sort(['RAND' => 'ASC'])
            ->limit($iQty)
            ->select(self::$arFilter)
            ->fetchUsing('GetNext')
            ->getList();

        return $this->processModel($obLotsModel);
    }

    /**
     * Нормализация количества лотов.
     *
     * @param array $arItems Лоты, уже полученные в результате запроса.
     * @return array
     */
    private function normalizeCountItems(array $arItems): array
    {
        $arResult = $arItems;

        /** @var integer $iNeedQtyToNormalize Сколько лотов нужно, чтобы добрать до минимально необходимого. */
        $iNeedQtyToNormalize = self::MIN_COUNT_ITEMS_IN_SLIDER - count($this->arResult['ITEMS']);
        /** @var array $arExcludeItems ID лотов, подлежащие исключению, ибо они уже есть в массиве. */
        $arExcludeItems = collect($arItems)->pluck('ID')->toArray();

        $arShuffledItems = $this->getDataForNormalize($arExcludeItems, $iNeedQtyToNormalize);

        if (!empty($arShuffledItems)) {
            $arResult = array_merge($arResult, $arShuffledItems);
        }

        return array_values($arResult);
    }
}
