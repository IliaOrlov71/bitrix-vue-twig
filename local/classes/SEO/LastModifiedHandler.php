<?php

namespace Local\SEO;

/**
 * Class LastModifiedHandler
 * @package Local\SEO
 */
class LastModifiedHandler
{
    /** @var array $arLastModifiedData Дата последнего изменения последовательно по всей странице. */
    private static $arLastModifiedData = [];

    /**
     * Фасад.
     *
     * @param string $sHashCode Хэш-код.
     * @param mixed  $value     Таймстамп.
     *
     * @return void
     */
    public static function add(string $sHashCode, $value) : void
    {
        $obSelf = new self();
        $obSelf->addData($sHashCode, $value);
    }
    /**
     * Добавить дату.
     *
     * @param string $sHashCode Хэш-код.
     * @param mixed  $value     Таймстамп.
     *
     * @return void
     */
    public function addData(string $sHashCode, $value): void
    {
        self::$arLastModifiedData[md5($sHashCode)] = $value;
    }

    /**
     * Накопленные данные.
     *
     * @return array
     */
    public function getData() : array
    {
        return self::$arLastModifiedData;
    }
    /**
     * Получить значение самого свежего изменения.
     *
     * @return mixed
     */
    public function getNewestModified()
    {
        if (empty(self::$arLastModifiedData)) {
            return '';
        }

        $arValues = array_values(self::$arLastModifiedData);
        rsort($arValues);

        return $arValues[0];
    }
}
