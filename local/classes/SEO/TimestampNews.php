<?php

namespace Local\SEO;

/**
 * Class TimestampNews
 * @package Local\SEO
 */
class TimestampNews
{
    /** @var array $arTimeStamps Таймстампы элементов новостей. */
    private $arTimestamps;

    /**
     * TimestampNews constructor.
     *
     * @param array $arResultItems Массив $arResult['ITEMS'] из компонента news.list.
     */
    public function __construct(array $arResultItems)
    {
        $this->arTimestamps = collect($arResultItems)->pluck('TIMESTAMP_X')->toArray();

        usort($this->arTimestamps, function ($a, $b) {
            if (strtotime($a) < strtotime($b)) {
                return 1;
            } else {
                if (strtotime($a) > strtotime($b)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
    }

    /**
     * Получить свежайший timestamp.
     *
     * @return string
     */
    public function getNewestTimestamp() : string
    {
        /** @var string $sFinalTimestamp Самый свежий timestamp. */
        return !empty($this->arTimestamps[0]) ? $this->arTimestamps[0] : '';
    }
}
