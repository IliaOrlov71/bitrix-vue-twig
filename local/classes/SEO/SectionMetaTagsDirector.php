<?php

namespace Local\SEO;

use Bitrix\Iblock\InheritedProperty\SectionValues;

/**
 * Class SectionMetaTagsDirector
 * @package Local\SEO
 *
 * @since 01.07.2021
 */
class SectionMetaTagsDirector
{
    /**
     * @var string $prefix Префикс.
     */
    private $prefix = '';

    /**
     * @var string $defaultTitle Title по умолчанию.
     */
    private $defaultTitle = '';

    /**
     * @var string $defaultDescription Description по умолчанию.
     */
    private $defaultDescription = '';

    /**
     * Установить мета-тэги.
     *
     * @param integer $iblockId  ID инфоблока.
     * @param integer $sectionId ID элемента.
     *
     * @return void
     */
    public function set(int $iblockId, int $sectionId) : void
    {
        global $APPLICATION;

        if (!$iblockId || !$sectionId) {
            return;
        }

        $ipropValues = new SectionValues($iblockId, $sectionId);

        $arResult['IPROPERTY_VALUES'] = $ipropValues->getValues();

        $title = $this->defaultTitle;
        if ($arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE']) {
            $title = $arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE'];
        }

        $APPLICATION->SetPageProperty('title', $this->prefix . $title);

        $description = $this->defaultDescription;
        if ($arResult['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION']) {
            $description = $arResult['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION'];
        }

        $APPLICATION->SetPageProperty('description', $this->prefix . $description);
    }

    /**
     * @param string $prefix Префикс.
     *
     * @return void
     */
    public function setPrefix(string $prefix): void
    {
        $this->prefix = $prefix;
    }

    /**
     * @param string $defaultTitle Title по умолчанию.
     *
     * @return void
     */
    public function setDefaultTitle(string $defaultTitle): void
    {
        $this->defaultTitle = $defaultTitle;
    }

    /**
     * @param string $defaultDescription Description по умолчанию.
     *
     * @return void
     */
    public function setDefaultDescription(string $defaultDescription): void
    {
        $this->defaultDescription = $defaultDescription;
    }
}
