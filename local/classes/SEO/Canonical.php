<?php

namespace Local\SEO;

use Bitrix\Main\Application;
use Bitrix\Main\SystemException;

/**
 * Class Canonical
 * Вывод канонических ссылок.
 * @package Local\Lotus
 */
class Canonical
{
    /** @const string TARGET_ID ID отложенной функции */
    private const TARGET_ID = 'gc_global_canonical';

    /** @const string PAGE_PROPERTY_CANONICAL_NAME Навзание свойства папки для канонических страниц. */
    private const PAGE_PROPERTY_CANONICAL_NAME = 'canonical';

    /**
     * @param string $canonicalLink Каноническая ссылка.
     *
     * @return void
     */
    public static function passLink(string $canonicalLink = ''): void
    {
        if (!$canonicalLink) {
            return;
        }

        $GLOBALS['APPLICATION']->AddViewContent(
            self::TARGET_ID,
            '<link rel="canonical" href="'.self::getFullUrl($canonicalLink).'"/>'
        );
    }

    /**
     * Вывод канонической ссылки в header.
     *
     * @return void
     */
    public static function show(): void
    {
        $GLOBALS['APPLICATION']->ShowViewContent(self::TARGET_ID);
    }

    /**
     * Канонические ссылки для статических страниц. Берутся из
     * свойства папки canonical.
     *
     * @param string $canonicalUrl URL (для списковых страниц).
     *
     * @return void
     */
    public static function staticPage(string $canonicalUrl = ''): void
    {
        if (!$canonicalUrl) {
            $canonicalUrl = $GLOBALS['APPLICATION']->GetPageProperty(self::PAGE_PROPERTY_CANONICAL_NAME);
        }

        if ($canonicalUrl) {
            $result = sprintf('<link rel="canonical" href="%s" />', self::getFullUrl($canonicalUrl));
            $GLOBALS['APPLICATION']->AddViewContent(self::TARGET_ID, $result);
        }
    }

    /**
     * Получить текущий URL.
     *
     * @return string
     * @throws SystemException
     */
    public static function getCurrentUrl() : string
    {
        $request = Application::getInstance()->getContext()->getRequest();

        return strtok($request->getRequestUri(), '?');
    }

    /**
     * Проверка - HTTP или HTTPS.
     *
     * @return boolean
     */
    private static function isSecureConnection(): bool
    {
        return
            (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
            || $_SERVER['SERVER_PORT'] == 443;
    }

    /**
     * Получить полный (включая https, домен) путь к канонической странице.
     *
     * @param string $url Укороченный URL (без домена).
     *
     * @return string
     */
    private static function getFullUrl(string $url = ''): string
    {
        $canonicalLink = $url ?: $_SERVER['REQUEST_URI'];
        $typeHttp = self::isSecureConnection() ? 'https://' : 'http://';

        return $typeHttp . $_SERVER['HTTP_HOST'] . $canonicalLink;
    }
}
