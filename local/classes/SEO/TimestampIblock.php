<?php

namespace Local\SEO;

use CIBlockElement;

/**
 * Class TimestampIblock
 * @package Local\SEO
 *
 * @since 30.11.2020 Доработка.
 */
class TimestampIblock
{
    /**
     * @var CIBlockElement
     */
    private $el;
    /**
     * @var integer
     */
    private $iblockId;

    /** @var array $payload Дополнительные циферки с датами изменений. */
    private $payload = [];

    /**
     * TimestampIblock constructor.
     *
     * @param integer        $iblockId ID инфоблока.
     * @param CIBlockElement $el       Битриксовый объект.
     */
    public function __construct(
        int $iblockId,
        CIBlockElement $el
    ) {

        $this->el = $el;
        $this->iblockId = $iblockId;
    }

    /**
     * Получить свежайший timestamp.
     *
     * @return string
     */
    public function getNewestTimestamp() : string
    {
        $arData = $this->query($this->iblockId);

        if (!empty($this->payload)) {
            $result = $this->process($arData);
            return !empty($result[0]) ? $result[0] : '';
        }

        return !empty($arData[0]['TIMESTAMP_X']) ? $arData[0]['TIMESTAMP_X'] : '';
    }

    /**
     * @param array $payload Дополнительные циферки с датами изменений.
     *
     * @return $this
     *
     * @since 30.11.2020
     */
    public function setPayload(array $payload): self
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * ВСЕ Элементы инфоблока.
     *
     * @param integer $iblockId ID инфоблока.
     *
     * @return array
     */
    protected function query(
        int $iblockId
    ) : array {

        $arResult = [];
        $result = $this->el->GetList(
            ['timestamp_x' => 'desc'],
            ['IBLOCK_ID' => $iblockId],
            false,
            false,
            ['TIMESTAMP_X']
        );

        while ($item = $result->fetch()) {
            $arResult[] = $item;
        }

        if (!empty($this->payload)) {
            $arResult = array_merge($arResult, $this->payload);
        }

        return $arResult;
    }

    /**
     * Сырая сортировка.
     *
     * @param array $arResultItems Массив с датами.
     *
     * @return array
     *
     * @since 31.11.2020
     */
    private function process(array $arResultItems = []) : array
    {
        if (empty($arResultItems)) {
            return [];
        }

        $result = collect($arResultItems)->pluck('TIMESTAMP_X')->toArray();
        usort($result, function ($a, $b) {
            if (strtotime($a) < strtotime($b)) {
                return 1;
            } else {
                if (strtotime($a) > strtotime($b)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        return $result;
    }
}
