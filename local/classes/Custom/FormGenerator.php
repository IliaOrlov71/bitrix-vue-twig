<?php

namespace Local\Custom;

use CIBlockElement;
use CIBlockResult;
use Local\Util\GutaResize;

/**
 * Класс для работы с формой
 * Class FormManager
 * @package Local\Guta
 */
class FormGenerator
{

    /** Cписок необходимых полей. */
    private const AR_REQUIRED_FIELDS = [
        'TYPE', 'SQUARE', 'PRICE_SQUARE',
        'ADDRESS', 'FLOOR', 'HEIGHT',
        'IMG', 'DETAIL_TEXT'
    ];
    /**
     * Метод возвращает ИД картинки из свойства SLIDER.
     * @param array $arProperty Свойство элемента.
     * @return string
     */
    public static function getImgIdFromSlider(array $arProperty): string
    {
        if (!$arProperty['PROPERTY_VALUE_ID']) {
            return '';
        }

        return $arProperty['PROPERTY_VALUE_ID'];
    }

    /**
     * Метод возвращает путь к картинке.
     * @param array $arElement Элемент инфоблока.
     * @return string
     */
    public static function getImgSrc(array $arElement): string
    {
        $sChosenImg = 'PREVIEW_PICTURE';
        if (!$arElement[$sChosenImg]) {
            $sChosenImg = 'DETAIL_PICTURE';
            if (!$arElement[$sChosenImg]) {
                $arElement[$sChosenImg] = '';
            }
        }

        return $arElement[$sChosenImg];
    }

    /**
     * Метод возвращает объект CIBlockResult.
     *
     * @param integer $iBlockID   ИД инфоблока.
     * @param integer $iElementID ИД элемента.
     *
     * @return CIBlockResult
     */
    public static function getCIBlockResult(int $iBlockID, int $iElementID): CIBlockResult
    {
        return CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => $iBlockID, 'ID' => $iElementID],
            false,
            false,
            []
        );
    }

    /**
     * Метод добавляет свойство "Описание" к списку свойств.
     *
     * @param integer $iBlockID         ИД инфоблока.
     * @param integer $iElementID       ИД элемента.
     * @param array   $arPropertyResult Список свойств.
     *
     * @return void
     */
    public static function setDetailTextProperty(int $iBlockID, int $iElementID, array &$arPropertyResult): void
    {
        $obElements = self::getCIBlockResult($iBlockID, $iElementID);
        while ($arEl = $obElements->GetNext()) {
            $arPropertyResult[] = [
                'CODE' => 'DETAIL_TEXT',
                'NAME' => 'Описание',
                'VALUE' => $arEl['DETAIL_TEXT']
            ];
        }
    }

    /**
     * Метод приводит элементы массива к одному формату.
     *
     * @param array $arElements Массив элементов.
     *
     * @return array
     */
    public static function cleanElements(array $arElements): array
    {
        $arFormFieldList = [];

        array_walk($arElements, function ($arElement) use (&$arFormFieldList) {
            if ($arElement['PROPERTY_TYPE'] === 'L') {
                $arElement['VALUE'] = $arElement['VALUE_ENUM'];
            }
            $arFormFieldList[] = ['NAME' => $arElement['NAME'], 'VALUE' => $arElement['VALUE']];
        });

        return $arFormFieldList;
    }

    /**
     * Метод возвращает отсортированный массив
     *
     * @param array $arElements Массив элементов.
     *
     * @return array
     */
    public static function sortElements(array $arElements): array
    {
        $arFormFieldList = [];
        $arFieldsOrder = [
            'Площадь помещения, кв.м.',
            'Тип помещения',
            'Изображение',
            'Цена за кв.м. в год, руб.',
            'Этаж',
            'Высота потолков, м',
            'Адрес',
            'Описание'
        ];

        foreach ($arFieldsOrder as $arField) {
            array_walk($arElements, function ($arElement) use (&$arFormFieldList, $arField) {
                if ($arElement['NAME'] === $arField) {
                    $arFormFieldList[] = $arElement;
                }
            });
        }

        return $arFormFieldList;
    }

    /**
     * Метод добавляет свойство "Изображение" к списку свойств.
     * @param integer $iBlockID         ИД инфоблока.
     * @param integer $iElementID       ИД элемента.
     * @param array   $arPropertyResult Список свойств.
     *
     * @return void
     */
    public static function setImgProperty(int $iBlockID, int $iElementID, array &$arPropertyResult): void
    {
        $obElements = self::getCIBlockResult($iBlockID, $iElementID);

        while ($arEl = $obElements->GetNext()) {
            $sImgSrc = self::getImgSrc($arEl);
            $arPropertyResult[] = [
                'CODE' => 'IMG',
                'NAME' => 'Изображение',
                'VALUE' => $sImgSrc
            ];
        }
    }

    /**
     * Метод возвращает значение для свойства "Изображение".
     *
     * @param array $arPropertyResult Массив свойств.
     *
     * @return string
     */
    public static function getImgPropertyValue(array $arPropertyResult): string
    {
        foreach ($arPropertyResult as $property) {
            $sFieldKey = 'VALUE';
            $sImgIdFromSlider = '';

            if ($property['NAME'] === 'Слайдер') {
                $sImgIdFromSlider = self::getImgIdFromSlider($property);
                continue;
            }

            if ($property['NAME'] === 'Изображение') {
                if (!$property[$sFieldKey]) {
                    $property[$sFieldKey] = $sImgIdFromSlider;
                }

                if (!empty($property[$sFieldKey])) {
                    $property[$sFieldKey] =  $_SERVER['DOCUMENT_ROOT'] . GutaResize::resize(
                        $property[$sFieldKey],
                        600,
                        400
                    );

                    return $property[$sFieldKey];
                }

                return '';
            }
        }
    }

    /**
     * Метод устанавливает значение для поля 'Изображение'.
     *
     * @param array $arPropertyResult Массив свойств.
     * @param array $arFormFieldList  Массив полей.
     *
     * @return void
     */
    public static function setImgPropertyValue(array $arPropertyResult, array &$arFormFieldList): void
    {
        foreach ($arFormFieldList as $key => $arFormField) {
            if ($arFormField['CODE'] === 'IMG') {
                $arFormFieldList[$key]['VALUE'] = self::getImgPropertyValue($arPropertyResult);
            }
        }
    }

    /**
     * Метод возвращает  список полей формы, вида [['NAME' => $NAME, 'VALUE' => $VALUE], [...]].
     * @param integer $iBlockID   ИД инфоблока.
     * @param integer $iElementID ИД элемента.
     *
     * @return array
     */
    public static function getFormFieldList(int $iBlockID, int $iElementID): array
    {
        $arFormFieldList = [];

        $arPropertyResult = CIBlockElement::GetProperty($iBlockID, $iElementID, [], [])->arResult;
        self::setImgProperty(
            $iBlockID,
            $iElementID,
            $arPropertyResult
        );

        self::setDetailTextProperty(
            $iBlockID,
            $iElementID,
            $arPropertyResult
        );
        $arFormFieldList = array_filter($arPropertyResult, function ($arProperty) use (&$arFormFieldList, $arPropertyResult) {
            if (in_array($arProperty['CODE'], self::AR_REQUIRED_FIELDS)) {
                return true;
            }
        });

        self::setImgPropertyValue($arPropertyResult, $arFormFieldList);
        $arFormFieldList = self::cleanElements($arFormFieldList);
        $arFormFieldList = self::sortElements($arFormFieldList);

        return $arFormFieldList;
    }

    /**
     * Метод формирует html код (форму).
     * @param array  $arFormFieldList   Список полей формы.
     * @param string $sBlockSectionName Имя раздела.
     *
     * @return string
     */
    public static function renderForm(array $arFormFieldList, string $sBlockSectionName): string
    {
        $sOuterHTML = '';

        $sOuterHTML .= '<form>' .
                        '<h3 style="text-align: center;">' . $sBlockSectionName . '</h3>' .
                        '<div>';

        foreach ($arFormFieldList as $arFormField) {
            if ($arFormField['NAME'] === 'Площадь помещения, кв.м.') {
                $sOuterHTML .= '<div style=" display: inline-block; background-color: #FF0000;
                width: 120px; height: 80px; color: #FFFFFF">' . '<p style="
                font-size: 20px; font-weight: bold; text-align: center;">' .
                    $arFormField['VALUE'] . ' м<sup>2</sup></sup></p></div>';
                continue;
            }

            if ($arFormField['NAME'] === 'Тип помещения') {
                $sOuterHTML .= '<div style="display: inline-block; width: 200px; height: 80px;
                font-size: 20px; font-weight: bold; margin-left: 40px;">' .
                    '<p>' . $arFormField['VALUE'] . '</p>' .
                    '</div>';
                continue;
            }

            if ($arFormField['NAME'] === 'Изображение') {
                $sOuterHTML .= '<img src="' . $arFormField['VALUE'] . '"></div><br>';
                continue;
            }

            if ($arFormField['NAME'] === 'Описание') {
                $sOuterHTML .= '<div>' .
                    '<p style="font-size: 16px; ">' . $arFormField['NAME'] . '</p>' .
                    '<p style="padding-top: 10px; padding-bottom: 10px; border-top: 1px solid #C0C0C0;
                    border-bottom: 1px solid #C0C0C0; width: 700px;">' . $arFormField['VALUE'] .'</p>' .
                    '</div>';
                continue;
            }

            $sOuterHTML .= '<div style=" padding-left: 10px;padding-top: 20px; padding-bottom: 10px;
                width: 750px; background-color: #F9FAFC">' .
                '<div style="width: 240px; display: inline-block; font-size: 16px; ">' . $arFormField['NAME'] . '</div>';

            $sOuterHTML .= '<div style="width: 350px; display: inline-block; margin-left: 120px; font-weight: bold">' .
                $arFormField['VALUE'] . '</div></div><br>';
        }

        $sOuterHTML .= '</form';

        return $sOuterHTML;
    }
}
