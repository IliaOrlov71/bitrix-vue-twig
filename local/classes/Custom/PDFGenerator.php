<?php

namespace Local\Custom;

use Dompdf\Dompdf;

/**
 * Класс(обертка) для работы с PDF, использующий внешнюю библиотеку Dompdf.
 *
 * {@internal
 * Для того, чтобы добавить свой шрифт, нужно запустить из консоли
 * файл load_font.php, указав параметры запуска name  и path, например
 * load_font.php Roboto D:\font\Roboto-Regular.ttf
 * }}
 *
 * список доступных шрифтов находится по пути:
 * @see dompdf\dompdf\lib\fonts\dompdf_font_family_cache.php
 * Class PDFManager
 * @package Local\Guta
 */
class PDFGenerator
{
    /** @const string UPLOAD_PATH Путь к папке, хранящей сгенерированные PDF файлы. */
    private const UPLOAD_PATH = '/upload/';
    /** @const array DEFAULT_FONT Параметр для конструктора класса, вида свойство => значение. */
    public const DEFAULT_FONT = ['defaultFont' => 'dejavu sans'];
   /** @var object $obPDFText поле для создания объекта класса Dompdf. */
    public $obPDFText;

    /**
     * Определяет public $obPDFText как объект Dompdf.
     * PDFGenerator constructor.
     *
     * @param array $arDefaultFont Шрифт по умолчанию.
     */
    public function __construct(array $arDefaultFont = self::DEFAULT_FONT)
    {
        $this->obPDFText = new Dompdf($arDefaultFont);
    }

    /**
     * Метод сохраняет html в pdf  в файле $sFileName, либо возвращает пустую строку.
     *
     * @param string $sFileName Хэш-имя файла.
     *
     * @return string
     */
    public function setFile(string $sFileName): string
    {
       /** @var string $sFilePath Путь до файла. */
        $sFilePath = $_SERVER['DOCUMENT_ROOT'] . self::UPLOAD_PATH . $sFileName . '.pdf';

        $this->obPDFText->render();

        if (is_dir(dirname($sFilePath)) && is_writable(dirname($sFilePath))) {
            file_put_contents($sFilePath, $this->obPDFText->output());

            return self::UPLOAD_PATH . $sFileName . '.pdf';
        }

        return '';
    }
}
