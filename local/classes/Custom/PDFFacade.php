<?php

namespace Local\Custom;

use Local\Guta\IBlockSectionManager;

/**
 * Class PDFFacade
 * @package Local\Custom
 */
class PDFFacade
{
    /**
     * Метод возвращает строку html-кода.
     *
     * @param integer $iBlockID        ИД инфоблока.
     * @param integer $iElementID      ИД элемента.
     * @param integer $iBlockSectionID ИД раздела.
     *
     * @return string
     *
     * @throws
     */
    public static function getHTML(int $iBlockID, int $iElementID, int $iBlockSectionID): string
    {
        $sBlockSectionName = IBlockSectionManager::getSBlockSectionNameByID($iBlockSectionID);
        $arFormFieldList = FormGenerator::getFormFieldList($iBlockID, $iElementID);

        return $sRenderedForm = FormGenerator::renderForm($arFormFieldList, $sBlockSectionName);
    }

    /**
     * Метод сохраняет PDF файл на сервере и возврвщает полный путь файла.
     *
     * @param string $sRegeneratedForm Html строка.
     *
     * @return string
     * @throws
     */
    public static function setPDF(string $sRegeneratedForm): string
    {
        if (empty($sRegeneratedForm)) {
            return '';
        }

        $sFileName = md5($sRegeneratedForm . random_bytes(16));

        $obPDF = new PDFGenerator();
        $obPDF->obPDFText->loadHtml($sRegeneratedForm);

        return $obPDF->setFile($sFileName);
    }
}
