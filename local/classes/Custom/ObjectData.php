<?php

namespace Local\Custom;

use Local\Constants;
use Local\Models\Rentals;
use Local\Util\Slider\CommonTrait;

/**
 * Class ObjectData
 * @package Local\Custom
 */
class ObjectData
{
    use CommonTrait;

    /** @const integer SLIDER_WIDTH_PICTURE Ширина картинок для ресайза. */
    private const SLIDER_WIDTH_PICTURE = 800;
    /** @const integer SLIDER_HEIGHT_PICTURE Высота картинок для ресайза. */
    private const SLIDER_HEIGHT_PICTURE = 800;
    /** @var int Номер страницы. */
    private $iPageNumber;
    /** @var int Количество элементов на странице.  */
    private $pageSize;
    /** @var array $arFilter */
    private $arFilterRaw = [];
    /** @var array $arSortRaw Сортировка. */
    private $arSortRaw = [];
    /** @var array $arMapProperties */
    private static $arMapProperties = [
      'type' => 'PROPERTY_TYPE',
      'city' => 'PROPERTY_CITY',
      'object' => 'IBLOCK_SECTION_ID',
    ];
    /** @var array $arSortProperties Сортировка. */
    private static $arSortProperties = [
        'square' => 'PROPERTY_SQUARE',
        'price' => 'PROPERTY_PRICE_SQUARE'
    ];

    /**
     * objectData constructor.
     *
     * @param integer $iPageNumber Номер страницы.
     * @param integer $pageSize    Количество элементов на странице.
     */
    public function __construct(int $iPageNumber = 1, int $pageSize = 20)
    {
        $this->iPageNumber = $iPageNumber;
        $this->pageSize = $pageSize;
    }

    /**
     * Установить фильтр.
     *
     * @param array $arGet Массив, приходящий из $_GET массива фильтра.
     *
     * @return void
     */
    public function setFilter(array $arGet = [])
    {
        if (empty($arGet)) {
            return;
        }

        $this->arFilterRaw = $this->treatmentFilter($arGet);
    }

    /**
     *
     * Сортировка.
     *
     * @param array $arData   Исходные данные.
     * @param array $arFields Поля.
     *
     * @return mixed
     */
    private function sort($arData, $arFields)
    {
        return $arData;
    }
    /**
     * Подготовка битриксового фильтра.
     *
     * @param array $arFilter Фильтр.
     *
     * @return array
     */
    private function treatmentFilter(array $arFilter)
    {
        $arResult = [];

        $maxPrice = !empty($arFilter['maxprice']) ? $arFilter['maxprice'] :  Constants::INFINITY;
        $minPrice = !empty($arFilter['minprice']) ? $arFilter['minprice'] : 0;

        $maxSquare = !empty($arFilter['maxsquare']) ? $arFilter['maxsquare'] : Constants::INFINITY;
        $minSquare = !empty($arFilter['minsquare']) ? $arFilter['minsquare'] : 0;

        foreach ($arFilter as $key => $arFilterItem) {
            if (empty(self::$arMapProperties[$key])) {
                continue;
            }
            // Обработка параметров, передаваемых через запятую.
            $arExplode = explode(',', trim($arFilterItem));
            // Удалить пустые элементы.
            $arExplode = array_filter($arExplode);

            $sPropertyKey = self::$arMapProperties[$key];
            $arResult [$sPropertyKey] = $arExplode;
        }

        // Диапазоны.

        $priceFilter['><PROPERTY_PRICE_SQUARE'] = [
            'LOGIC' => 'OR',
            [$minPrice, $maxPrice],
            '=PROPERTY_PRICE_SQUARE' => false, // Включать в выборку лоты, у которых не указана цена.
        ];

        $squareFilter['><PROPERTY_SQUARE'] = [
            'LOGIC' => 'OR',
            [$minSquare, $maxSquare],
            '=PROPERTY_SQUARE' => false, // Включать в выборку лоты, у которых не указана площадь.
        ];

        $arResult = array_merge($arResult, $priceFilter, $squareFilter);

        return $arResult;
    }

    /**
     * Получить данные.
     *
     * @return array
     */
    public function get()
    {
        return $this->getDataFromSubsection($this->iPageNumber, $this->pageSize);
    }

    /**
     * Получение данных, согласно посещению подраздела.
     *
     * @param integer $iPageNumber Номер страницы.
     * @param integer $pageSize    Количество элементов на странице.
     *
     * @return array
     */
    private function getDataFromSubsection(int $iPageNumber = 1, int $pageSize = 20) : array
    {
        /** @var array $arSort Результирующая сортировка. */
        $arSort = empty($this->arSortRaw) ? ['SORT' => 'DESC'] : $this->arSortRaw;

        $obLotsModel = Rentals::query()
            ->filter(array_merge(['ACTIVE' => 'Y'], $this->arFilterRaw))
            ->sort($arSort)
            ->select(self::$arFilter)
            ->navigation(['iNumPage'=>$iPageNumber, 'nPageSize' => $pageSize])
            ->page($iPageNumber)
            ->fetchUsing('GetNext')
            ->getList();

        $arResult = $this->processModel($obLotsModel);

        // Сортировка (если необходимо).
        if (!empty($this->arSortRaw)) {
            $arResult = $this->sort($arResult, $this->arSortRaw);
        }

        return $arResult;
    }

    /**
     * Обработка параметров (разделитель - запятая).
     *
     * @param string $sData Строка, приходящая из $_GET параметра.
     *
     * @return array Расщепленный массив.
     */
    public static function parseParams(string $sData = ''): array
    {
        $arExplode = explode(',', trim($sData));

        return array_filter($arExplode);
    }
}
