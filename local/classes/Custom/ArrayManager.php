<?php


namespace Local\Custom;

/**
 * Class ArrayManager
 * @package Local\Custom
 */
class ArrayManager
{
    /**
     * Метод возвращает сгрупированный по $sFilter массив.
     *
     * @param array  $arList  Массив для группировки.
     * @param string $sFilter Поле, по которому будет сгруппирован массив.
     *
     * @return array
     */
    public static function groupBy(array $arList, string $sFilter): array
    {
        $arResultList = [];

        array_walk($arList, function ($arItem, $key) use ($sFilter, &$arResultList) {
            if ($sFilter) {
                $arResultList[$arItem[$sFilter]][$key] = $arItem;
            }
        });

        return $arResultList;
    }

    /**
     * Метод возвращает следующий элемент массива, следующий за элементом с ключом $currentElemKey.
     *
     * @param array $arList         Массив значений.
     * @param mixed $currentElemKey Ключ текущего элемента.
     *
     * @return array
     */
    public static function getNext(array $arList, $currentElemKey): array
    {
        $isFound = false;
        $arResult = [];

        foreach ($arList as $key => $arItem) {
            if ($isFound) {
                $arResult[] = $arItem;
                break;
            }
            if ($key === $currentElemKey) {
                $isFound = true;
            }
        }

        return $arResult;
    }
}
