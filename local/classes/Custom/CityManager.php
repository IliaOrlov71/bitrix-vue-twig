<?php


namespace Local\Custom;

use CIBlockSection;
use Local\Models\RentalObjects;

/**
 * Class CityManager
 * @package Local\Custom
 */
class CityManager
{
    /**
     * Метод возвращает список уникальных городов.
     *
     * @return array.
     */
    public static function getAllCities(): array
    {
        $obRentalModel = RentalObjects::query()
            ->filter(['ACTIVE' => 'Y'])
            ->sort(['SORT' => 'DESC'])
            ->select('UF_CITY')
            ->fetchUsing('GetNext')
            ->getList();

        return $obRentalModel->pluck('UF_CITY')->unique()->toArray();
    }

    /**
     * Метод возвращает список всех разделов инфоблока.
     *
     * @param integer $iBlockId ИД инфоблока.
     *
     * @return array.
     */
    public static function getAllPlaces(int $iBlockId): array
    {
        $arSections = [];

        $arFilter = ['IBLOCK_ID' => $iBlockId, 'GLOBAL_ACTIVE'=>'Y'];
        $res = CIBlockSection::GetList([], $arFilter, false, ['UF_*']);

        while ($arSection = $res->getNext()) {
            if ($arSection) {
                $arSections[] = $arSection;
            }
        }

        return $arSections;
    }
}
