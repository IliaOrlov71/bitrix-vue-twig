<?php

namespace Local\Commands\Exceptions;

use Local\SymfonyTools\Framework\Exceptions\BaseException;

/**
 * Class RuntimeAppException
 * Runtime исключения классов пространства имен Constants.
 * @package Local\Commands\Exceptions
 */
class RuntimeConsoleException extends BaseException
{

}
