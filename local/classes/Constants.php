<?php

namespace Local;

/**
 * Class Constants
 * @package Local
 */
class Constants
{
    /**  @const integer PICTURE_FOR_PDF_WIDTH Ширина изображения для вставки в PDF. */
    const PICTURE_FOR_PDF_WIDTH = 600;

    /**  @const integer PICTURE_FOR_PDF_HEIGHT Высота изображения для вставки в PDF. */
    const PICTURE_FOR_PDF_HEIGHT = 400;

    /**  @const integer PREVIEW_WIDTH_PICTURE Ширина изображения на слайдере. */
    const PREVIEW_WIDTH_PICTURE = 600;

    /**  @const integer PREVIEW_HEIGHT_PICTURE Ширина изображения на слайдере. */
    const PREVIEW_HEIGHT_PICTURE = 600;

    /** @const string PHONE_TO_CALL телефон для обращений. */
    const PHONE_TO_CALL = '+7 (499) 788-6-000';

    /** @const string PHONE_TO_CALL_MOBILE телефон для обращений (тэг tel). */
    const PHONE_TO_CALL_MOBILE = '+74997886000';

    /** @const string IBLOCK_TYPE_CONTENT Тип инфоблока Контент. */
    const IBLOCK_TYPE_CONTENT = 'content';

    /** @const string IBLOCK_CODE_RENTA Код инфоблока Аренда. */
    const IBLOCK_CODE_RENTA = 'renta';

    /** @const string IBLOCK_CODE_RENTA Код инфоблока Файлы объектов. */
    const IBLOCK_CODE_OBJECT_FILES = 'files-objects';

    /** @const string SECONDS_IN_HOUR Секунд в часе. */
    const SECONDS_IN_HOUR = 3600;

    /** @const string COOKIES_SECTION_NAME Название куки, сохраняющей просмотренный раздел. */
    const COOKIES_SECTION_NAME = 'gofficeSectionViewed';

    /** @const string SECONDS_IN_DAY Секунд в сутках. */
    const SECONDS_IN_DAY = 86400;

    /** @const string SECONDS_IN_WEEK Секунд в неделе. */
    const SECONDS_IN_WEEK = 604800;

    /** @const string SECONDS_IN_MONTH Секунд в месяце. */
    const SECONDS_IN_MONTH = 2592000;

    /** @const string FORM_SUBSCRIBE Символьный код формы подписки.  */
    const FORM_SUBSCRIBE = 'FORM_SUBSCRIBE';

    /** @const string FORM_CALLBACK Символьный код формы Обратный звонок.  */
    const FORM_CALLBACK = 'FORM_FEEDBACK_CALLBACK';

    /** @const integer INFINITY Типа бесконечность.  */
    const INFINITY = 999999999;
}
