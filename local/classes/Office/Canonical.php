<?php

namespace Local\Office;

/**
 * Class Canonical
 * Вывод канонических ссылок компонента из свойства.
 * @package Local\Lotus
 */
class Canonical
{
    /** @const string TARGET_ID ID отложенной функции */
    private const TARGET_ID = 'canonicalLink';
    /** @const string PAGE_PROPERTY_CANONICAL_NAME Навзание свойства папки для канонических страниц. */
    private const PAGE_PROPERTY_CANONICAL_NAME = 'canonical-link';
    /**
     * @param \CBitrixComponent $obBitrixComponent Объект Битрикс-компонента.
     * @param string            $sCanonicalLink    Каноническая ссылка.
     *
     * @return void
     */
    public static function passLink(\CBitrixComponent $obBitrixComponent, string $sCanonicalLink = ''): void
    {
        if (!$sCanonicalLink) {
            return;
        }
        // Канононические URL из свойства CANONICAL_URL
        $obBitrixComponent->__parent->__template->SetViewTarget(self::TARGET_ID); ?>
        <link rel="canonical" href="<?php echo self::getFullUrl($sCanonicalLink); ?>"/>
        <?php $obBitrixComponent->__parent->__template->EndViewTarget();
    }

    /**
     * Канонические ссылки для статических страниц. Берутся из
     * свойства папки canonical.
     *
     * @param string $sCanonicalUrl URL (для списковых страниц).
     *
     * @return void
     */
    public static function staticPage(string $sCanonicalUrl = ''): void
    {
        if (!$sCanonicalUrl) {
            $sCanonicalUrl = $GLOBALS['APPLICATION']->GetDirProperty(self::PAGE_PROPERTY_CANONICAL_NAME);
        }

        if ($sCanonicalUrl) {
            $sResultLine = sprintf('<link rel="canonical" href="%s" />', self::getFullUrl($sCanonicalUrl));
            $GLOBALS['APPLICATION']->AddViewContent(self::TARGET_ID, $sResultLine);
        }
    }

    /**
     * Вывод канонической ссылки в header.
     *
     * @return void
     */
    public static function show(): void
    {
        $GLOBALS['APPLICATION']->ShowViewContent(self::TARGET_ID);
    }

    /**
     * Проверка - HTTP или HTTPS.
     *
     * @return boolean
     */
    private static function isSecureConnection(): bool
    {
        return
            (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
            || $_SERVER['SERVER_PORT'] == 443;
    }

    /**
     * Получить полный (включая https, домен) путь к канонической странице.
     * @param string $sUrl Укороченный URL (без домена).
     *
     * @return string
     */
    private static function getFullUrl(string $sUrl = ''): string
    {
        $sCanonicalLink = $sUrl ?: $_SERVER['REQUEST_URI'];
        $sTypeHttp = self::isSecureConnection() ? 'https://' : 'http://';

        return $sTypeHttp.$_SERVER['HTTP_HOST'].$sCanonicalLink;
    }
}
