<?php

namespace Local\Office;

use Local\Constants;
use Local\Guta\IBlockElementManager;
use Local\Models\RentalObjects;
use Local\Models\Rentals;

/**
 * Class FilterData
 * Хелперы для подготовки данных для фильтра.
 * @package Local\Office
 */
class FilterData
{
    /**
     * Данные об объектах.
     *
     * @return array
     */
    public static function objects()
    {
        $obRentalModel = RentalObjects::query()
            ->filter(['ACTIVE' => 'Y'])
            ->sort(['SORT' => 'DESC'])
            ->select('ID', 'NAME', 'UF_CITY')
            ->getList();

        $arCollections = [];
        $obRentalModel->each(
        static function ($item, $key) use (&$arCollections) {
            $activeElements = \CIBlockSection::GetSectionElementsCount($item['ID'], ['CNT_ACTIVE' => 'Y']);
            // Если есть активные элементы в разделе.
            if ((int)$activeElements >= 1) {
                $arCollections['objects'][] =
                    [
                        'label' => $item['NAME'],
                        'value' => $item['ID'],
                        'city' => $item['UF_CITY'],
                    ];
            }

            $arCollections['city'][$item['UF_CITY']] =
            [
            'label' => $item['UF_CITY'],
            'value' => $item['UF_CITY'],
            ];
        }
        );

        $arCollections['city'] = array_values($arCollections['city']);

        return $arCollections;
    }

    /**
     * Все типы объектов.
     *
     * @return array
     * @throws \Bitrix\Main\LoaderException
     */
    public static function getTypes()
    {
        // Выборка с группировкой по типам лотов.
        $arTypes = IBlockElementManager::getIBlockElements(
            [
                'AR_ORDER' => ['SORT' => 'DESC'],
                'AR_FILTER' => ['ACTIVE' => 'Y', 'IBLOCK_CODE' => Constants::IBLOCK_CODE_RENTA],
                'AR_GROUP' => ['PROPERTY_TYPE'],
            ]
        );

        $arResult = [];

        foreach ($arTypes as $arItem) {
            $arResult[] = [
                'label' => $arItem['PROPERTY_TYPE_VALUE'],
                'value' => $arItem['PROPERTY_TYPE_ENUM_ID']
            ];
        }

        return $arResult;
    }

    /**
     * Минимальное и максимальные характеристики
     * для лотов (цена и площадь).
     *
     * @return array
     */
    public static function getMinMax(): array
    {
        $obRentalModel = Rentals::query()
            ->filter(['ACTIVE' => 'Y'])
            ->select('ID', 'NAME', 'PROPERTY_SQUARE', 'PROPERTY_PRICE_SQUARE')
            ->getList();

        /** @var integer $minSquare Минимальная площадь лота. */
        $minSquare = $obRentalModel->min('PROPERTY_SQUARE_VALUE');
        /** @var integer $maxSquare Максимальная площадь лота. */
        $maxSquare = $obRentalModel->max('PROPERTY_SQUARE_VALUE');
        /** @var  integer $minPrice Минимальная цена лота. */
        $minPrice = $obRentalModel->min('PROPERTY_PRICE_SQUARE_VALUE');
        /** @var integer $maxPrice Максимальная цена лота. */
        $maxPrice = $obRentalModel->max('PROPERTY_PRICE_SQUARE_VALUE');

        return [
            'minsquare' => $minSquare,
            'maxsquare' => $maxSquare,
            'minprice' => $minPrice,
            'maxprice' => $maxPrice,
        ];
    }
}
