<?php

namespace Local\Services\Bitrix\WebForm;

use Bitrix\Main\LoaderException;
use CForm;
use InvalidArgumentException;
use Local\Services\Bitrix\Exceptions\IblockModelDuplicateException;
use Local\Services\Bitrix\Interfaces\FormSearcherInterface;
use Local\Services\Bitrix\WebForm\Exceptions\ErrorAddingWebFormException;

/**
 * Class FormProcessor
 * @package Local\Services\Bitrix\WebForm
 *
 * @since 29.10.2020
 */
class FormProcessor
{
    /**
     * @var FormParamsProcessor $formParamsProcessor Обработчик параметров формы.
     */
    private $formParamsProcessor;

    /**
     * @var FormResult $formResult Занесение результатов в базу.
     */
    private $formResult;

    /**
     * @var FormSearcherInterface $formSearcher  Поисковик по формам.
     */
    private $formSearcher;

    /**
     * @var CForm $form Битриксовый CForm.
     */
    private $form;

    /** @var string $formCode Код формы. */
    private $formCode = '';

    /** @var integer $idForm ID формы. */
    private $idForm;

    /** @var array $inboudData */
    private $inboundData = [];

    /**
     * FormProcessor constructor.
     *
     * @param FormParamsProcessor   $formParamsProcessor Обработчик параметров формы.
     * @param FormResult            $formResult          Занесение результатов в базу.
     * @param FormSearcherInterface $formSearcher        Поисковик по формам.
     * @param CForm                 $form                Битриксовый CForm.
     */
    public function __construct(
        FormParamsProcessor $formParamsProcessor,
        FormResult $formResult,
        FormSearcherInterface $formSearcher,
        CForm $form
    ) {
        $this->formParamsProcessor = $formParamsProcessor;
        $this->formResult = $formResult;
        $this->formSearcher = $formSearcher;
        $this->form = $form;
    }

    /**
     * Обработать форму.
     *
     * @return integer ID созданной формы.
     *
     * @throws ErrorAddingWebFormException   Не получилось добавить данные в форму.
     * @throws LoaderException               Не загружен модуль form.
     * @throws IblockModelDuplicateException Дубликат в базе.
     */
    public function processForm() : int
    {
        $parameters = $this->formParamsProcessor->setData(
            $this->inboundData
        )->getParameters($this->idForm);

        $this->formSearcher->setIdForm($this->idForm)
                           ->addFilter($this->inboundData);

        if ($this->formSearcher->exist()) {
            throw new IblockModelDuplicateException('Already have this user - phone & email saved in base.');
        }

        $result = $this->formResult->setNotifyByEmail(false)
                                   ->add($this->idForm, $parameters);

        if ($result->STATUS !== UpdateResult::STATUS_OK) {
            throw new ErrorAddingWebFormException(
                'Ошибка создания формы: ' . $result->RESULT
            );
        }

        return $result->RESULT;
    }

    /**
     * Сеттер кода формы. Сразу вычисляется ее ID.
     *
     * @param string $formCode Символьный код формы.
     *
     * @return $this
     */
    public function setFormCode(string $formCode) : self
    {
        $this->formCode = $formCode;

        $rsForm = $this->form::GetBySID($this->formCode);
        $arResult = $rsForm->Fetch();

        if ($arResult['ID'] > 0) {
            $this->idForm = $arResult['ID'];
            return $this;
        }

        throw new InvalidArgumentException(
            sprintf(
                'Форма с кодом %s не существует.',
                $formCode
            )
        );
    }

    /**
     * Сеттер параметров.
     *
     * @param array $data Параметры.
     *
     * @return $this
     */
    public function setData(array $data) : self
    {
        $this->inboundData = $data;

        return $this;
    }
}
