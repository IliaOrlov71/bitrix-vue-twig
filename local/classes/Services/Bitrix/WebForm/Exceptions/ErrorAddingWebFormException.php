<?php

namespace Local\Services\Bitrix\WebForm\Exceptions;

use Local\SymfonyTools\Framework\Exceptions\BaseException;

/**
 * Class ErrorAddingWebFormException
 * @package Local\Services\Bitrix\WebForm\Exceptions
 *
 * @since 29.10.2020
 */
class ErrorAddingWebFormException extends BaseException
{

}
