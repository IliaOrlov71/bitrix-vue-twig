<?php

namespace Local\Services\Bitrix\WebForm;

use Bitrix\Main\LoaderException;
use CFormCRM;
use CFormResult;

/**
 * Class FormResult
 * @package Local\Services\Bitrix\WebForm
 *
 * @since 29.10.2020
 *
 * @see https://github.com/ASDAFF/hipot.framework. Light refactoring.
 */
class FormResult
{
    /**
     * @var CFormResult $formResult Битриксовый CFormResult.
     */
    private $formResult;

    /**
     * @var CFormCRM $cFormCRM Битриксовый CFormCRM.
     */
    private $cFormCRM;

    /** @var boolean $notifyByEmail Уведомлять по email. */
    private $notifyByEmail = true;

    /**
     * FormResult constructor.
     *
     * @param CFormResult $formResult Битриксовый CFormResult.
     * @param CFormCRM    $cFormCRM   Битриксовый CFormCRM.
     */
    public function __construct(
        CFormResult $formResult,
        CFormCRM $cFormCRM
    ) {
        $this->formResult = $formResult;
        $this->cFormCRM = $cFormCRM;
    }

    /**
     * Добавить в модуль веб-формы в форму данные
     *
     * @param integer $idWebform    id формы, для которой пришел ответ
     * @param array   $arValuesForm = <pre>array (
     *   [WEB_FORM_ID] => 3
     *   [web_form_submit] => Отправить
     *
     *   [form_text_18] => aafafsfasdf
     *   [form_text_19] => q1241431342
     *   [form_text_21] => afsafasdfdsaf
     *   [form_textarea_20] =>
     *   [form_text_22] => fasfdfasdf
     *   [form_text_23] => 31243123412впывапвыапывпыв аывпывпыв
     *
     *   18, 19, 21 - ID ответов у вопросов https://yadi.sk/i/_9fwfZMvO2kblA
     *   )</pre>
     *
     * @return UpdateResult
     * @throws LoaderException Ошибка загрузки модуля форм.
     *
     * @see https://github.com/ASDAFF/hipot.framework
     */
    public function add(int $idWebform, array $arValuesForm = [])
    {
        global $strError;

        // add result like bitrix:form.result.new
        $arValuesForm['WEB_FORM_ID'] = $idWebform;
        if ($arValuesForm['WEB_FORM_ID'] <= 0) {
            return new UpdateResult(['RESULT' => 'форма с ID ' . $idWebform . ' не найдена.', 'STATUS' => UpdateResult::STATUS_ERROR]);
        }

        $arValuesForm['web_form_submit'] = 'Отправить';

        if ($idResult = $this->formResult->Add($idWebform, $arValuesForm)) {
            if ($idResult) {
                $this->notifyByEmail($idWebform, $idResult);

                return new UpdateResult(['RESULT' => $idResult, 'STATUS' => UpdateResult::STATUS_OK]);
            }
        }

        return new UpdateResult(['RESULT' => $strError, 'STATUS' => UpdateResult::STATUS_ERROR]);
    }

    /**
     * Сеттер - отправлять почту или нет.
     *
     * @param boolean $notifyByEmail Отправлять почту.
     *
     * @return $this
     */
    public function setNotifyByEmail(bool $notifyByEmail): self
    {
        $this->notifyByEmail = $notifyByEmail;

        return $this;
    }

    /**
     * Отправить почту.
     *
     * @param mixed $idWebform ID формы.
     * @param mixed $idResult  ID результата.
     *
     * @return void
     */
    private function notifyByEmail($idWebform, $idResult) : void
    {
        if (!$this->notifyByEmail) {
            return;
        }

        // send email notifications
        $this->cFormCRM::onResultAdded($idWebform, $idResult);
        $this->formResult->SetEvent($idResult);
        $this->formResult->Mail($idResult);
    }
}
