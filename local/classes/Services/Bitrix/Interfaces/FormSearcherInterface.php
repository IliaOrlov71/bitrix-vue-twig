<?php

namespace Local\Services\Bitrix\Interfaces;

use InvalidArgumentException;

/**
 * Interface FormSearcherInterface
 * @package Local\Services\Bitrix\Interfaces
 *
 * @since 30.10.2020
 */
interface FormSearcherInterface
{
    /**
     * Фильтр.
     *
     * @param array $value Массив вида
     * ['EMAIL' => 'f@f.ru']. Или [['EMAIL' => 'f@f.ru'], ['PHONE' => '+791567899']].
     *
     * @return $this
     */
    public function addFilter(array $value);

    /**
     * Поиск.
     *
     * @return array
     *
     * @throws InvalidArgumentException Не задали ID формы.
     */
    public function filter() : array;

    /**
     * Проверка на существование форм с заданными параметрами.
     *
     * @return boolean
     *
     * @throws InvalidArgumentException Не задали ID формы.
     */
    public function exist() : bool;

    /**
     * ID формы.
     *
     * @param integer $idForm ID формы.
     *
     * @return $this
     */
    public function setIdForm(int $idForm);

    /**
     * Ограничить выборку.
     *
     * @param integer $limit Ограничить выборку.
     *
     * @return $this
     */
    public function setLimit(int $limit);

    /**
     * Код формы. Сразу получается ID формы.
     *
     * @param string $formCode Символьный код формы.
     *
     * @return $this
     *
     * @throws InvalidArgumentException Если форма не существует.
     */
    public function setFormCode(string $formCode);
}
