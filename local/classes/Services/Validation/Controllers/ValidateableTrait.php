<?php

namespace Local\Services\Validation\Controllers;

use Local\Services\Validation\Exceptions\ValidateErrorException;
use Local\Services\Validation\Traits\Validatable;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ValidateableTrait
 * Трэйт валидации для контроллеров.
 * @package Local\Services\Validation\Controllers
 *
 * @since 08.09.2020
 * @since 10.09.2020 Изменен тип исключений.
 * @since 02.11.2020 Рефакторинг. Валидация пустого Request (nullable возможности).
 */
trait ValidateableTrait
{
    use Validatable;

    /**
     * Валидирует Request.
     *
     * @param Request    $request Request.
     * @param array|null $rules   Правила валидации.
     *
     * @return boolean
     * @throws ValidateErrorException Ошибки валидации.
     */
    public function validateRequest(
        Request $request,
        array $rules = null
    ) : bool {
        $data = $this->getDataRequest($request);

        if (empty($data)) {
            throw new ValidateErrorException('Empty input params.');
        }

        return $this->validate(
            $data,
            $rules
        );
    }

    /**
     * Валидирует Request. C возможностью пустых параметров.
     *
     * @param Request    $request Request.
     * @param array|null $rules   Правила валидации.
     *
     * @return boolean
     * @throws ValidateErrorException Ошибки валидации.
     *
     * @since 02.11.2020
     */
    public function validateNullableRequest(
        Request $request,
        array $rules = null
    ) : bool {
        $data = $this->getDataRequest($request);

        return $this->validate(
            $data,
            $rules
        );
    }

    /**
     * Данные для валидации.
     *
     * @param Request $request Request.
     *
     * @return array
     *
     * @since 02.11.2020
     */
    private function getDataRequest(Request $request) : array
    {
        // Тип запроса.
        $typeRequest = $request->getMethod();

        return ($typeRequest === 'POST') ?
            $request->request->all()
            :
            $request->query->all();
    }
}
