<?php

namespace Local\Services\Seo\Meta;

use Bitrix\Iblock\InheritedProperty\SectionValues;
use CIBlockSection;
use Local\Services\Seo\Meta\Traits\MetaTrait;

/**
 * Class MetaSection
 * @package Local\Util\Meta
 *
 * @since 14.12.2020
 */
class MetaSection
{
    use MetaTrait;

    /**
     * @var CIBlockSection $blockSection Битриксовый CIBlockSection.
     */
    private $blockSection;

    /**
     * @var MetaTagsParserGuzzle $metaGuzzle Получение реальных title & description.
     */
    private $metaGuzzle;

    /**
     * @var string $baseUri Хост (c http/https).
     */
    private $baseUri;

    /**
     * MetaSection constructor.
     *
     * @param CIBlockSection       $blockSection Битриксовый CIBlockSection.
     * @param MetaTagsParserGuzzle $metaGuzzle   Получение реальных title & description.
     * @param string               $baseUri      Хост (c http/https).
     */
    public function __construct(
        CIBlockSection $blockSection,
        MetaTagsParserGuzzle $metaGuzzle,
        string $baseUri
    ) {
        $this->blockSection = $blockSection;
        $this->metaGuzzle = $metaGuzzle;
        $this->baseUri = $baseUri;
    }

    /**
     * Движуха.
     *
     * @param integer $iblockId ID инфоблока.
     *
     * @return array
     */
    public function get(int $iblockId) : array
    {
        $arResult = [];

        $db = $this->blockSection::GetList(
            [],
            ['IBLOCK_ID' => $iblockId, 'ACTIVE' => 'Y'],
            false,
            ['ID', 'NAME', 'SECTION_PAGE_URL', 'UF_*']
        );

        while ($result = $db->GetNext()) {
            $ipropValues = new SectionValues($iblockId, $result['ID']);
            $values = $ipropValues->queryValues();

            $realMetaTags = [
                'title' => '',
                'description' => ''
            ];

            if (!empty($result['SECTION_PAGE_URL']) && $this->collectRealData) {
                $realMetaTags = $this->metaGuzzle->getMeta($this->baseUri . $result['SECTION_PAGE_URL']);
            }

            $canonicalLink = '';
            if (!empty($result['UF_SEO_CANONICAL'])) {
                $canonicalLink = $result['UF_SEO_CANONICAL'];
            }

            if (!empty($result['UF_CANONICAL_LINK'])) {
                $canonicalLink = $result['UF_CANONICAL_LINK'];
            }

            $result = [
                'NAME' => $result['NAME'],
                'DETAIL_PAGE_URL' => strtolower($this->baseUri . $result['SECTION_PAGE_URL']),
                'title' => $values['SECTION_META_TITLE']['VALUE'],
                'description' => $values['SECTION_META_DESCRIPTION']['VALUE'],
                'canonical' => $canonicalLink,
            ];

            if ($this->collectRealData) {
                $result['404'] = !$realMetaTags ? 'Y' : 'N';
                $result['real_title'] = $realMetaTags['title'] ?? '';
                $result['real_description'] = $realMetaTags['description'] ?? '';
            }

            $arResult[] = $result;
        }

        return $arResult;
    }
}
