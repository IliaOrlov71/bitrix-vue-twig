<?php

namespace Local\Services\Seo\Meta\Exception;

use Local\SymfonyTools\Framework\Exceptions\BaseException;

/**
 * Class IblockNotFoundException
 * @package Local\Services\Seo\Meta\Exception
 *
 * @since 15.12.2020
 */
class IblockNotFoundException extends BaseException
{

}