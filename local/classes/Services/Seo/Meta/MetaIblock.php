<?php

namespace Local\Services\Seo\Meta;

use CIBlock;
use Local\Services\Seo\Meta\Exception\IblockNotFoundException;
use Local\Services\Seo\Meta\Traits\MetaTrait;

/**
 * Class MetaIblock
 * @package Local\Services\Seo\Meta
 *
 * @since 14.12.2020
 */
class MetaIblock
{
    use MetaTrait;

    /**
     * @var MetaSection $metaSection Обработчик разделов.
     */
    private $metaSection;

    /**
     * @var MetaElement $metaElement Обработчик элементов.
     */
    private $metaElement;

    /**
     * @var CIBlock $block Битриксовый CIBlock.
     */
    private $block;

    /**
     * MetaIblock constructor.
     *
     * @param MetaSection $metaSection Обработчик разделов.
     * @param MetaElement $metaElement Обработчик элементов.
     * @param CIBlock     $block       Битриксовый CIBlock.
     */
    public function __construct(
        MetaSection $metaSection,
        MetaElement $metaElement,
        CIBlock $block
    ) {
        $this->metaSection = $metaSection;
        $this->metaElement = $metaElement;
        $this->block = $block;
    }

    /**
     * Движуха.
     *
     * @param integer $iblockId ID инфоблока.
     *
     * @return array
     * @throws IblockNotFoundException Инфоблок не найден.
     */
    public function collect(int $iblockId)
    {
        $this->metaSection->setCollectRealData($this->collectRealData);
        $this->metaElement->setCollectRealData($this->collectRealData);

        $this->checkExistIblock($iblockId);
        $arResult = $this->metaSection->get($iblockId);

        return array_merge($arResult, $this->metaElement->get($iblockId));
    }

    /**
     * Движуха. По коду инфоблоока
     *
     * @param string $iblockCode Код инфоблока.
     *
     * @return array
     * @throws IblockNotFoundException Инфоблок не найден.
     */
    public function collectByCode(string $iblockCode)
    {
        $iblockId = $this->getIBlockIdByCode($iblockCode);

        $this->metaSection->setCollectRealData($this->collectRealData);
        $this->metaElement->setCollectRealData($this->collectRealData);

        $arResult = $this->metaSection->get($iblockId);

        return array_merge($arResult, $this->metaElement->get($iblockId));
    }

    /**
     * Попытка определить, что передали - ID инфоблока или его код.
     *
     * @param mixed $argument Либо ID инфоблока, либо код инфоблока.
     *
     * @return array
     * @throws IblockNotFoundException Инфоблок не найден.
     */
    public function autodetectTypeArgument($argument) : array
    {
        if (is_numeric($argument)) {
            return $this->collect((int)$argument);
        }

        return $this->collectByCode((string)$argument);
    }

    /**
     * Проверить инфоблок на существование.
     *
     * @param integer $iblockId ID инфоблока.
     *
     * @return boolean
     * @throws IblockNotFoundException Инфоблок не найден.
     */
    private function checkExistIblock(int $iblockId) : bool
    {
        $res = $this->block::GetList(
            [],
            ['ACTIVE' => 'Y', 'ID' => $iblockId]
        );

        $arResult = $res->Fetch();
        if ($arResult['ID'] > 0) {
            return true;
        }

        throw new IblockNotFoundException('Инфоблок с ID '. $iblockId . ' не найден');
    }

    /**
     * ID инфоблока по коду.
     *
     * @param string $iblockCode Код инфоблока.
     *
     * @return integer
     *
     * @throws IblockNotFoundException Инфоблок не найден.
     */
    public function getIBlockIdByCode(string $iblockCode) : int
    {
        $res = $this->block::GetList(
            [],
            ['ACTIVE' => 'Y', 'CODE' => $iblockCode]
        );

        $arResult = $res->Fetch();

        if ($arResult['ID'] > 0) {
            return $arResult['ID'];
        }

        throw new IblockNotFoundException('Инфоблок с кодом '. $iblockCode . ' не найден');
    }
}
