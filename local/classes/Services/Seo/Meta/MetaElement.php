<?php

namespace Local\Services\Seo\Meta;

use Bitrix\Iblock\InheritedProperty\ElementValues;
use CIBlockElement;
use Local\Services\Seo\Meta\Traits\MetaTrait;

/**
 * Class MetaElement
 * @package Local\Services\Seo\Meta
 *
 * @since 14.12.2020
 */
class MetaElement
{
    use MetaTrait;

    /**
     * @var CIBlockElement $blockElement Битриксовый CIBlockElement.
     */
    private $blockElement;

    /**
     * @var MetaTagsParserGuzzle $metaGuzzle Получение реальных title & description.
     */
    private $metaGuzzle;

    /**
     * @var string $baseUri Хост (c http/https).
     */
    private $baseUri;

    /**
     * MetaSection constructor.
     *
     * @param CIBlockElement       $blockElement Битриксовый CIBlockElement.
     * @param MetaTagsParserGuzzle $metaGuzzle   Получение реальных title & description.
     * @param string               $baseUri      Хост (c http/https).
     */
    public function __construct(
        CIBlockElement $blockElement,
        MetaTagsParserGuzzle $metaGuzzle,
        string $baseUri
    ) {
        $this->blockElement = $blockElement;
        $this->metaGuzzle = $metaGuzzle;
        $this->baseUri = $baseUri;
    }

    /**
     * Движуха.
     *
     * @param integer $iblockId ID инфоблока.
     *
     * @return array
     */
    public function get(int $iblockId) : array
    {
        $arResult = [];

        $arFilter = ['IBLOCK_ID' => $iblockId, 'ACTIVE' => 'Y'];
        $db = $this->blockElement::GetList(
            [],
            $arFilter,
            false,
            false,
            ['ID', 'NAME', 'DETAIL_PAGE_URL', 'PROPERTY_SEO_CANONICAL_LINK']
        );

        while ($result = $db->GetNext()) {
            $ipropValues = new ElementValues($iblockId, $result['ID']);
            $values = $ipropValues->queryValues();

            $realMetaTags = [
                'title' => '',
                'description' => ''
            ];

            if (!empty($result['DETAIL_PAGE_URL']) && $this->collectRealData) {
                $realMetaTags = $this->metaGuzzle->getMeta($this->baseUri . $result['DETAIL_PAGE_URL']);
            }

            $result = [
              'NAME' => $result['NAME'],
              'DETAIL_PAGE_URL' => strtolower($this->baseUri . $result['DETAIL_PAGE_URL']),
              'title' => $values['ELEMENT_META_TITLE']['VALUE'],
              'description' => $values['ELEMENT_META_DESCRIPTION']['VALUE'],
              'canonical' => $result['PROPERTY_SEO_CANONICAL_LINK_VALUE'],
            ];

            if ($this->collectRealData) {
                $result['404'] = !$realMetaTags ? 'Y' : 'N';
                $result['real_title'] = $realMetaTags['title'] ?? '';
                $result['real_description'] = $realMetaTags['description'] ?? '';
            }

            $arResult[] = $result;
        }

        return $arResult;
    }
}
