<?php

namespace Local\Services\Seo\Meta;

/**
 * Class MetaTagsParser
 * Retrive meta tags (title, description) by url.
 * @package Local\Services\Seo\Meta
 *
 * @since 14.12.2020
 */
class MetaTagsParser
{
    /**
     *
     * @param string  $url     URL.
     * @param array   $tags    array ('description', 'keywords')
     * @param integer $timeout Timeout. Seconds.
     *
     * @return mixed false| array
     */
    public function getMeta($url, $tags = ['description', 'keywords'], $timeout = 10) {

        $html = $this->curl($url, $timeout);
        if (!$html) {
            return false;
        }

        $doc = new \DOMDocument();
        @$doc->loadHTML($html);
        $nodes = $doc->getElementsByTagName('title');

        $arResult = [];

        $arResult['title'] = $nodes->item(0)->nodeValue;
        $metas = $doc->getElementsByTagName('meta');

        for ($i = 0; $i < $metas->length; $i++) {
            $meta = $metas->item($i);

            foreach($tags as $tag) {
                if ($meta->getAttribute('name') == $tag) {
                    $arResult[$tag] = $meta->getAttribute('content');
                }
            }
        }
        return $arResult;
    }

    /**
     * Curl the document.
     *
     * @param string  $url     URL.
     * @param integer $timeout Timeout.
     *
     * @return string $data
     */
    private function curl($url, $timeout = 10) : string
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

        $data = curl_exec($ch);

        /* Check for 404 (file not found). */
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($httpCode === 404) {
            return '';
        }

        curl_close($ch);

        return $data;
    }
}