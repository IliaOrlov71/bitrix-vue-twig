<?php

namespace Local\Services\Seo\Meta;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class MetaTagsParserGuzzle
 * Retrive meta tags (title, description) by url.
 * @package Local\Services\Seo\Meta
 *
 * @since 14.12.2020
 */
class MetaTagsParserGuzzle
{
    /**
     * @var Client $client Клиент.
     */
    private $client;

    /**
     * MetaGuzzle constructor.
     *
     * @param Client $client Транспорт.
     */
    public function __construct(
        Client $client
    ) {
        $this->client = $client;
    }

    /**
     *
     * @param string  $url     URL.
     * @param array   $tags    Array ('description', 'keywords').
     * @param integer $timeout Timeout. Seconds.
     *
     * @return mixed false| array
     */
    public function getMeta(
        string $url,
        array $tags = ['description', 'keywords'],
        int $timeout = 10
    ) {
        // sleep(0.75);
        try {
            $html = $this->get($url, $timeout);
        } catch (Exception $e) {
            return false;
        }

        if (!$html) {
            return false;
        }

        $doc = new \DOMDocument();
        @$doc->loadHTML('<?xml encoding="utf-8" ?>' . $html);
        $nodes = $doc->getElementsByTagName('title');

        $arResult = [];

        $arResult['title'] = $nodes->item(0)->nodeValue;
        $metas = $doc->getElementsByTagName('meta');

        for ($i = 0; $i < $metas->length; $i++) {
            $meta = $metas->item($i);

            foreach ($tags as $tag) {
                if ($meta->getAttribute('name') == $tag) {
                    $arResult[$tag] = $meta->getAttribute('content');
                }
            }
        }
        return $arResult;
    }

    /**
     * Отправка GET запроса
     *
     * @param string  $url     URL.
     * @param integer $timeout Timeout.
     *
     * @return string
     * @throws Exception Ошибки транспорта.
     */
    private function get(string $url, int $timeout = 10)
    {
        try {
            $response = $this->client->request(
                'GET',
                $url,
                ['connect_timeout' => $timeout, 'timeout' => $timeout]
            );
        } catch (GuzzleException $e) {
            throw new Exception(
                $e->getMessage()
            );
        }

        if ($response->getStatusCode() === 400) {
            return '';
        }

        return $response->getBody()->getContents();
    }
}
