<?php

namespace Local\Services\Seo\Meta\Traits;

/**
 * Trait MetaTrait
 * @package Local\Services\Seo\Meta\Traits
 *
 * @since 15.12.2020
 */
trait MetaTrait
{
    /**
     * @var boolean $collectRealData Собирать реальные мета тэги или нет.
     */
    private $collectRealData = false;

    /**
     * @param boolean $collectRealData Собирать реальные мета тэги или нет.
     *
     * @return $this
     */
    public function setCollectRealData(bool $collectRealData): self
    {
        $this->collectRealData = $collectRealData;

        return $this;
    }
}
