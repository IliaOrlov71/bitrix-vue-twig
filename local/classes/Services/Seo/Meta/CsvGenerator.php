<?php

namespace Local\Services\Seo\Meta;

use League\Csv\CannotInsertRecord;
use League\Csv\CharsetConverter;
use League\Csv\Exception;
use League\Csv\Writer;

/**
 * Class CsvGenerator
 * @package Local\Util\Meta
 *
 * @since 14.12.2020
 */
class CsvGenerator
{
    /**
     * @var Writer $csv CSV handler.
     */
    private $csv;

    /**
     * CsvGenerator constructor.
     */
    public function __construct()
    {
        $this->csv = Writer::createFromString('');
    }

    /**
     * Движуха. Получить текстовый контент csv файла.
     *
     * @param array $arResult Массив с исходными данными.
     * @param array $arHeader Заголовок CSV.
     *
     * @return string
     *
     * @throws CannotInsertRecord|Exception
     */
    public function get(array $arResult, array $arHeader = []) : string
    {
        $this->csv->setDelimiter(';');

        if (!empty($arHeader)) {
            $this->csv->insertOne($arHeader);
        }

        $this->csv->insertAll($arResult);

        return $this->csv->getContent();
    }

    /**
     * Преобразовывать результат в 1251.
     *
     * @return $this;
     */
    public function to1251() : self
    {
        CharsetConverter::addTo($this->csv, 'UTF-8', 'Windows-1251');

        return $this;
    }
}