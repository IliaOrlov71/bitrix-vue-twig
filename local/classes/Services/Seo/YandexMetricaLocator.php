<?php

namespace Local\Services\Seo;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class YandexMetricaLocator
 * @package Local\Services\Seo
 *
 * @since 05.02.2021
 */
class YandexMetricaLocator
{
    /**
     * @var Environment $twig Твиг.
     */
    private $twig;

    /**
     * @var array $config Конфигурация из контейнера.
     */
    private $config;

    /**
     * @var boolean $disabled
     */
    private $disabled = false;

    /**
     * @var string $currentIdMetric Текущий ID Метрики.
     */
    private $currentIdMetric;

    /**
     * YandexMetricaLocator constructor.
     *
     * @param Environment $twig   Твиг.
     * @param array       $config Конфигурация из контейнера.
     */
    public function __construct(
        Environment $twig,
        array $config
    ) {
        $this->twig = $twig;
        $this->config = $config;
        $this->currentIdMetric = $this->config['defaultId'] ?? '';
    }

    /**
     * Вывести скрипт подключения Яндекс.Метрики.
     *
     * @param integer $idMetric ID метрики. Если 0, то берется из конфига.
     *
     * @return void
     * @throws LoaderError | RuntimeError | SyntaxError Ошибки Твига.
     */
    public function render(int $idMetric = 0) : void
    {
        if ($this->disabled) {
            return;
        }

        echo $this->twig->render(
            $this->config['template'],
            [
            'id' => $idMetric ?: $this->currentIdMetric
            ]
        );
    }

    /**
     * @return $this
     */
    public function disable() : self
    {
        $this->disabled = true;

        return $this;
    }

    /**
     * Текущий ID метрики.
     *
     * @return string
     */
    public function getCurrentId() : string
    {
        return $this->currentIdMetric;
    }

    /**
     * @param string $currentIdMetric ID метрики.
     *
     * @return $this
     */
    public function setIdMetric(string $currentIdMetric): self
    {
        $this->currentIdMetric = $currentIdMetric;

        return $this;
    }
}
