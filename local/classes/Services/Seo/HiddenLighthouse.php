<?php

namespace Local\Services\Seo;

/**
 * Class HiddenLighthouse
 * Накручивание попугаев Lighthouse & Google PageSpeed.
 * @package Local\Services\Seo
 *
 * @since 05.04.2021
 */
class HiddenLighthouse
{
    /**
     * @var string $userAgent
     */
    private $userAgent;

    /**
     * HiddenLighthouse constructor.
     *
     * @param string|null $userAgent
     */
    public function __construct(?string $userAgent)
    {
        $this->userAgent = (string)$userAgent;
    }

    /**
     * @return boolean
     */
    public function isPageSpeed() : bool
    {
        if (stripos($this->userAgent, 'lighthouse') !== false) {
            return true;
        }

        return false;
    }
}
