<?php

namespace Local\Services\Interfaces;

use Exception;
use Local\Services\Transport\RequestMis;

/**
 * Interface HttpTransportInterface
 * @package Local\Services\Interfaces
 */
interface HttpTransportInterface
{
    /**
     * Отправка POST запроса.
     *
     * @param string $method Ссылка на метод.
     * @param array  $data   Массив с данными.
     *
     * @return mixed
     */
    public function post(string $method, array $data);

    /**
     * Отправка GET запроса.
     *
     * @param string $method Ссылка на метод c параметрами.
     *
     * @return string
     * @throws Exception
     */
    public function get(string $method);
}
