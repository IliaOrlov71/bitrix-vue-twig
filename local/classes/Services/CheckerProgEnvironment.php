<?php

namespace Local\Services;

use Local\Util\ErrorScreen;

/**
 * Class CheckerProgEnvironment
 * Проверка конифгурации на соответствие необходимым требованиям.
 * @package Local\Services
 *
 * @since 03.12.2020
 */
class CheckerProgEnvironment
{
    /** @var boolean $debug DEBUG из .env. */
    private $debug;

    /**
     * @var ErrorScreen $errorScreen Экран смерти.
     */
    private $errorScreen;

    /**
     * @var string[] $extensionsBag Требуемые для работы PHP extensions.
     */
    private $extensionsBag = [
        [
            'method' => 'checkPhpIntl',
            'error_text' => 'Расширение PHP Intl у Вас не установлено. Проследуйте (Open Server):
                   Дополнительно -> Конфигурация -> PHP 7.3(1). Найдите и раскомментируйте строчку extension=php_intl.dll.
                   Перезагрузите OpenServer.'
        ]
    ];

    /**
     * CheckerProgEnvironment constructor.
     *
     * @param boolean     $debug       DEBUG из .env.
     * @param ErrorScreen $errorScreen Экран смерти.
     */
    public function __construct(
        bool $debug,
        ErrorScreen $errorScreen
    ) {
        $this->debug = $debug;
        $this->errorScreen = $errorScreen;
    }

    /**
     * Проверка на необходимые extensions.
     *
     * @return void
     */
    public function check() : void
    {
        if (!$this->debug) {
            return;
        }

        foreach ($this->extensionsBag as $extension) {
            $method = $extension['method'];
            if (!$this->{$method}()) {
                $this->errorScreen->die(
                    $extension['error_text']
                );
            }
        }
    }

    /**
     * Проверка на подключение расширения PHP INTL.
     *
     * @return boolean
     */
    public function checkPhpIntl() : bool
    {
        return function_exists('collator_create');
    }
}
