<?php

namespace Local\Services\Transport;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Local\MIS\Config\Config;
use Local\Services\Interfaces\HttpTransportInterface;

/**
 * Class RequestMisGuzzle
 * @package Local\Services\Transport
 */
class RequestMisGuzzle implements HttpTransportInterface
{
    /**
     * @var Client $client CURL клиент.
     */
    private $client;

    /** @var string $url API MIS. */
    private $url;

    /**
     * RequestMis constructor.
     *
     * @param Client $curl   GuzzleClient.
     * @param Config $config Конфигуратор API MIS.
     */
    public function __construct(
        Client $curl,
        Config $config
    ) {
        $this->client = $curl;
        $this->url = $config->baseApiUrl();
    }

    /**
     * Отправка POST запроса
     *
     * @param string $method Ссылка на метод.
     * @param array  $data   Массив с данными.
     *
     * @return string
     * @throws Exception Ошибки транспорта.
     *
     * @since 28.11.2020 Баг-фикс: ответ становится не пустой при использовании middleware логгера.
     */
    public function post(string $method, array $data)
    {
        try {
            $response = $this->client->request('POST', $this->url . $method, [
                'body' => json_encode($data)
            ]);
        } catch (GuzzleException $e) {
            throw new Exception('Post Request Error: ' . $e->getMessage());
        }

        /**
         * Баг-фикс: ответ становится не пустой при использовании middleware логгера.
         */
        $response->getBody()->rewind();

        return $response->getBody()->getContents();
    }

    /**
     * Отправка GET запроса
     *
     * @param string $method Ссылка на метод c параметрами.
     * Например "GetPromoActionImage?appCode=com.postmodern.mobimedReact&promoactionId=111".
     *
     * @return string
     * @throws Exception Ошибки транспорта.
     */
    public function get(string $method)
    {
        try {
            $response = $this->client->request('GET', $this->url.$method);
        } catch (GuzzleException $e) {
            throw new Exception(
                'Get Request Error: ' . $e->getMessage()
            );
        }

        return $response->getBody()->getContents();
    }
}
