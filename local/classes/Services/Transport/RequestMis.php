<?php

namespace Local\Services\Transport;

use Curl\Curl; // Обертка над curl
use Exception;
use Local\MIS\Config\Config;
use Local\Services\Interfaces\HttpTransportInterface;

/**
 * Class RequestMis
 * Запросы к МИС.
 * @package Local\Services\Transport
 */
class RequestMis implements HttpTransportInterface
{
    /**
     * @var Curl $curl CURL клиент.
     */
    private $curl;

    /** @var string $url API MIS. */
    private $url;

    /**
     * RequestMis constructor.
     *
     * @param Curl   $curl   Curl.
     * @param Config $config Конфигуратор API MIS.
     */
    public function __construct(
        Curl $curl,
        Config $config
    ) {
        $this->curl = $curl;

        $this->url = $config->baseApiUrl();
    }

    /**
     * Отправка POST запроса.
     *
     * @param string $method Ссылка на метод.
     * @param array  $data   Массив с данными.
     *
     * @return string
     * @throws Exception Ошибки транспорта.
     */
    public function post(string $method, array $data)
    {
        $curl = $this->curl;
        $curl->setHeader('Content-Type', ' application/json');

        /**
         * @since 05.11.2020
         * Временно отключил проверку качества сертификата.
         * (к вечеру не актуально - сертификат обновили)
         */

        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, 0);

        $curl->post($this->url . $method, json_encode($data));

        if ($curl->error) {
            throw new Exception('Post Request Error: ' . $curl->errorCode . ': ' . $curl->errorMessage);
        }

        return json_encode($curl->response);
    }

    /**
     * Отправка GET запроса.
     *
     * @param string $method Ссылка на метод c параметрами.
     * Например "GetPromoActionImage?appCode=com.postmodern.mobimedReact&promoactionId=111".
     *
     * @return string
     * @throws Exception
     */
    public function get(string $method)
    {
        $curl = $this->curl;
        $curl->get($this->url . $method);

        if ($curl->error) {
            throw new Exception('Get Request Error: ' . $curl->errorCode . ': ' . $curl->errorMessage);
        }

        return json_encode($curl->response);
    }
}
