<?php

namespace Local\Services;

/**
 * Class CaptchaProcessor
 * @package Local\Services
 *
 * @since 17.09.2020
 */
class CaptchaProcessor
{
    /** @const string CAPTCHA_SITE_KEY Код сайта для капчи. */
    private const CAPTCHA_SITE_KEY = '6LeQ6K0UAAAAANIdI-avPcAIbZYok5CVcibZ3QCv';
    /** @const string CAPTCHA_SECRET_KEY Секретный код для капчи. */
    private const CAPTCHA_SECRET_KEY = '6LeQ6K0UAAAAAFIU0HukxIeqU1lDW3SMdCv9Kj58';
    /** @const string URL проверки Google Captcha re2 на валидность. */
    private const CAPTCHA_VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';

    /** @var string $captchaResponse Ответ Google Captcha. */
    private $captchaResponse;

    /**
     * Сеттер ответа Google капчи.
     *
     * @param string $captchaResponse Ответ Google Captcha.
     *
     * @return CaptchaProcessor
     */
    public function setCaptchaResponse(string $captchaResponse): self
    {
        $this->captchaResponse = $captchaResponse;

        return $this;
    }

    /**
     * Проверка капчи на валидность.
     *
     * @return boolean
     */
    public function check() : bool
    {
        if (!$this->captchaResponse) {
            return false;
        }

        return $this->transport(
            self::CAPTCHA_VERIFY_URL,
            [
            'secret' => self::CAPTCHA_SECRET_KEY,
            'response' => $this->captchaResponse
            ]
        );
    }

    /**
     * Простенький запросник методом POST.
     *
     * @param string $url     URL.
     * @param array  $payload Нагрузка.
     *
     * @return bool
     */
    protected function transport(string $url, array $payload) : bool
    {
        $options = [
            'http' => [
                'method' => 'POST',
                'content' => http_build_query($payload)
            ]
        ];

        $context  = stream_context_create($options);
        $verify = file_get_contents($url, false, $context);
        $result = json_decode($verify);

        return $result->success;
    }
}
