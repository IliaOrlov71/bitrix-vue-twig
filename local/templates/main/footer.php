<?php
use Local\Guta\IBlockElementManager;
use Local\Guta\IBlockManager;
use Local\Constants;
?>
</main>
<!-- / main content -->
<!-- footer -->

    <footer class="footer">
    <div class="footer__inner">
        <?php $APPLICATION->IncludeComponent(
            'guta:footer',
            '.default',
            array(
            ),
            false
        );
        ?>
        <div class="footer__bottom">
            <div class="container">
                <?php $APPLICATION->IncludeComponent(
                    'bitrix:main.include',
                    '',
                    array(
                        'AREA_FILE_SHOW' => 'file',
                        'PATH' => '/__include__/footer/copyright.php',
                    ),
                    false
                );?>
            </div>
        </div>
    </div>
    <!--  Панель Битрикса  -->
    <?php $APPLICATION->ShowPanel() ?>
    <!-- / Панель Битрикса  -->
</footer>
<!-- / footer -->
<script src="<?= $assetManager->getEntry('main.js') ?>"></script>
</body>
</html>
