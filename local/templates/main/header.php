<?php

use Local\Constants;
use Bitrix\Main\Application;
use Local\Guta\MenuManager;
use Local\Office\Canonical;
use Local\Office\FilterData;

?>
    <!doctype html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <?php if (!env('DEBUG')) :?>
        <!-- Yandex.Metrika counter -->
        <script data-skip-moving="true">
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(41037009, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true,
                trackHash:true
            });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/41037009" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    <?php endif; ?>
    <title><?php $APPLICATION->ShowTitle() ?></title>

    <?php $APPLICATION->ShowHead();

    CJSCore::Init('jquery2');
    var_dump(env('DEBUG'));
    echo "asd";

    $sPath2Build = (env('DEBUG')) ? 'local/build/' : 'local/dist/';
    $assetManager = new Local\Util\Assets($sPath2Build);

    Canonical::show();?>

    <meta id="viewport" name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="<?= $assetManager->getEntry('global.css') ?>">

</head>
<body class="page page_<?= LANGUAGE_ID ?> page_<?php $APPLICATION->ShowProperty('page_type', 'secondary') ?>">
    <!-- SVG icons-->
    <div style="display: none;"><?=file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/' . $sPath2Build.'/images/icons.svg')?></div>

    <!-- pop-up "Заказать звонок" -->
    <div class="vue-component" data-component="PopupForm" data-initial='{"FORM_ID": "<?php echo Constants::FORM_CALLBACK?>"}'></div>
    <!-- pop-up сообщение -->
    <div class="vue-component" data-component="PopupMessage"></div>

    <!-- wrapper -->
    <div class="wrapper">

        <?php
    /** @var array $arMenu Массив с элементами меню. */
        try {
            $arMenu = MenuManager::getTreeMenuByDir('/', 'top', false, true);
        } catch (Exception $e) {
            $arMenu= [];
        }?>

        <!--подключение twig-->
        <?php

        $loader = new Twig_Loader_Filesystem($_SERVER['DOCUMENT_ROOT'] . '/local/twig/');

        $twig = new Twig_Environment($loader, [
            'debug' => env('DEBUG'),
            'cache' => $_SERVER['DOCUMENT_ROOT'] . '/bitrix/cache/twig',
        ]);
        ?>
        <!--/подключение twig-->

        <!-- header -->
        <div class="vue-component" id="vue-header" data-component="Header">
            <?= $twig->render('header.twig', ['arMenu' => $arMenu, 'phone' => Constants::PHONE_TO_CALL]); ?>
        </div>
        <!-- / header -->

        <!-- Мобильное меню -->
        <?= $twig->render('menu/mobile.twig', [
            'arResult' => $arMenu
        ]); ?>
        <!-- / Мобильное меню -->

        <!-- Search -->
        <?php
        $obRequest = Application::getInstance()->getContext()->getRequest();

        $APPLICATION->IncludeComponent(
            'guta:filter',
            '.default',
            [
                'CACHE_TYPE' => 'A',
                'CACHE_TIME' => Constants::SECONDS_IN_MONTH,
                'FILTER' => htmlspecialchars($obRequest->getQuery('filter')),
            ],
            false
        );

        ?>

        <!-- / Search -->

        <!-- main content -->
        <main class="main">

