<?php use Local\Constants;
use Local\Util\Supply;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="container">
    <h1 class="title title_regular">
        <?php echo $arResult['H1'] ?>
    </h1>
</div>

<section class="section container objectList">
    <?php if ($arParams['SHOW_GUTA_FILTER'] !== 'N') :?>
        <div id="vue-objectListFilter" class="vue-component" data-component="ObjectListFilter"
             data-initial='<?php echo json_encode($arResult['ALL_TYPES_FILTER'])?>'
        ></div>
    <?php endif;?>
    <div id="vue-objectList" class="vue-component" data-component="ObjectList">
        <div class="objectList__content">
            <div v-if="isShowSlider">
                <?php
                $APPLICATION->IncludeComponent(
                    'guta:objects.section.slider',
                    '',
                    [
                        'IBLOCK_SECTION_ID' => $arResult['ID'],
                        'CACHE_TYPE' => 'N',
                        'FILTERED_PARAMS' => !empty($arParams['FILTERED']) ? $arResult['ITEMS'] : []
                    ],
                    $component
                );
                ?>
            </div>

            <div class="objectList__cardList" v-if="!isShowSlider">
                <?php foreach ($arResult['ITEMS'] as $arItem) : ?>
                <div class="objectCard">
                    <a class="objectCard__image"
                       href="<?php echo $arItem['DETAIL_PAGE_URL'] ?>"
                       style="background-image: url('<?= $arItem['RESIZED_IMAGE'] ?>')">
                    </a>
                    <?php if (!empty($arItem['PROPERTIES']['TYPE']['VALUE'])) :?>
                        <div class="objectCard__tags">
                            <ul>
                                <?php foreach ($arItem['PROPERTIES']['TYPE']['VALUE'] as $key => $sTypesItem) :?>
                                    <li>
                                        <?php echo $sTypesItem?>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    <?php endif;?>

                    <?php if ($arItem['PROPERTIES']['SQUARE']['VALUE'] && !empty($arItem['PROPERTIES']['PRICE_SQUARE']['VALUE'])) : ?>
                        <div class="objectCard__priceGroup">
                            <?php if ($arItem['PROPERTIES']['SQUARE']['VALUE']) : ?>
                                <div class="objectCard__area">
                                    <span><?php echo $arItem['PROPERTIES']['SQUARE']['VALUE'] ?> м<sup>2</sup></span>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($arItem['PROPERTIES']['PRICE_SQUARE']['VALUE'])) : ?>
                                <div class="objectCard__priceYear">
                                    <?php echo Supply::normalizePrice($arItem['PROPERTIES']['PRICE_SQUARE']['VALUE'])?> ₽ за м<sup>2</sup> в год
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($arItem['PRICE_MONTH'])) :?>
                        <div class="objectCard__priceMonth">
                            <?php echo Supply::normalizePrice($arItem['PRICE_MONTH'])?> ₽ в месяц
                        </div>
                    <?php endif;?>

                    <div class="objectCard__title">
                        <?php echo $arItem['NAME'] ?>
                    </div>

                    <?php
                    // Если нет адреса в карточке лота, то берем адрес из свойства объекта.
                    $sAddressObject = $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?? '';
                    if (!$sAddressObject) {
                        $sAddressObject = $arResult['UF_ADDRESS'];
                    }

                    if ($sAddressObject) : ?>
                        <div class="objectCard__address">
                            <svg class="objectCard__mapIcon"><use xlink:href="#sprite-map-pin"></use></svg>
                            <?php echo $arItem['PROPERTIES']['CITY']['VALUE']?><br/>
                            <?php echo $sAddressObject ?>
                        </div>
                    <?php endif; ?>

                    <a href="<?php echo $arItem['DETAIL_PAGE_URL'] ?>" class="objectCard__moreBtn">
                        <svg><use xlink:href="#sprite-arrow_black"></use></svg>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="page-load-status objectList__status" v-show="isShowLoader">
                <transition-group name="infinite-scroll-animation">
                    <p class="infinite-scroll-request" key="request">Загрузка...</p>
                    <p class="infinite-scroll-error" key="error">Ошибка!</p>
                </transition-group>
            </div>


            <?php if (!empty($arResult['DESCRIPTION'])) :?>
                    <div>
                        <?php
                        echo $arResult['DESCRIPTION'];
                        ?>
                    </div>
            <?php endif;?>


            <?php
            // Отключил в рамках доработок.
            if (!empty($arResult['DOCUMENTS']) && 1 === 2) :?>
                <div class="objectList__links">
                    <?php foreach ($arResult['DOCUMENTS'] as $arDocumentItem) :?>
                        <?php if (!empty($arDocumentItem['FILE'])) :?>
                            <a class="objectList__linksItem" href="<?=$arDocumentItem['FILE']?>"
                               target="_blank">
                                <?php echo $arDocumentItem['TITLE']?>
                            </a>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>
            <?php endif;?>

        </div>
    </div>


    <div id="vue-lotAccordionBlock" class="vue-component" data-component="LotAccordionBlock">
        <?php
        $APPLICATION->IncludeComponent(
            'guta:documents',
            'accordeon',
            [
                'ID' => $arResult['ID'],
                'CACHE_TYPE' => 'N',
            ],
            $component
        );
        ?>
    </div>


    <?php if (!empty($arResult['NAV_STRING'])) : ?>
        <section id="pageNavNews" class="objectList__pagination">
            <?= $arResult['NAV_STRING'] ?>
        </section>
    <?php endif; ?>
</section>
