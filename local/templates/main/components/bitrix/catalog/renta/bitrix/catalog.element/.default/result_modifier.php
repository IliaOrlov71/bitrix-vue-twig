<?php

use Local\Custom\PDFFacade;
use Local\Models\RentalObjects;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}

$cp = $this->__component;

if (is_object($cp)) {
    // Канонические ссылки
    if (!empty($arResult['PROPERTIES']['CANONICAL_URL']['VALUE'])) {
        $cp->arResult['CANONICAL_URL'] = $arResult['PROPERTIES']['CANONICAL_URL']['VALUE'];
    } else {
        $cp->arResult['CANONICAL_URL'] = '';
    }

    // H1
    $cp->arResult['H1'] = $arResult['PROPERTIES']['H1']['VALUE'] ?: $arResult['NAME'];

    // PDF. Паспорт объекта.
    $sRegeneratedForm = PDFFacade::getHTML(
        $arResult['IBLOCK_ID'],
        $arResult['ID'],
        $arResult['IBLOCK_SECTION_ID']
    );
    $cp->arResult['PDF_FILE_PATH'] = PDFFacade::setPDF($sRegeneratedForm);

    // Адрес. Если не завели в базе, то брать из свойства Объекта.
    $cp->arResult['ADDRESS'] = $arResult['PROPERTIES']['ADDRESS']['VALUE'];
    if (empty($arResult['PROPERTIES']['ADDRESS']['VALUE'])) {
        $obRentalModel = RentalObjects::query()
            ->filter(['ACTIVE' => 'Y', 'ID' => $arResult['IBLOCK_SECTION_ID']])
            ->select('ID', 'UF_ADDRESS')
            ->getList()
            ->first();

        $arAddress = $obRentalModel->toArray();
        $cp->arResult['ADDRESS'] = $arAddress['UF_ADDRESS'];
    }

    // Расчет стоимости за месяц.
    $cp->arResult['PRICE_MONTH'] = (int)$arResult['PROPERTIES']['TOTAL_PRICE_MONTH']['VALUE'];

    if (!$cp->arResult['PRICE_MONTH']
        && $arResult['PROPERTIES']['PRICE_SQUARE']['VALUE']
        && $arResult['PROPERTIES']['SQUARE']['VALUE']) {
        $cp->arResult['PRICE_MONTH'] = round(
            ($arResult['PROPERTIES']['SQUARE']['VALUE'] * $arResult['PROPERTIES']['PRICE_SQUARE']['VALUE']) / 12
        );
    }

    $cp->setResultCacheKeys(
        [
            'CANONICAL_URL',
            'H1',
            'PDF_FILE_PATH',
            'ADDRESS',
            'PRICE_MONTH'
        ]
    );
}
