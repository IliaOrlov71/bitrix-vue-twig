<?php
use Local\Util\Supply;
use Local\Constants;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="section container lot">
    <?php if (!empty($arResult['PROPERTIES']['TYPE']['VALUE'])) :?>
        <ul class="lot__tag-list">
            <?php foreach ($arResult['PROPERTIES']['TYPE']['VALUE'] as $key => $sTypesItem) :?>
                <li class="lot__tag">
                    <a href="<?=$arResult['LIST_PAGE_URL']?>?types=<?=$arResult['PROPERTIES']['TYPE']['VALUE_ENUM_ID'][$key]?>&filter=Y">
                        <?php echo $sTypesItem?>
                    </a>
                </li>
            <?php endforeach;?>
        </ul>
    <?php endif;?>
    <div class="lot__header">
        <?php if (!empty($arResult['PROPERTIES']['SQUARE']['VALUE'])) :?>
            <div class="lot__area"><?php echo $arResult['PROPERTIES']['SQUARE']['VALUE']?> м<sup>2</sup></div>
        <?php endif?>
        <h1 class="lot__title"><?php echo $arResult['H1']?></h1>
    </div>
    <div class="lot__slider">
        <?php
        if ($arResult['PROPERTIES']['SLIDER']['VALUE']) {
            $APPLICATION->IncludeComponent(
                'guta:lot.slider',
                '',
                [
                    'ID' => $arResult['ID'],
                    'IMAGES' => (array)$arResult['PROPERTIES']['SLIDER']['VALUE'],
                    'CACHE_TYPE' => 'A',
                    'CACHE_TIME' => Constants::SECONDS_IN_MONTH,
                ],
                $component
            );
        }
        ?>
        <div class="lot__phone lot__phone_desktop">
            По вопросам аренды звоните<br>
            <a class="lot__phone-link" href="tel:<?=Constants::PHONE_TO_CALL_MOBILE;?>">
                <?=Constants::PHONE_TO_CALL;?>
            </a>
        </div>
    </div>
    <div class="lot__table">
        <?php if (!empty($arResult['PROPERTIES']['PRICE_SQUARE']['VALUE'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Цена за кв. метр в год (₽)</div>
                <div class="lot__table-col">
                    <?php echo Supply::normalizePrice($arResult['PROPERTIES']['PRICE_SQUARE']['VALUE'])?> ₽
                </div>
            </div>
        <?php endif;?>

        <?php if (!empty($arResult['PRICE_MONTH'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Общая цена в месяц (₽)</div>
                <div class="lot__table-col">
                    <?php echo Supply::normalizePrice($arResult['PRICE_MONTH'])?> ₽
                </div>
            </div>
        <?php endif;?>

        <?php if (!empty($arResult['PROPERTIES']['FLOOR']['VALUE'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Этаж</div>
                <div class="lot__table-col"><?php echo $arResult['PROPERTIES']['FLOOR']['VALUE']?></div>
            </div>
        <?php endif;?>

        <?php if (!empty($arResult['PROPERTIES']['BCLASS']['VALUE'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Класс здания</div>
                <div class="lot__table-col"><?php echo $arResult['PROPERTIES']['BCLASS']['VALUE']?></div>
            </div>
        <?php endif;?>

        <?php if (!empty($arResult['PROPERTIES']['VACATIONS']['VALUE'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Арендные каникулы</div>
                <div class="lot__table-col"><?php echo $arResult['PROPERTIES']['VACATIONS']['VALUE']?></div>
            </div>
        <?php endif;?>

        <?php if (!empty($arResult['PROPERTIES']['HEIGHT']['VALUE'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Высота потолков</div>
                <div class="lot__table-col"><?php echo $arResult['PROPERTIES']['HEIGHT']['VALUE']?> м</div>
            </div>
        <?php endif;?>

        <?php if (!empty($arResult['PROPERTIES']['QTY_ROOM']['VALUE'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Количество комнат</div>
                <div class="lot__table-col"><?php echo $arResult['PROPERTIES']['QTY_ROOM']['VALUE']?></div>
            </div>
        <?php endif;?>

        <?php if (!empty($arResult['PROPERTIES']['LAYOUT']['VALUE'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Планировка</div>
                <div class="lot__table-col"><?php echo $arResult['PROPERTIES']['LAYOUT']['VALUE']?> </div>
            </div>
        <?php endif;?>

        <?php if (!empty($arResult['PROPERTIES']['STATE_ROOM']['VALUE'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Состояние</div>
                <div class="lot__table-col"><?php echo $arResult['PROPERTIES']['STATE_ROOM']['VALUE']?> </div>
            </div>
        <?php endif;?>

        <?php if (!empty($arResult['PROPERTIES']['CITY']['VALUE'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Город</div>
                <div class="lot__table-col"><?php echo $arResult['PROPERTIES']['CITY']['VALUE']?></div>
            </div>
        <?php endif;?>

        <?php if (!empty($arResult['ADDRESS'])) :?>
            <div class="lot__table-row">
                <div class="lot__table-col">Адрес</div>
                <div class="lot__table-col"><?php echo $arResult['ADDRESS']?></div>
            </div>
        <?php endif;?>
    </div>
    <div id="vue-lotAccordionBlock" class="vue-component" data-component="LotAccordionBlock">
        <div class="lot__accordion-group">
            <?php if (!empty($arResult['DETAIL_TEXT'])) :?>
                <accordion>
                        <template v-slot:title>
                            Описание
                        </template>
                        <template v-slot:content>
                            <?php echo $arResult['~DETAIL_TEXT']?>
                        </template>

                </accordion>
            <?php endif;?>

            <?php if (!empty($arResult['PROPERTIES']['COMMUNICATION']['VALUE'])) :?>
                <accordion>
                    <template v-slot:title>
                        Коммуникации
                    </template>
                    <template v-slot:content>
                        <?php echo $arResult['PROPERTIES']['COMMUNICATION']['~VALUE']?>
                    </template>
                </accordion>
            <?php endif;?>
            <?php if (!empty($arResult['PROPERTIES']['TERMS']['VALUE'])) :?>
                <accordion>
                    <template v-slot:title>
                        Условия сделки
                    </template>
                    <template v-slot:content>
                        <?php echo $arResult['PROPERTIES']['TERMS']['~VALUE']?>
                    </template>
                </accordion>
            <?php endif;?>
        </div>
    </div>

    <div class="lot__btn-group">
        <?php if (!empty($arResult['PDF_FILE_PATH'])) :?>
        <a
            href="#print"
            class="lot__print-btn"
            onclick="printJS(`<?php echo $arResult['PDF_FILE_PATH']?>`); return false;"
        >печатать</a>
        <a target="_blank" href="<?php echo $arResult['PDF_FILE_PATH']?>" class="lot__download-btn">cкачать</a>
        <?php endif;?>
    </div>
    <div class="lot__phone lot__phone_mobile">
        По вопросам аренды звоните<br>
        <a class="lot__phone-link" href="tel:<?=Constants::PHONE_TO_CALL_MOBILE;?>">
            <?=Constants::PHONE_TO_CALL;?>
        </a>
    </div>
</section>

<?php
$APPLICATION->IncludeComponent(
    'guta:similar.lots',
    '',
    [
        'IBLOCK_CODE' => $arResult['IBLOCK_ID'],
        'CURRENT_ELEMENT_ID' => $arResult['ID'],
        'CACHE_TYPE' => 'N',
        'HEADER' => 'Похожие предложения',
        'CACHE_TIME' => Constants::SECONDS_IN_MONTH,
        'NORMALIZE_QUANTITY' => true
    ],
    $component
);
?>

