<?php use Local\Constants;
use Local\Guta\IBlockElementManager;
use Local\Models\ObjectFiles;
use Local\Util\GutaResize;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @const integer PREVIEW_WIDTH_PICTURE Необходимая ширина картинок. */
const PREVIEW_WIDTH_PICTURE = 400;
/** @const integer PREVIEW_HEIGHT_PICTURE Необходимая высота картинок. */
const PREVIEW_HEIGHT_PICTURE = 400;

$cp = $this->__component;

if (is_object($cp)) {
    $cp->arResult['H1'] = !empty($arResult['NAME']) ? $arResult['NAME'] : 'Фильтр';
    $cp->arResult['FILTERED_SECTION'] = !empty($arParams['FILTERED']) ? $arParams['FILTERED'] : [];
    // Ресайз картинок. Логика: сначала PREVIEW_PICTURE, если ее не существует, то
    // берем DETAIL_PICTURE.
    foreach ($arResult['ITEMS'] as $key => $arItem) {
        // Если нет картинок, то ничего не делаем.
        if (empty($arItem['PREVIEW_PICTURE']['ID'])
            && empty($arItem['DETAIL_PICTURE']['ID'])) {
            $cp->arResult['ITEMS'][$key]['RESIZED_IMAGE'] = '';
            continue;
        }
        /** @var integer $idPictureForResize ID картинки для ресайза. */
        $idPictureForResize = !empty($arItem['PREVIEW_PICTURE']['ID']) ? $arItem['PREVIEW_PICTURE']['ID'] : $arItem['DETAIL_PICTURE']['ID'];
        // Ресайз.
        $cp->arResult['ITEMS'][$key]['RESIZED_IMAGE'] = GutaResize::resize(
            $idPictureForResize,
            PREVIEW_WIDTH_PICTURE,
            PREVIEW_HEIGHT_PICTURE
        );

        // Расчет стоимости за месяц.
        $cp->arResult['ITEMS'][$key]['PRICE_MONTH'] = (int)$arItem['PROPERTIES']['TOTAL_PRICE_MONTH']['VALUE'];

        if (!$cp->arResult['ITEMS'][$key]['PRICE_MONTH']
            && $arItem['PROPERTIES']['PRICE_SQUARE']['VALUE']
            && $arItem['PROPERTIES']['SQUARE']['VALUE']) {
            $cp->arResult['ITEMS'][$key]['PRICE_MONTH'] = round(
                ($arItem['PROPERTIES']['SQUARE']['VALUE'] * $arItem['PROPERTIES']['PRICE_SQUARE']['VALUE']) / 12
            );
        }
    }

    // Данные для фильтра "по назначению".
    /** @var array $arPriceFilter Данные для фильтра. */
    $arPriceFilter = [
        'name' => 'price',
        'currentVal' => '',
        'title' => 'по цене',
        'values' => [
            [
                'link' => '?filter_section=Y&price=ASC',
                'label' => 'по возрастанию',
            ],

            [
                'link' => '?filter_section=Y&price=DESC',
                'label' => 'по убыванию',
            ],
        ],
    ];

    $arPriceSquare = [
        'name' => 'square',
        'currentVal' => '',
        'title' => 'по площади',
        'values' => [
            [
                'link' => '?filter_section=Y&square=ASC',
                'label' => 'по возрастанию',
            ],

            [
                'link' => '?filter_section=Y&square=DESC',
                'label' => 'по убыванию',
            ],
        ],
    ];

    /** @var array $arValues Значения фильтра по назначению. */
    $arValues = [];

    // Данные для фильтра "по назначению".
    // Отфильтровать значения фильтра, для которых нет элементов.

    try {
        $arTypeData = IBlockElementManager::getIBlockElements(
            [
                'AR_ORDER' => ['SORT' => 'DESC'],
                'AR_FILTER' => [
                    'ACTIVE' => 'Y',
                    'IBLOCK_CODE' => Constants::IBLOCK_CODE_RENTA,
                    'IBLOCK_SECTION_ID' => $arResult['ID'],
                ],
                'AR_GROUP' => ['PROPERTY_TYPE'],
            ]
        );
    } catch (\Bitrix\Main\LoaderException $e) {
        $arTypeData = [];
    }

    foreach ($arTypeData as $arEnumItem) {
        $arValues[] = [
            'link' => '?filter_section=Y&type='.$arEnumItem['PROPERTY_TYPE_ENUM_ID'],
            'label' => $arEnumItem['PROPERTY_TYPE_VALUE'],
        ];
    }

    $cp->arResult['ALL_TYPES_FILTER'] = [
        'filters' => [

            $arPriceFilter,
            $arPriceSquare,
            [
                'name' => 'type',
                'currentVal' => '',
                'title' => 'по назначению',
                'values' => $arValues,
            ],
        ],
    ];

    // Прикрепленные документы
    $cp->arResult['DOCUMENTS'] = [];

    if (!empty($arResult['UF_FILES'])) {
        $obObjectModel = ObjectFiles::query()
            ->sort(['SORT' => 'ASC'])
            ->filter(['ACTIVE' => 'Y', 'ID' => $arResult['UF_FILES']])
            ->select('ID', 'NAME', 'PROPERTY_TITLE_BUTTON', 'PROPERTY_DOCUMENTS')
            ->getList();

        /** @var array $arDocumentData Данные о документах. */
        $arDocumentData = [];

        $obObjectModel->each(
            function ($item, $key) use (&$arDocumentData) {
                $arDocumentData[] = [
                    'FILE' => !empty($item['PROPERTY_DOCUMENTS_VALUE']) ? CFile::GetPath(
                        $item['PROPERTY_DOCUMENTS_VALUE']
                    ) : '',
                    'TITLE' => !empty($item['PROPERTY_TITLE_BUTTON_VALUE']) ? $item['PROPERTY_TITLE_BUTTON_VALUE'] : '',
                ];
            }
        );

        $cp->arResult['DOCUMENTS'] = $arDocumentData;
    }

    $cp->setResultCacheKeys(['H1', 'FILTERED_SECTION', 'ALL_TYPES_FILTER', 'DOCUMENTS', 'PRICE_MONTH']);
}
