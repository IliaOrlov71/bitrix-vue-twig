<?php use Bitrix\Main\Application;
use Local\Office\Canonical;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

Canonical::staticPage();

// Хлебная крошка для страницы с результатами фильтра.
$request = Application::getInstance()->getContext()->getRequest();
if (!empty($arParams['FILTERED'] && empty($request->getQuery('filter_section')))) {
    $APPLICATION->AddChainItem('Фильтр', '');
}

// Установка title & description.
$sTitle = !(empty($arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE'])) ?
    $arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE']
    : $arResult['NAME'];

// Модификаций title для страниц с фильтром.
if (!empty($arParams['FILTERED'] && empty($request->getQuery('filter_section')))) {
    $sTitle= $sTitle . 'Аренда | Фильтр по лотам';
}

$sDescription = !(empty($arResult['IPROPERTY_VALUES']['SECTION_META_DESCRIPTION'])) ?
    $arResult['IPROPERTY_VALUES']['SECTION_META_DESCRIPTION']
    : '';

$APPLICATION->SetPageProperty('title', $sTitle);
$APPLICATION->SetPageProperty('description', $sDescription);
