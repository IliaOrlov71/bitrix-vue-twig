<?php use Local\Guta\IBlockManager;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
/** @var string $sNameSection Название инфоблока.*/
$sNameSection = IBlockManager::getIBlockNameById($arParams['IBLOCK_ID']);
$sH1 = empty($arParams['H1_PROPERTY']) ? $sNameSection : $arParams['H1_PROPERTY'];
?>

<div class="container">

    <?php if ($sH1) :?>
        <div class="row">
                <div class="col-12">
                    <h1 class="title title_regular">
                        <?= $sH1 ?>
                    </h1>
                </div>
        </div>
    <?php endif;?>

    <div class="row">
        <?php if (!empty($arResult['IBLOCK_DESCRIPTION'])) : ?>
            <div class="col-12">
                <div class="text">
                    <?= $arResult['IBLOCK_DESCRIPTION'] ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="vue-component" data-component="OfficesItems" id="vue-officesItems">
    <div class="container d-none d-md-block officesList">
        <div class="row">
            <div class="col-12">
                <div class="row justify-content-center officesList__towns">
                    <?php foreach ($arResult['SECTIONS_BY_CITY'] as $key => $arItemCity) :?>
                        <div class="col-auto">
                            <div class="officesList__town"
                                 v-bind:class="{ 'officesList__town_active': <?= $key ?>==key}"
                                 @click="changeCity(<?= $key ?>)"><?= $arItemCity['UF_CITY'] ?></div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <?php foreach ($arResult['SECTIONS_BY_CITY'] as $key => $arItem) : ?>
                    <transition name="officesList__animation">
                        <div class="row officesList__items justify-content-center justify-content-md-start"
                             v-if="key==<?= $key ?>">
                            <?php
                            foreach ($arItem['ITEMS'] as $arItemCitySection) :
                                if (empty($arItemCitySection['PICTURE']['SRC'])) {
                                    continue;
                                }
                                ?>
                                <div class="col-auto col-md-6 col-lg-4 officesList__item"
                                     @click="clickItem('<?= $arItemCitySection['SECTION_PAGE_URL'] ?>')">
                                    <div class="officesItem">
                                        <div class="officesItem__block">
                                            <div class="officesItem__img">
                                                <a href="<?= $arItemCitySection['SECTION_PAGE_URL'] ?>">
                                                    <img src="<?= ($arItemCitySection['PICTURE']['RESIZE_SRC']) ? $arItemCitySection['PICTURE']['RESIZE_SRC'] : $arItemCitySection['PICTURE']['SRC']; ?>">
                                                </a>
                                            </div>
                                            <div class="officesItem__panel">
                                                <div class="officesItem__title"><?= $arItemCitySection['NAME'] ?></div>
                                                <?php if (!empty($arItemCitySection['UF_ADDRESS'])) : ?>
                                                    <div class="officesItem__description"><?= $arItemCitySection['UF_ADDRESS'] ?></div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </transition>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>


<div class="vue-component" data-component="OfficesSlider" id="vue-officesSlider">
    <div class="container d-md-none officesList">
        <div class="row justify-content-center officesList__towns">
            <?php foreach ($arResult['SECTIONS_BY_CITY'] as $sKeyCity => $arItemCity) : ?>
                <div class="col-auto">
                    <div class="officesList__town"
                         v-bind:class="{ 'officesList__town_active': <?= $key ?>==key}"
                         @click="changeCity(<?= $sKeyCity ?>)"><?= $arItemCity['UF_CITY'] ?></div>
                </div>
            <?php endforeach;?>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="objectSlider">
                    <div class="objectSlider__slider">
                        <slick ref="slick" :options="slickOptions">
                            <?php
                            foreach ($arResult['SECTIONS_BY_CITY'] as $key => $arItem) :
                                foreach ($arItem['ITEMS'] as $arItemCitySection) :
                                    if (empty($arItemCitySection['PICTURE']['SRC'])) {
                                        continue;
                                    }
                                    ?>
                                    <div class="officesItem" data-key="<?= $key ?>">
                                        <div class="officesItem__block">
                                            <div class="officesItem__img">
                                                <a href="<?= $arItemCitySection['SECTION_PAGE_URL'] ?>">
                                                    <img src="<?= ($arItemCitySection['PICTURE']['RESIZE_SRC']) ? $arItemCitySection['PICTURE']['RESIZE_SRC'] : $arItemCitySection['PICTURE']['SRC']; ?>">
                                                </a>
                                            </div>
                                            <div class="officesItem__panel">
                                                <div class="officesItem__title"><?= $arItemCitySection['NAME'] ?></div>
                                                <?php if (!empty($arItemCitySection['UF_ADDRESS'])) :?>
                                                    <div class="officesItem__description"><?= $arItemCitySection['UF_ADDRESS'] ?></div>
                                                <?php endif;?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach;
                            endforeach; ?>
                        </slick>
                    </div>
                    <div class="objectSlider__nav" v-if="totalSlides>1">
                        <button class="objectSlider__arrowLeft" @click="prevSlide">
                            <svg>
                                <use xlink:href="#sprite-arrow_black"></use>
                            </svg>
                        </button>
                        <div class="objectSlider__pageNumbers">
                            <div class="objectSlider__currentPage">{{ currentSlide + 1}}</div>
                            <div class="objectSlider__pageCount">{{ totalSlides }}</div>
                        </div>
                        <button class="objectSlider__arrowRight" @click="nextSlide">
                            <svg>
                                <use xlink:href="#sprite-arrow_black"></use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







