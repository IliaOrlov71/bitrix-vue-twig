<?php

use Local\Guta\IBlockManager;
use Local\Util\GutaResize;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/* Создаем объекта Битрикс Компонент, создаем у поля arResult новый массив сгруппированных по городу объектов. */
/* Кэшируем этот массив. */

foreach ($arResult['SECTIONS'] as $item) {
    if (!empty($item['UF_CITY'])) {
        $item['PICTURE']['RESIZE_SRC'] = GutaResize::resize($item['PICTURE']['ID'], 370, 500);

        $key = array_search($item['UF_CITY'], array_column($arResult['SECTIONS_BY_CITY'], 'UF_CITY'));
        if (!is_numeric($key)) {
            $arResult['SECTIONS_BY_CITY'][]['UF_CITY'] = $item['UF_CITY'];
            $key = count($arResult['SECTIONS_BY_CITY']) - 1;
        }

        if ($item['ELEMENT_CNT'] > 0) {
            $arResult['SECTIONS_BY_CITY'][$key]['ITEMS'][] = $item;
        }
    }
}

$cp = $this->__component;
if (is_object($cp)) {
    try {
        // Описание инфоблока.
        $cp->arResult['IBLOCK_DESCRIPTION'] = IBlockManager::getIBlockDescriptionById($arParams['IBLOCK_ID']);
    } catch (\Bitrix\Main\ArgumentException $e) {
        $cp->arResult['IBLOCK_DESCRIPTION'] = '';
    } catch (\Bitrix\Main\LoaderException $e) {
        $cp->arResult['IBLOCK_DESCRIPTION'] = '';
    }
    $cp->setResultCacheKeys(['SECTIONS_BY_CITY', 'IBLOCK_DESCRIPTION']);
}
