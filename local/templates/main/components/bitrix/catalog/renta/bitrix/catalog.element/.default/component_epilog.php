<?php use Local\Office\Canonical;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}


// Канононические URL из свойства CANONICAL_URL
Canonical::passLink($this, (string)$arResult['CANONICAL_URL']);
