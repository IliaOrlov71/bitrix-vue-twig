<?php
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;
use Local\Constants;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

$this->setFrameMode(true);

/**
 * Фильтр.
 */
$request = Application::getInstance()->getContext()->getRequest();

if ($request->getQuery('filter_section') === 'Y') {
    $sPrice = htmlspecialchars($request->getQuery('price'));
    $sSquare = htmlspecialchars($request->getQuery('square'));
    $sCategory = htmlspecialchars($request->getQuery('type'));
    /**
     * Фильтр по цене - убывание/возрастание.
     */
    if (!empty($sPrice)) {
        $arParams['ELEMENT_SORT_FIELD'] =  'PROPERTY_PRICE_SQUARE';
        $arParams['ELEMENT_SORT_ORDER'] =  $sPrice;
    }
    /**
     * Фильтр по площади - убывание/возрастание.
     */
    if (!empty($sSquare)) {
        $arParams['ELEMENT_SORT_FIELD'] =  'PROPERTY_SQUARE';
        $arParams['ELEMENT_SORT_ORDER'] =  $sSquare;
    }
    /**
     * Фильтр по типу объекта.
     */
    if (!empty($sCategory)) {
        /** @var array Битриксовый глобальный фильтр. */
        global $arrFilter;

        $arrFilter = [
            'PROPERTY_TYPE' => $sCategory,
        ];
    }
}

// Необходимо для работы метода CLightHTMLEditor::IsMobileDevice.
CModule::includeModule('fileman');

$intSectionID = $APPLICATION->IncludeComponent(
    'bitrix:catalog.section',
    '',
    [
        'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'SHOW_ALL_WO_SECTION' => 'N',
        'ELEMENT_SORT_FIELD' => $arParams['ELEMENT_SORT_FIELD'],
        'ELEMENT_SORT_ORDER' => $arParams['ELEMENT_SORT_ORDER'],
        'ELEMENT_SORT_FIELD2' => $arParams['ELEMENT_SORT_FIELD2'],
        'ELEMENT_SORT_ORDER2' => $arParams['ELEMENT_SORT_ORDER2'],
        'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
        'PRODUCT_PROPERTIES' => (isset($arParams['PRODUCT_PROPERTIES']) ? $arParams['PRODUCT_PROPERTIES'] : []),
        'PROPERTY_CODE' => (isset($arParams['LIST_PROPERTY_CODE']) ? $arParams['LIST_PROPERTY_CODE'] : []),
        'PROPERTY_CODE_MOBILE' => $arParams['LIST_PROPERTY_CODE_MOBILE'],
        'META_KEYWORDS' => $arParams['LIST_META_KEYWORDS'],
        'META_DESCRIPTION' => $arParams['LIST_META_DESCRIPTION'],
        'BROWSER_TITLE' => $arParams['LIST_BROWSER_TITLE'],
        'SET_LAST_MODIFIED' => $arParams['SET_LAST_MODIFIED'],
        'INCLUDE_SUBSECTIONS' => $arParams['INCLUDE_SUBSECTIONS'],
        'BASKET_URL' => $arParams['BASKET_URL'],
        'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
        'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
        'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
        'SECTION_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
        'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['element'],
        'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
        'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
        'FILTER_NAME' => $arParams['FILTER_NAME'],
        'CACHE_TYPE' => $arParams['CACHE_TYPE'],
        'CACHE_TIME' => $arParams['CACHE_TIME'],
        'CACHE_FILTER' => $arParams['CACHE_FILTER'],
        'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
        'SET_TITLE' => $arParams['SET_TITLE'],
        'MESSAGE_404' => $arParams['~MESSAGE_404'],
        'SET_STATUS_404' => $arParams['SET_STATUS_404'],
        'SHOW_404' => $arParams['SHOW_404'],
        'FILE_404' => $arParams['FILE_404'],
        'DISPLAY_COMPARE' => $arParams['USE_COMPARE'],
        'PAGE_ELEMENT_COUNT' => CLightHTMLEditor::IsMobileDevice() ? 99999 : $arParams['PAGE_ELEMENT_COUNT'],
        'LINE_ELEMENT_COUNT' => $arParams['LINE_ELEMENT_COUNT'],
        'PRICE_CODE' => $arParams['~PRICE_CODE'],
        'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
        'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
        'SEF_FOLDER' => $arParams['SEF_FOLDER'],
        'SEF_MODE' => $arParams['SEF_MODE'],
        'SEF_URL_TEMPLATES' => $arParams['SEF_URL_TEMPLATES'],
        'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],

        'DISPLAY_TOP_PAGER' => $arParams['DISPLAY_TOP_PAGER'],
        'DISPLAY_BOTTOM_PAGER' => $arParams['DISPLAY_BOTTOM_PAGER'],
        'PAGER_TITLE' => $arParams['PAGER_TITLE'],
        'PAGER_SHOW_ALWAYS' => $arParams['PAGER_SHOW_ALWAYS'],
        'PAGER_TEMPLATE' => $arParams['PAGER_TEMPLATE'],
        'PAGER_DESC_NUMBERING' => $arParams['PAGER_DESC_NUMBERING'],
        'PAGER_DESC_NUMBERING_CACHE_TIME' => $arParams['PAGER_DESC_NUMBERING_CACHE_TIME'],
        'PAGER_SHOW_ALL' => $arParams['PAGER_SHOW_ALL'],
        'PAGER_BASE_LINK_ENABLE' => $arParams['PAGER_BASE_LINK_ENABLE'],
        'PAGER_BASE_LINK' => $arParams['PAGER_BASE_LINK'],
        'PAGER_PARAMS_NAME' => $arParams['PAGER_PARAMS_NAME'],
        'LAZY_LOAD' => $arParams['LAZY_LOAD'],
        'MESS_BTN_LAZY_LOAD' => $arParams['~MESS_BTN_LAZY_LOAD'],
        'LOAD_ON_SCROLL' => $arParams['LOAD_ON_SCROLL'],
        'OFFERS_LIMIT' => (isset($arParams['LIST_OFFERS_LIMIT']) ? $arParams['LIST_OFFERS_LIMIT'] : 0),
        'USE_FILTER' => $arParams['USE_FILTER'],
        'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
        'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
        'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
        'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
        'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
        'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
        'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
        'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
        'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
        'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
        'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
        'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
        'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
        'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
        'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),
        'ADD_SECTIONS_CHAIN' => $arParams['ADD_SECTIONS_CHAIN'],
        'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
        'COMPARE_NAME' => $arParams['COMPARE_NAME'],
        'SECTION_USER_FIELDS' => ['UF_CITY', 'UF_ADDRESS', 'UF_FILES'],
        'AJAX_OPTION_ADDITIONAL' => $arParams['AJAX_OPTION_ADDITIONAL'],
        'AJAX_OPTION_HISTORY' => $arParams['AJAX_OPTION_HISTORY'],
        'AJAX_OPTION_JUMP' => $arParams['AJAX_OPTION_JUMP'],
        'AJAX_OPTION_STYLE' =>$arParams['AJAX_OPTION_STYLE'],
        'AJAX_MODE' => $arParams['AJAX_MODE'],
        'FILTERED' => !empty($arrFilter) ?  $arrFilter : []
    ],
    $component
);


/**
 * Установить куку о просмотре раздела.
 */
$obCookie = new Cookie(Constants::COOKIES_SECTION_NAME, $intSectionID);
if (empty($_SERVER['HTTPS'])) {
    $obCookie->setSecure(false);
}
$obCookie->setDomain($_SERVER['SERVER_NAME']);

try {
    Application::getInstance()->getContext()->getResponse()->addCookie($obCookie);
} catch (\Bitrix\Main\SystemException $e) {
}
