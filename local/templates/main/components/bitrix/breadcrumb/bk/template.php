<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
    die();
}

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if (empty($arResult)) {
    return '';
}

$strReturn = '';

$strReturn .= '<div class="container"><div class="row">';
$strReturn .= '<div class="col-sm-12" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';
$strReturn .= '<ul class="breadcrumbs">';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++) {
    $title = htmlspecialcharsEx($arResult[$index]['TITLE']);

    if ($arResult[$index]['LINK'] !== '' && $index != $itemSize-1) {
        $strReturn .= '<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="name" href="'.$arResult[$index]['LINK'].'">'.$arResult[$index]['TITLE'].'</a>
                <meta itemprop="position" content="'.($index + 1).'" />
                </li>';
    } else {
        $strReturn .= '<li class="breadcrumbs__item breadcrumbs__item_active" 
                    itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                '.$arResult[$index]['TITLE'].'
                <meta itemprop="position" content="'.($index + 1).'" />
                </li>';
    }
}

$strReturn .= '</ul>';
$strReturn .= '</div>';
$strReturn .= '</div></div>';

return $strReturn;
