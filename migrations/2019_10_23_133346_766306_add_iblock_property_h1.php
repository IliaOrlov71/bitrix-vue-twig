<?php

use Arrilot\BitrixMigrations\BaseMigrations\BitrixMigration;
use Arrilot\BitrixMigrations\Exceptions\MigrationException;

class AddIblockPropertyH120191023133346766306 extends BitrixMigration
{
    /**
     * Run the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function up()
    {
        $fields = array (
            'ID' => 0,
            'NAME' => 'Заголовок H1',
            'SORT' => 1000,
            'CODE' => 'H1',
            'MULTIPLE' => 'N',
            'IS_REQUIRED' => 'N',
            'ACTIVE' => 'Y',
            'USER_TYPE' => false,
            'PROPERTY_TYPE' => 'S',
            'IBLOCK_ID' => $this->getIblockIdByCode('renta'),
            'FILE_TYPE' => '',
            'LIST_TYPE' => 'L',
            'ROW_COUNT' => 1,
            'COL_COUNT' => 30,
            'LINK_IBLOCK_ID' => 0,
            'DEFAULT_VALUE' => '',
            'USER_TYPE_SETTINGS' =>
                array (
                ),
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'MULTIPLE_CNT' => 5,
            'HINT' => '',
            'VALUES' =>
                array (
                ),
            'SECTION_PROPERTY' => 'Y',
            'SMART_FILTER' => 'N',
            'DISPLAY_TYPE' => false,
            'DISPLAY_EXPANDED' => 'N',
            'FILTER_HINT' => '',
            'FEATURES' =>
                array (
                ),
        );

        $ibp = new CIBlockProperty();
        $propId = $ibp->add($fields);

        if (!$propId) {
            throw new MigrationException('Ошибка при добавлении свойства инфоблока '.$ibp->LAST_ERROR);
        }
    }

    /**
     * Reverse the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function down()
    {
        //
    }
}
