<?php

use Arrilot\BitrixMigrations\BaseMigrations\BitrixMigration;
use Arrilot\BitrixMigrations\Exceptions\MigrationException;

class AddIblockType20191023122054560297 extends BitrixMigration
{
    /**
     * Run the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function up()
    {
        $cbt = new CIBlockType;
        $cbtRes = $cbt->Add([
            'ID'=>'content',
            'SECTIONS'=>'Y',
            'IN_RSS'=>'N',
            'SORT'=>100,
            'LANG'=>array(
                'ru'=>array(
                    'NAME'=>'Контент',
                ),
                'en'=>array(
                    'NAME'=>'Content',
                )
            )
        ]);

        if (!$cbtRes) {
            throw new MigrationException('Ошибка при добавлении типа инфоблока rooms'.$cbt->LAST_ERROR);
        }
    }

    /**
     * Reverse the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function down()
    {
        //
    }
}
