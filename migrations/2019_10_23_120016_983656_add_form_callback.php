<?php

use Arrilot\BitrixMigrations\BaseMigrations\BitrixMigration;
use Arrilot\BitrixMigrations\Exceptions\MigrationException;

class AddFormCallback20191023120016983656 extends BitrixMigration
{
    /**
     * Run the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function up()
    {

        $arFieldsForm = array(
            'NAME'    => 'Форма обратного звонка',
            'SID'     => 'FORM_FEEDBACK_CALLBACK',
            'C_SORT'  => 200,
            'BUTTON'  => 'Отправить',
            'arSITE'  => array('s1'),
            'arMENU'  => array('ru' => 'Форма обратного звонка',  "en" => "Form сфддифсл")
        );

// добавим новую веб-форму
        $FORM_ID = CForm::Set($arFieldsForm, false, 'N');

        if ($FORM_ID > 0) {
            echo 'Добавлена веб-форма с ID='.$FORM_ID;

            $arANSWER[] = array(
                'MESSAGE'     => 'да',                           // параметр ANSWER_TEXT
                'C_SORT'      => 100,                            // порядок фортировки
                'ACTIVE'      => 'Y',                            // флаг активности
                'FIELD_TYPE'  => 'radio',                        // тип ответа
                'FIELD_PARAM' => 'checked class="inputradio"'  // параметры ответа
            );

            $arANSWER[] = array(
                'MESSAGE'     => 'нет',
                'C_SORT'      => 200,
                'ACTIVE'      => 'Y',
                'FIELD_TYPE'  => 'radio'
            );

            $textarea = array(
                array(
                    'MESSAGE' => 'Комментарии', // параметр ANSWER_TEXT
                    'C_SORT' => 1000, // порядок фортировки
                    'ACTIVE' => 'Y', // флаг активности
                    'FIELD_TYPE' => 'textarea',
                    'FIELD_PARAM' => 'Y',
                ),

            );


            $arAddFields = array(

                'NAME' => array(
                    'FIELD_TYPE' => 'text',
                    'TITLE' => 'Имя',
                    'SORT' => 100,
                    'arANSWER' => [
                        ['FIELD_TYPE' => 'text',
                            'ACTIVE' => 'Y',
                            'C_SORT' => 50,
                            'MESSAGE' => 'NAME',
                            'VALUE' => ''

                        ]
                    ],
                ),

                'PHONE' => array(
                    'FIELD_TYPE' => 'text',
                    'TITLE' => 'Телефон',
                    'SORT' => 300,
                    'arANSWER' => [
                        ['FIELD_TYPE' => 'text',
                            'ACTIVE' => 'Y',
                            'C_SORT' => 50,
                            'MESSAGE' => 'PHONE',
                            'VALUE' => ''

                        ]
                    ],
                ),

                'COMMENTS' => array(
                    'FIELD_TYPE' => 'textarea',
                    'TITLE' => 'Комментарии',
                    'arANSWER' => $textarea,
                    'arFILTER_ANSWER_TEXT' => array('textarea'),
                    'SORT' => 500
                ),

                'CONFIRMATION' => array(
                    'FIELD_TYPE' => 'checkbox',
                    'TITLE' => 'Согласие на обработку персональных данных',
                    'SORT' => 400,
                    'arFILTER_ANSWER_TEXT' => array('radio'),
                    'arANSWER' => $arANSWER,

                ),

            );



            foreach ($arAddFields as $key => $arFiledItem) {
                unset($arFiledNew);

                //добавить поле в форму
                $arFiledNew = array(
                    'FORM_ID' => $FORM_ID,
                    'ACTIVE' => 'Y',
                    'TITLE' => $arFiledItem['TITLE'],
                    'SID' => $key,
                    'C_SORT' => $arFiledItem['SORT'],
                    'ADDITIONAL' => 'N',
                    'IN_RESULTS_TABLE' => 'Y',
                    'IN_EXCEL_TABLE' => 'Y',
                    'FIELD_TYPE' => $arFiledItem['FIELD_TYPE'],
                    'FILTER_TITLE' => $arFiledItem['TITLE'],
                    'RESULTS_TABLE_TITLE' => $arFiledItem['TITLE']
                );

                if (!empty($arFiledItem['arANSWER'])) {
                    $arFiledNew['arANSWER'] = $arFiledItem['arANSWER'];
                    $arFiledNew['arFILTER_ANSWER_TEXT'] = $arFiledItem['arFILTER_ANSWER_TEXT'];
                } else {
                    $arFiledNew['FIELD_TYPE'] = 'text';
                }

                // добавим новое поле
                $NEW_ID = CFormField::Set($arFiledNew, false, 'N');
                if ($NEW_ID > 0) {
                    echo 'Добавлено поле с ID='.$NEW_ID;
                } else // ошибка
                {
                    // выводим текст ошибки
                    global $strError;
                    echo $strError;
                }
            }

            $arTemplates = CForm::SetMailTemplate($FORM_ID);

            // приписываем вновь созданные почтовые шаблоны данной веб-форме

            CForm::Set(array("arMAIL_TEMPLATE" => $arTemplates), $FORM_ID);

            //добавление статуса
            $arFields = array(
                'FORM_ID' => $FORM_ID,               // ID веб-формы
                'C_SORT' => 100,                    // порядок сортировки
                'ACTIVE' => 'Y',                    // статус активен
                'TITLE' => 'DEFAULT',         // заголовок статуса
                'DESCRIPTION' => 'DEFAULT', // описание статуса
                'CSS' => 'statusgreen',          // CSS класс
                'HANDLER_OUT' => '',                     // обработчик
                'HANDLER_IN' => '',                     // обработчик
                'DEFAULT_VALUE' => 'Y',
                'arPERMISSION_VIEW' => array(30),
                'arPERMISSION_MOVE' => array(30),
                'arPERMISSION_EDIT' => array(30),
                'arPERMISSION_DELETE' => array(30),
            );

            $NEW_ID = CFormStatus::Set($arFields, false, 'N');
            if ($NEW_ID > 0) {
                echo 'Успешно добавлен статус ID='.$NEW_ID;
            } else // ошибка
            {
                // выводим текст ошибки
                global $strError;
                echo $strError;
            }
        }
    }

    /**
     * Reverse the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function down()
    {
        //
    }
}
