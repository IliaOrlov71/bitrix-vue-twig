<?php

use Arrilot\BitrixMigrations\BaseMigrations\BitrixMigration;
use Arrilot\BitrixMigrations\Exceptions\MigrationException;

class AddIblockPropertyType20191023142158551715 extends BitrixMigration
{
    /**
     * Run the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function up()
    {
        $fields = array (
            'ID' => 0,
            'NAME' => 'Тип помещения',
            'SORT' => 10,
            'CODE' => 'TYPE',
            'MULTIPLE' => 'Y',
            'IS_REQUIRED' => 'N',
            'ACTIVE' => 'Y',
            'USER_TYPE' => false,
            'PROPERTY_TYPE' => 'L',
            'IBLOCK_ID' => $this->getIblockIdByCode('renta'),
            'FILE_TYPE' => '',
            'LIST_TYPE' => 'L',
            'ROW_COUNT' => 1,
            'COL_COUNT' => 30,
            'LINK_IBLOCK_ID' => 0,
            'DEFAULT_VALUE' => '',
            'USER_TYPE_SETTINGS' => false,
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'MULTIPLE_CNT' => 5,
            'HINT' => '',
            'VALUES' =>
                array (
                    'n0' =>
                        array (
                            'ID' => 'n0',
                            'VALUE' => 'Офис',
                            'XML_ID' => 'office',
                            'SORT' => 10,
                            'DEF' => 'Y',
                        ),
                    'n1' =>
                        array (
                            'ID' => 'n1',
                            'VALUE' => 'Торговое',
                            'XML_ID' => 'trade',
                            'SORT' => 20,
                            'DEF' => 'N',
                        ),
                    'n2' =>
                        array (
                            'ID' => 'n2',
                            'VALUE' => 'ПСН',
                            'XML_ID' => 'psn',
                            'SORT' => 30,
                            'DEF' => 'N',
                        ),
                    'n3' =>
                        array (
                            'ID' => 'n3',
                            'VALUE' => 'Street Retail',
                            'XML_ID' => 'retail',
                            'SORT' => 40,
                            'DEF' => 'N',
                        ),
                    'n4' =>
                        array (
                            'ID' => 'n4',
                            'VALUE' => 'HoReCa',
                            'XML_ID' => 'horeca',
                            'SORT' => 50,
                            'DEF' => 'N',
                        ),
                    'n5' =>
                        array (
                            'ID' => 'n5',
                            'VALUE' => 'Склады',
                            'XML_ID' => 'store',
                            'SORT' => 60,
                            'DEF' => 'N',
                        ),
                ),
            'SECTION_PROPERTY' => 'Y',
            'SMART_FILTER' => 'N',
            'DISPLAY_TYPE' => 'F',
            'DISPLAY_EXPANDED' => 'N',
            'FILTER_HINT' => '',
            'FEATURES' =>
                array (
                    'iblock:LIST_PAGE_SHOW' =>
                        array (
                            'ID' => 'n0',
                            'MODULE_ID' => 'iblock',
                            'FEATURE_ID' => 'LIST_PAGE_SHOW',
                            'IS_ENABLED' => 'N',
                        ),
                    'iblock:DETAIL_PAGE_SHOW' =>
                        array (
                            'ID' => 'n1',
                            'MODULE_ID' => 'iblock',
                            'FEATURE_ID' => 'DETAIL_PAGE_SHOW',
                            'IS_ENABLED' => 'N',
                        ),
                ),
        );

        $ibp = new CIBlockProperty();
        $propId = $ibp->add($fields);

        if (!$propId) {
            throw new MigrationException('Ошибка при добавлении свойства инфоблока '.$ibp->LAST_ERROR);
        }
    }

    /**
     * Reverse the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function down()
    {
        //
    }
}
