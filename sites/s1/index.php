<?php

use Bitrix\Main\Application;
use Local\Constants;
use Local\Util\StaticPageH1;

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php';
$APPLICATION->SetPageProperty('title', 'БК Недвижимость');
$APPLICATION->SetTitle("БК Недвижимость");
$APPLICATION->SetPageProperty('page_type', 'main');

$sH1 = StaticPageH1::value();

/**
 * Заглавная картинка и краткое описание.
 */
$APPLICATION->IncludeComponent(
    'guta:header.image.text',
    '',
    [
        'H1' => $sH1,
        'CACHE_TYPE' => 'A',
        'CACHE_TIME' => Constants::SECONDS_IN_MONTH,
    ],
    $component
);
?>

<!--Предложение от собственника-->
<section class="section offers">
    <h2 class="offers__title title title_regular">
        Предложение от собственника
    </h2>
    <div class="container">
        <div class="offers__content row">
            <div class="offers__item col-6 col-md-3">
                <svg>
                    <use xlink:href="#sprite-plus_1"></use>
                </svg>
                <p class="offers__item-text">Без коммиссии</p>
            </div>
            <div class="offers__item col-6 col-md-3">
                <svg>
                    <use xlink:href="#sprite-plus_2"></use>
                </svg>
                <p class="offers__item-text">Арендные каникулы</p>
            </div>
            <div class="offers__item col-6 col-md-3">
                <svg>
                    <use xlink:href="#sprite-plus_3"></use>
                </svg>
                <p class="offers__item-text">Эксплуатация и НДС уже включены в ставку</p>
            </div>
            <div class="offers__item col-6 col-md-3">
                <svg>
                    <use xlink:href="#sprite-plus_4"></use>
                </svg>
                <p class="offers__item-text">Предоставляем юридический адрес</p>
            </div>
        </div>
    </div>
</section>
<!--Предложение от собственника-->

<!--Рекомендованные объекты-->
<?php
$APPLICATION->IncludeComponent(
    'guta:recommended.objects',
    '',
    [
        'IBLOCK_CODE' => Constants::IBLOCK_CODE_RENTA,
        'CACHE_TYPE' => 'N',
        'HEADER' => 'Рекомендованные объекты',
        'CACHE_TIME' => Constants::SECONDS_IN_MONTH,
        'COOKIES' => Application::getInstance()->getContext()->getRequest()->getCookie(Constants::COOKIES_SECTION_NAME),
        'NORMALIZE_QUANTITY' => true,
    ],
    false
);
?>
<!--/Рекомендованные объекты-->

<!--Форма подписки-->
<div class="vue-component" data-component="SubscribeForm" data-initial='{"FORM_ID": "<?php echo Constants::FORM_SUBSCRIBE ?>"}'></div>
<!--/Форма подписки-->

<!--Список объектов-->
<?php
$APPLICATION->IncludeComponent(
    'guta:objects.list',
    '',
    [
        'CACHE_TYPE' => 'N',
        'CACHE_TIME' => Constants::SECONDS_IN_MONTH,
    ],
    false
);
?>
<!--/Список объектов-->

<?php require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php';?>
