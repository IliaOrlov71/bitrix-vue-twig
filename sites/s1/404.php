<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>

<section class="pageNotFound">
    <div class="row">
        <div class="pageNotFound__content col">
            <h1 class="pageNotFound__title">404</h1>
            <div class="pageNotFound__subtitle">Страница не найдена</div>
            <a href="/" class="pageNotFound__btn">Вернуться на главную</a>
        </div>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
